<?
\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");

$module_id = "xdev.cp";

global $settings_id;
$settings_id = "xdev_cp";

$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);
if($MODULE_RIGHT < "W")
{
	CAdminMessage::ShowMessage(array("MESSAGE" => GetMessage("ACCESS_DENIED"), "TYPE" => "ERROR"));
	return;
}
CModule::IncludeModuleEx($module_id);

$strWarning = "";

$aTabs = array(
        array("DIV" => "edit1", "TAB" => GetMessage("MAIN_TAB_RIGHTS"), "TITLE" => GetMessage("MAIN_TAB_TITLE_RIGHTS")),
);


$tabControl = new CAdminTabControl("tabControl", $aTabs);

//Save options
if($MODULE_RIGHT >= "W" && strlen($Update.$Apply.$RestoreDefaults) > 0 && check_bitrix_sessid())
{
        if(strlen($RestoreDefaults) > 0)
        {
                COption::RemoveOption($settings_id);
		$z = CGroup::GetList($v1="id",$v2="asc", array("ACTIVE" => "Y", "ADMIN" => "N"));
		while($zr = $z->Fetch())
			$APPLICATION->DelGroupRight($module_id, array($zr["ID"]));
        }
        else
        {
	}
}
?>
<script type="text/javascript">
<!--
	function RestoreDefaults()
	{
	        if(confirm('<? echo GetMessageJS("MAIN_HINT_RESTORE_DEFAULTS_WARNING"); ?>'))
	                window.location = "<?echo $APPLICATION->GetCurPage()?>?RestoreDefaults=Y&lang=<?echo LANG?>&mid=<?echo urlencode($mid)?>&<?=bitrix_sessid_get()?>";
	}
//-->
</script>

<form method="post" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialchars($mid)?>&amp;lang=<?echo LANG?>"><?

$tabControl->Begin();

$tabControl->BeginNextTab();

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");?>
<?$tabControl->Buttons();?>
	<input type="hidden" name="Update" value="Y">
	<input type="submit" class="adm-btn-save"<?if($MODULE_RIGHT < "W") echo " disabled ";?>name="Update" value="<?echo GetMessage("MAIN_SAVE")?>">
	<input type="reset"<?if($MODULE_RIGHT < "W") echo " disabled ";?>name="reset" value="<? echo GetMessage("MAIN_RESET"); ?>">
	<input type="button"<?if($MODULE_RIGHT < "W") echo " disabled ";?>type="button" title="<?echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS")?>" OnClick="RestoreDefaults();" value="<?echo GetMessage("MAIN_RESTORE_DEFAULTS")?>">
<?$tabControl->End();?>
<?=bitrix_sessid_post();?>
</form>