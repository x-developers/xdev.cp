<?
$MESS["XDEV_CP_MODULE_NAME"] = "Parser";
$MESS["XDEV_CP_MODULE_DESC"] = "Html parser";
$MESS["XDEV_CP_PARTNER_NAME"] = "X Developers";
$MESS["XDEV_CP_PARTNER_URI"] = "http://x-dev.ru";
$MESS["XDEV_CP_ACCESS_DENIED"] = "Access denied";
$MESS["XDEV_CP_ACCESS_MODERATION"] = "Moderation";
$MESS["XDEV_CP_ACCESS_FULL"] = "Full access";
$MESS["XDEV_CP_INSTALL_TITLE"] = "Install module";
$MESS["XDEV_CP_UNINSTALL_TITLE"] = "Deinstall module";
?>