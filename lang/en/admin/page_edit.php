<?
$MESS["XDEV_CP_PAGE_TAB1"] = "Settings";
$MESS["XDEV_CP_PAGE_TAB1_TITLE"] = "Base settings";

$MESS["XDEV_CP_PAGE_TAB2"] = "Elements";
$MESS["XDEV_CP_PAGE_TAB2_TITLE"] = "Elements";

$MESS["XDEV_CP_PAGE_BACK_TO_ADMIN"] = "Go back to list";
$MESS["XDEV_CP_PAGE_NEW_RECORD"] = "New record";
$MESS["XDEV_CP_PAGE_NEW_NODE_RECORD"] = "New record";
$MESS["XDEV_CP_PAGE_DELETE_RECORD"] = "Delete record";
$MESS["XDEV_CP_PAGE_DELETE_RECORD_CONFIRM"] = "Warning! All data will be lost! Continue?";
$MESS["XDEV_CP_PAGE_EDIT_TITLE"] = "Page ##ID#";
$MESS["XDEV_CP_PAGE_NEW_TITLE"] = "New page";
$MESS["XDEV_CP_PAGE_NODE_LIST"] = "List of nodes";
$MESS["XDEV_CP_PAGE_NEW_SHEDULE"] = "Add to queue";
$MESS["XDEV_CP_PAGE_CHOOSE_PROFILE_ID"] = "(shoose profile)";
?>