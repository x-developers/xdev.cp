<?
$MESS["XDEV_CP_NODE_TAB1"] = "Settings";
$MESS["XDEV_CP_NODE_TAB1_TITLE"] = "Base settings";

$MESS["XDEV_CP_NODE_BACK_TO_ADMIN"] = "Go back to list";
$MESS["XDEV_CP_NODE_NEW_RECORD"] = "New record";
$MESS["XDEV_CP_NODE_DELETE_RECORD"] = "Delete record";
$MESS["XDEV_CP_NODE_DELETE_RECORD_CONFIRM"] = "Warning! All data will be lost! Continue?";
$MESS["XDEV_CP_NODE_EDIT_TITLE"] = "Element ##ID#";
$MESS["XDEV_CP_NODE_NEW_TITLE"] = "New element";
?>
