<?
$MESS["XDEV_CP_UAGENT_TAB1"] = "Settings";
$MESS["XDEV_CP_UAGENT_TAB1_TITLE"] = "Base settings";

$MESS["XDEV_CP_UAGENT_BACK_TO_ADMIN"] = "Go back to list";
$MESS["XDEV_CP_UAGENT_NEW_RECORD"] = "New record";
$MESS["XDEV_CP_UAGENT_DELETE_RECORD"] = "Delete record";
$MESS["XDEV_CP_UAGENT_DELETE_RECORD_CONFIRM"] = "Warning! All data will be lost! Continue?";
$MESS["XDEV_CP_UAGENT_EDIT_TITLE"] = "User-agent ##ID#";
$MESS["XDEV_CP_UAGENT_NEW_TITLE"] = "New user-agent";
?>