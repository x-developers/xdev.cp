<?
$MESS["XDEV_CP_EXPORT_TAB1"] = "Settings";
$MESS["XDEV_CP_EXPORT_TAB1_TITLE"] = "Base settings";
$MESS["XDEV_CP_EXPORT_TAB2"] = "Elements";
$MESS["XDEV_CP_EXPORT_TAB2_TITLE"] = "Elements";

$MESS["XDEV_CP_EXPORT_BACK_TO_ADMIN"] = "Go back to list";
$MESS["XDEV_CP_EXPORT_NEW_RECORD"] = "New profile";
$MESS["XDEV_CP_EXPORT_DELETE_RECORD"] = "Delete profile";
$MESS["XDEV_CP_EXPORT_DELETE_RECORD_CONFIRM"] = "Warning! All data will be lost! Continue?";
$MESS["XDEV_CP_EXPORT_EDIT_TITLE"] = "Profile ##ID#";
$MESS["XDEV_CP_EXPORT_NEW_TITLE"] = "New profile";
$MESS["XDEV_CP_EXPORT_START"] = "Start export";
$MESS["XDEV_CP_EXPORT_STOP"] = "Stop export";

$MESS["XDEV_CP_EXPORT_PROGRESS_TITLE"] = "Progress";
$MESS["XDEV_CP_EXPORT_DONE_TITLE"] = "Progress";

$MESS["XDEV_CP_EXPORT_DROP_TEMPORARY_SUCCESS"] = "Termporary tables was dropped";

$MESS["XDEV_CP_EXPORT_INIT"] = "Initialization";
$MESS["XDEV_CP_EXPORT_INIT_SUCCESS"] = "Initialization";

$MESS["XDEV_CP_EXPORT_S"] = "Sections progress";
$MESS["XDEV_CP_EXPORT_S_PROGRESS"] = "Sections progress #PROGRESS_VALUE# from #PROGRESS_TOTAL#";
$MESS["XDEV_CP_EXPORT_S_SUCCESS"] = "Sections was exported";
$MESS["XDEV_CP_EXPORT_S_STATUS"] = "Success sections exported <b>#TOTAL#</b> with errors <b>#ERRORS#</b>";

$MESS["XDEV_CP_EXPORT_P"] = "Properties progress";
$MESS["XDEV_CP_EXPORT_P_PROGRESS"] = "Properties progress #PROGRESS_VALUE# from #PROGRESS_TOTAL#";
$MESS["XDEV_CP_EXPORT_P_SUCCESS"] = "Properties was exported";
$MESS["XDEV_CP_EXPORT_P_STATUS"] = "Success properties exported <b>#TOTAL#</b> with errors <b>#ERRORS#</b>";

$MESS["XDEV_CP_EXPORT_E"] = "Elements progress";
$MESS["XDEV_CP_EXPORT_E_PROGRESS"] = "Elements progress #PROGRESS_VALUE# from #PROGRESS_TOTAL#";
$MESS["XDEV_CP_EXPORT_E_SUCCESS"] = "Elements was exported";
$MESS["XDEV_CP_EXPORT_E_STATUS"] = "Success elements exported <b>#TOTAL#</b> with errors <b>#ERRORS#</b>";

$MESS["XDEV_CP_EXPORT_INTERVAL"] = "Step timeout (0 - do export in one step)";
?>