<?
$MESS["XDEV_CP_PROFILE_TAB1"] = "Settings";
$MESS["XDEV_CP_PROFILE_TAB1_TITLE"] = "Base settings";

$MESS["XDEV_CP_PROFILE_BACK_TO_ADMIN"] = "Go back to list";
$MESS["XDEV_CP_PROFILE_NEW_RECORD"] = "New record";
$MESS["XDEV_CP_PROFILE_NEW_PAGE_RECORD"] = "New page";
$MESS["XDEV_CP_PROFILE_DELETE_RECORD"] = "Delete record";
$MESS["XDEV_CP_PROFILE_DELETE_RECORD_CONFIRM"] = "Warning! All data will be lost! Continue?";
$MESS["XDEV_CP_PROFILE_EDIT_TITLE"] = "Site ##ID#";
$MESS["XDEV_CP_PROFILE_NEW_TITLE"] = "New site";
?>