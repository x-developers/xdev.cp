<?
$MESS["XDEV_CP_EXPORT_LIST_ADD_RECORD"] = "New profile";
$MESS["XDEV_CP_EXPORT_LIST_ADD_RECORD_TITLE"] = "Add new profile";
$MESS["XDEV_CP_EXPORT_LIST_DELETE_RECORD_CONFIRM"] = "Warning! All data will be lost! Continue?";
$MESS["XDEV_CP_EXPORT_LIST_NEW_ENTITY"] = "New entity";
$MESS["XDEV_CP_EXPORT_LIST_QUEUE_VIEW"] = "Queue list";
$MESS["XDEV_CP_EXPORT_TITLE_ADMIN"] = "Export profiles";
$MESS["XDEV_CP_EXPORT_LIST_STOP"] = "start";
$MESS["XDEV_CP_EXPORT_LIST_START"] = "stop";
?>