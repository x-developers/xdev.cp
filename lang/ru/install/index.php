<?
$MESS["XDEV_CP_MODULE_NAME"] = "Парсер";
$MESS["XDEV_CP_MODULE_DESC"] = "Html парсер";
$MESS["XDEV_CP_PARTNER_NAME"] = "X Developers";
$MESS["XDEV_CP_PARTNER_URI"] = "http://x-dev.ru";
$MESS["XDEV_CP_ACCESS_DENIED"] = "Доступ запрещен";
$MESS["XDEV_CP_ACCESS_MODERATION"] = "Изменение";
$MESS["XDEV_CP_ACCESS_FULL"] = "Полный доступ";
$MESS["XDEV_CP_INSTALL_TITLE"] = "Установка модуля";
$MESS["XDEV_CP_UNINSTALL_TITLE"] = "Удаление модуля";
?>