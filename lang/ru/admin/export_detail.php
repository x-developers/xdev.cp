<?
$MESS["XDEV_CP_EXPORT_TAB1"] = "Параметры";
$MESS["XDEV_CP_EXPORT_TAB1_TITLE"] = "Основные параметры";
$MESS["XDEV_CP_EXPORT_TAB2"] = "Элементы";
$MESS["XDEV_CP_EXPORT_TAB2_TITLE"] = "Элементы";

$MESS["XDEV_CP_EXPORT_BACK_TO_ADMIN"] = "Вернуться в список";
$MESS["XDEV_CP_EXPORT_NEW_RECORD"] = "Новый профиль";
$MESS["XDEV_CP_EXPORT_DELETE_RECORD"] = "Удалить профиль";
$MESS["XDEV_CP_EXPORT_DELETE_RECORD_CONFIRM"] = "Внимание! Это действие не обратимо! Продолжить?";
$MESS["XDEV_CP_EXPORT_EDIT_TITLE"] = "Профиль экспорта ##ID#";
$MESS["XDEV_CP_EXPORT_NEW_TITLE"] = "Новый профиль";
$MESS["XDEV_CP_EXPORT_IBLOCK_LIST_TITLE"] = "Посмотреть список";
$MESS["XDEV_CP_EXPORT_START"] = "Запустить экспорт";
$MESS["XDEV_CP_EXPORT_STOP"] = "Остановить экспорт";

$MESS["XDEV_CP_EXPORT_PROGRESS_TITLE"] = "Обработка очереди";
$MESS["XDEV_CP_EXPORT_DONE_TITLE"] = "Обработка завершена";

$MESS["XDEV_CP_EXPORT_DROP_TEMPORARY_SUCCESS"] = "Временные таблицы очищены";

$MESS["XDEV_CP_EXPORT_INIT"] = "Инициализация";
$MESS["XDEV_CP_EXPORT_INIT_SUCCESS"] = "Инициализация";

$MESS["XDEV_CP_EXPORT_S"] = "Обработка разделов";
$MESS["XDEV_CP_EXPORT_S_PROGRESS"] = "Обработано разделов #PROGRESS_VALUE# из #PROGRESS_TOTAL#";
$MESS["XDEV_CP_EXPORT_S_SUCCESS"] = "Разделы обработаны";
$MESS["XDEV_CP_EXPORT_S_STATUS"] = "Успешно экспортировано разделов <b>#TOTAL#</b> с ошибками <b>#ERRORS#</b>";

$MESS["XDEV_CP_EXPORT_P"] = "Обработка свойств";
$MESS["XDEV_CP_EXPORT_P_PROGRESS"] = "Обработано свойств #PROGRESS_VALUE# из #PROGRESS_TOTAL#";
$MESS["XDEV_CP_EXPORT_P_SUCCESS"] = "Свойства обработаны";
$MESS["XDEV_CP_EXPORT_P_STATUS"] = "Успешно экспортировано свойств <b>#TOTAL#</b> с ошибками <b>#ERRORS#</b>";

$MESS["XDEV_CP_EXPORT_E"] = "Обработка товаров";
$MESS["XDEV_CP_EXPORT_E_PROGRESS"] = "Обработано товаров #PROGRESS_VALUE# из #PROGRESS_TOTAL#";
$MESS["XDEV_CP_EXPORT_E_SUCCESS"] = "Товары обработаны";
$MESS["XDEV_CP_EXPORT_E_STATUS"] = "Успешно экспортировано товаров <b>#TOTAL#</b> с ошибками <b>#ERRORS#</b>";

$MESS["XDEV_CP_EXPORT_INTERVAL"] = "Длительность шага в секундах (0 - выполнять экспорт за один шаг)";
?>