<?
$MESS["XDEV_CP_EXPORT_ENTITY_TAB1"] = "Параметры";
$MESS["XDEV_CP_EXPORT_ENTITY_TAB1_TITLE"] = "Основные параметры";

$MESS["XDEV_CP_EXPORT_ENTITY_TAB2"] = "Элементы";
$MESS["XDEV_CP_EXPORT_ENTITY_TAB2_TITLE"] = "Элементы страницы";

$MESS["XDEV_CP_EXPORT_ENTITY_BACK_TO_ADMIN"] = "Вернуться в список";
$MESS["XDEV_CP_EXPORT_ENTITY_NEW_RECORD"] = "Новый элемент";
$MESS["XDEV_CP_EXPORT_ENTITY_DELETE_RECORD"] = "Удалить элемент";
$MESS["XDEV_CP_EXPORT_ENTITY_DELETE_RECORD_CONFIRM"] = "Внимание! Это действие не обратимо! Продолжить?";
$MESS["XDEV_CP_EXPORT_ENTITY_EDIT_TITLE"] = "Элемент ##ID#";
$MESS["XDEV_CP_EXPORT_ENTITY_NEW_TITLE"] = "Новый элемент";
$MESS["XDEV_CP_EXPORT_ENTITY_NEW_CHILD_RECORD"] = "Добавить дочерний элемент";
?>
