<?
$MESS["XDEV_CP_PAGE_TAB1"] = "Параметры";
$MESS["XDEV_CP_PAGE_TAB1_TITLE"] = "Основные параметры";

$MESS["XDEV_CP_PAGE_TAB2"] = "Элементы";
$MESS["XDEV_CP_PAGE_TAB2_TITLE"] = "Элементы страницы";

$MESS["XDEV_CP_PAGE_BACK_TO_ADMIN"] = "Вернуться в список";
$MESS["XDEV_CP_PAGE_NEW_RECORD"] = "Новая страница";
$MESS["XDEV_CP_PAGE_NEW_NODE_RECORD"] = "Добавить элемент";
$MESS["XDEV_CP_PAGE_DELETE_RECORD"] = "Удалить страницу";
$MESS["XDEV_CP_PAGE_DELETE_RECORD_CONFIRM"] = "Внимание! Это действие не обратимо! Продолжить?";
$MESS["XDEV_CP_PAGE_EDIT_TITLE"] = "Страница ##ID#";
$MESS["XDEV_CP_PAGE_NEW_TITLE"] = "Новая страница";
$MESS["XDEV_CP_PAGE_NODE_LIST"] = "Список узлов";
$MESS["XDEV_CP_PAGE_NEW_SHEDULE"] = "Добавить в очередь";
$MESS["XDEV_CP_PAGE_CHOOSE_PROFILE_ID"] = "(выберите профиль)";
?>