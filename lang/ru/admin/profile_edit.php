<?
$MESS["XDEV_CP_PROFILE_TAB1"] = "Параметры";
$MESS["XDEV_CP_PROFILE_TAB1_TITLE"] = "Основные параметры";

$MESS["XDEV_CP_PROFILE_BACK_TO_ADMIN"] = "Вернуться в список";
$MESS["XDEV_CP_PROFILE_NEW_RECORD"] = "Новый сайт";
$MESS["XDEV_CP_PROFILE_NEW_PAGE_RECORD"] = "Добавить страницу";
$MESS["XDEV_CP_PROFILE_DELETE_RECORD"] = "Удалить сайт";
$MESS["XDEV_CP_PROFILE_DELETE_RECORD_CONFIRM"] = "Внимание! Это действие не обратимо! Продолжить?";
$MESS["XDEV_CP_PROFILE_EDIT_TITLE"] = "Сайт ##ID#";
$MESS["XDEV_CP_PROFILE_NEW_TITLE"] = "Новый сайт";
$MESS["XDEV_CP_PROFILE_SELECTOR_NOTES"] = "<span class=\"required\">Внимание!</span> Значения всех текстовых полей являются <a href=\"http://jquery.com\" target=\"_blank\">jQuery</a> селекторами";
?>