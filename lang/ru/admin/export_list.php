<?
$MESS["XDEV_CP_EXPORT_LIST_ADD_RECORD"] = "Добавить профиль";
$MESS["XDEV_CP_EXPORT_LIST_ADD_RECORD_TITLE"] = "Добавить новый профиль";
$MESS["XDEV_CP_EXPORT_LIST_DELETE_RECORD_CONFIRM"] = "Внимание! Это действие не обратимо! Продолжить?";
$MESS["XDEV_CP_EXPORT_LIST_NEW_ENTITY"] = "Добавить сущность";
$MESS["XDEV_CP_EXPORT_LIST_QUEUE_VIEW"] = "Посмотреть очередь";
$MESS["XDEV_CP_EXPORT_TITLE_ADMIN"] = "Профили экспорта";
$MESS["XDEV_CP_EXPORT_LIST_STOP"] = "остановить";
$MESS["XDEV_CP_EXPORT_LIST_START"] = "запустить";
?>