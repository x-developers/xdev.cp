<?
$MESS["XDEV_CP_SHEDULE_TAB1"] = "Параметры";
$MESS["XDEV_CP_SHEDULE_TAB1_TITLE"] = "Основные параметры";

$MESS["XDEV_CP_SHEDULE_BACK_TO_ADMIN"] = "Вернуться в список";
$MESS["XDEV_CP_SHEDULE_NEW_RECORD"] = "Новое задание";
$MESS["XDEV_CP_SHEDULE_DELETE_RECORD"] = "Удалить задание";
$MESS["XDEV_CP_SHEDULE_DELETE_RECORD_CONFIRM"] = "Внимание! Это действие не обратимо! Продолжить?";
$MESS["XDEV_CP_SHEDULE_EDIT_TITLE"] = "Задание ##ID#";
$MESS["XDEV_CP_SHEDULE_NEW_TITLE"] = "Новое задание";
$MESS["XDEV_CP_SHEDULE_EXPORT_ADD"] = "Добавить профиль экспорта";

$MESS["XDEV_CP_SHEDULE_PROGRESS_TITLE"] = "Обрабока очереди";
$MESS["XDEV_CP_SHEDULE_STEP_RESET"] = "Очистка очереди";
$MESS["XDEV_CP_SHEDULE_STEP_RESET_SUCCESS"] = "Очередь очищена";
$MESS["XDEV_CP_SHEDULE_STEP_START"] = "Запуск очереди";
$MESS["XDEV_CP_SHEDULE_STEP_START_SUCCESS"] = "Очередь запущена";
$MESS["XDEV_CP_SHEDULE_STEP_EXECUTE"] = "Обработка очереди";
$MESS["XDEV_CP_SHEDULE_STEP_EXECUTE_PROGRESS"] = "Обработано страниц #PROGRESS_VALUE#";
$MESS["XDEV_CP_SHEDULE_STEP_EXECUTE_SUCCESS"] = "Все страницы обработаны";
$MESS["XDEV_CP_SHEDULE_STEP_REINDEX"] = "Переиндексация";
$MESS["XDEV_CP_SHEDULE_STEP_REINDEX_PROGRESS"] = "Проиндексировано #PROGRESS_VALUE# из #PROGRESS_TOTAL# записей";
$MESS["XDEV_CP_SHEDULE_STEP_REINDEX_SUCCESS"] = "Переиндексация выполнена";

$MESS["XDEV_CP_SHEDULE_SUCCESS_TITLE"] = "Очередь обработана";
$MESS["XDEV_CP_SHEDULE_TOTAL_PAGES"] = "Обработано страниц #COUNT#";
$MESS["XDEV_CP_SHEDULE_TOTAL_SUCCESS"] = "Успешно #COUNT#";
$MESS["XDEV_CP_SHEDULE_TOTAL_ERRORS"] = "С ошибками #COUNT#";
$MESS["XDEV_CP_SHEDULE_DETAIL_URL"] = "Посмотреть список";
$MESS["XDEV_CP_QUEUE_SHEDULE_INTERVAL"] = "Длительность шага в секундах (0 - выполнять импорт за один шаг)";

$MESS["XDEV_CP_QUEUE_SHEDULE_START_BUTTON"] = "Запустить импорт";
$MESS["XDEV_CP_QUEUE_SHEDULE_STOP_BUTTON"] = "Остановить импорт";
?>