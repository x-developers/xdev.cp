<?
$MESS["XDEV_CP_SHEDULE_LIST_ADD_RECORD"] = "Добавить задание";
$MESS["XDEV_CP_SHEDULE_LIST_ADD_RECORD_TITLE"] = "Добавить новое задание";
$MESS["XDEV_CP_SHEDULE_LIST_DELETE_RECORD_CONFIRM"] = "Внимание! Это действие не обратимо! Продолжить?";
$MESS["XDEV_CP_SHEDULE_LIST_QUEUE_VIEW"] = "Страницы в очереди";
$MESS["XDEV_CP_SHEDULE_TITLE_ADMIN"] = "Очередь загрузки";
$MESS["XDEV_CP_SHEDULE_LIST_STOP"] = "очистить";
$MESS["XDEV_CP_SHEDULE_LIST_START"] = "запустить";
$MESS["XDEV_CP_SHEDULE_LIST_EXPORT_ADD"] = "Добавить профиль экспорта";
?>