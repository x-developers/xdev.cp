<?
$MESS["XDEV_CP_UAGENT_TAB1"] = "Параметры";
$MESS["XDEV_CP_UAGENT_TAB1_TITLE"] = "Основные параметры";

$MESS["XDEV_CP_UAGENT_BACK_TO_ADMIN"] = "Вернуться в список";
$MESS["XDEV_CP_UAGENT_NEW_RECORD"] = "Новая запись";
$MESS["XDEV_CP_UAGENT_DELETE_RECORD"] = "Удалить";
$MESS["XDEV_CP_UAGENT_DELETE_RECORD_CONFIRM"] = "Внимание! Это действие не обратимо! Продолжить?";
$MESS["XDEV_CP_UAGENT_EDIT_TITLE"] = "User agent ##ID#";
$MESS["XDEV_CP_UAGENT_NEW_TITLE"] = "Новая запись";
?>