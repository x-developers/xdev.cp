<?
$MESS["XDEV_CP_NODE_TAB1"] = "Параметры";
$MESS["XDEV_CP_NODE_TAB1_TITLE"] = "Основные параметры";

$MESS["XDEV_CP_NODE_BACK_TO_ADMIN"] = "Вернуться в список";
$MESS["XDEV_CP_NODE_NEW_RECORD"] = "Новый элемент";
$MESS["XDEV_CP_NODE_NEW_PARENT_RECORD"] = "Добавить дочерний элемент";
$MESS["XDEV_CP_NODE_DELETE_RECORD"] = "Удалить элемент";
$MESS["XDEV_CP_NODE_DELETE_RECORD_CONFIRM"] = "Внимание! Это действие не обратимо! Продолжить?";
$MESS["XDEV_CP_NODE_EDIT_TITLE"] = "Элемент ##ID#";
$MESS["XDEV_CP_NODE_NEW_TITLE"] = "Новый элемент";
$MESS["XDEV_CP_NODE_PAGE_404"] = "Страница не найдена";
?>
