<?php

use Xdev\cp\Import;

$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../../../..");
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define("BX_CRONTAB", true);
define('BX_NO_ACCELERATOR_RESET', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

function export(&$NS)
{
    $import = new Import\Iblock($NS);
    if ($NS["STEP"] == 1) {
        $import->reset();
        $NS["STEP"]++;
    } elseif ($NS["STEP"] == 2) {
        $import->init();
        $NS["STEP"]++;
    } elseif ($NS["STEP"] == 3) {
        $res = $import->import();

        $counter = 0;
        foreach ($res as $key => $val) {
            $counter += count($val);
        }

        if (!$counter) {
            if (!$import->getNext()) {
                $NS["STEP"]++;
            }
        }
    } elseif ($NS["STEP"] == 4) {
        $NS["STEP"]++;
    } elseif ($NS["STEP"] == 5) {
        $NS["STEP"]++;
    }

    return $NS;
}

if (CModule::IncludeModule("xdev.cp")) {
    $NS = [
        "STEP" => 1,
        "ID" => intval($_SERVER["argv"][1]),
        "INTERVAL" => 0,
        "DATA" => [],
    ];

    foreach (["S", "E", "P"] as $val) {
        $NS["DATA"][$val] = [];
    }

    while ($NS['STEP'] < 5) {
        $NS = export($NS);
    }
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
