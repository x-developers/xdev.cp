<?php
use \Xdev\Cp\Queue;
$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../../../..");
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("BX_CRONTAB", true);
define('BX_NO_ACCELERATOR_RESET', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
// @ignore_user_abort(true);

sleep(10);

if(CModule::IncludeModule("xdev.cp"))
{
	$parameters = Queue\Threads::getParams();
	if(!is_array($parameters))
		die("Access denied");

	$id = intval($parameters["id"]);
	switch($parameters["mode"])
	{
		case "index":
			// sleep(5);
			echo \Xdev\Cp\Spider\ResultNodeTable::treeReSort($id);
			break;
	}

	// echo "OK";
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
