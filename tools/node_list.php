<?
/** @global CUser $USER */

/** @global CMain $APPLICATION */

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

use \Xdev\Cp\Spider\NodeManager;

if (!defined('B_ADMIN_SUBCOUPONS') || B_ADMIN_SUBCOUPONS != 1 || !defined('B_ADMIN_SUBCOUPONS_LIST')) {
    return;
}

$prologAbsent = (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true);
if (B_ADMIN_SUBCOUPONS_LIST === false && $prologAbsent) {
    return;
}

Bitrix\Main\Loader::includeModule('xdev.cp');

if ($prologAbsent) {
    require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');
    CUtil::JSPostUnescape();

    $pageID = 0;
    if (isset($_REQUEST['find_page_id'])) {
        $pageID = (int) $_REQUEST['find_page_id'];
    }

    $nodesAjaxPath = '/bitrix/tools/xdev.cp/node_list.php?lang='.LANGUAGE_ID.'&find_page_id='.$pageID;
    $can_edit = $APPLICATION->GetGroupRight('xdev.cp') >= "W";
}
if (!isset($pageID) || $pageID <= 0 || !isset($nodesAjaxPath) || empty($nodesAjaxPath)) {
    return;
}

if (isset($_REQUEST['mode']) && ($_REQUEST['mode'] == 'list' || $_REQUEST['mode'] == 'frame')) {
    CFile::DisableJSFunction(true);
}

Loc::loadMessages(__FILE__);

$strEditPath = "/bitrix/admin/xdev_cp_node_edit.php?lang=".LANGUAGE_ID;

$sDataClassName = "xdev\cp\Profile\NodeTable";

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/iblock/classes/general/subelement.php');

CJSCore::Init(['date']);

$sTableID = preg_replace("/\\\+/", "_", $sDataClassName)."_list";

$arRegisteredNodes = Xdev\Cp\Spider\NodeManager::getRegisteredNodes();

$arHeaders = $arFilterFields = $arInitFilter = [];
$sPrimaryKey = "ID";
foreach ($sDataClassName::getEntity()->getFields() as $field) {
    if (!$field instanceof \Bitrix\Main\Entity\ReferenceField) {
        $id = $field->getColumnName();
        if (in_array($id, ["ID", "PARENT_ID", "TO_PAGE_ID", "TYPE", "CSS_SELECTOR", "NAME"])) {
            if ($field->isPrimary()) {
                $sPrimaryKey = $field->getColumnName();
            }

            if (strlen($field->getTitle())) {
                $title = GetMessage(sprintf("XDEV_CP_PAGE_%s_FIELD_ADMIN", $field->getName()));
                $arHeaders[] = [
                    "id" => $field->getName(),
                    "content" => strlen($title) ? $title : $field->getTitle(),
                    "sort" => $field->getName(),
                    "default" => true,
                ];

                if ($field instanceof Bitrix\Main\Entity\DatetimeField) {
                    $arInitFilter[$id."_FROM"] = "find_".$id."_from";
                    $arInitFilter[$id."_TO"] = "find_".$id."_to";
                    $arFilterFields["find_".$id."_from"] = $id;
                    $arFilterFields["find_".$id."_to"] = $id;
                } else {
                    $arInitFilter[$id] = "find_".$id;
                    $arFilterFields["find_".$id] = $id;
                }
            }
        }
    }
}

$lAdmin = new CAdminSubList(
    $sTableID,
    new CAdminSubSorting($sTableID, $sPrimaryKey, $by_name = "by", $ord_name = "order", $list_url, "asc"),
    $nodesAjaxPath
);
// FILTER
$lAdmin->InitFilter($arInitFilter);

$arFilter = [
    "PAGE_ID" => $pageID,
];
foreach ($sDataClassName::getEntity()->getFields() as $field) {
    if ($field instanceof Bitrix\Main\Entity\ScalarField) {
        $id = $field->getColumnName();
        $fieldName = "find_".$id;

        if ($field instanceof Bitrix\Main\Entity\DatetimeField) {
            if (!empty(${$fieldName."_from"})) {
                $arFilter[">=".$id] = ${$fieldName."_from"};
            }
            if (!empty(${$fieldName."_to"})) {
                $arFilter["<=".$id] = ${$fieldName."_to"};
            }
        } else {
            if (!empty(${$fieldName})) {
                $arFilter[$id] = ${$fieldName};
            }
        }
    }
}

// ACTIONS
if ($lAdmin->EditAction() && $can_edit) {
    foreach ($FIELDS as $ID => $arFields) {
        if (!$lAdmin->IsUpdated($ID)) {
            continue;
        }

        $errors = "";
        $result = $sDataClassName::update($ID, $arFields);
        if (!$result->isSuccess()) {
            foreach ($result->getErrors() as $error) {
                $errors .= $error->getMessage()."<br>";
            }
        }
        if (!empty($errors)) {
            $lAdmin->AddUpdateError($errors, $ID);
        }
    }
}

if ($arID = $lAdmin->GroupAction()) {
    if ($_REQUEST["action_target"] == "selected") {
        $arID = [];
        $rs = $sDataClassName::getList(
            [
                "select" => ["ID"],
                "filter" => $arFilter,
            ]
        );
        while ($ar = $rs->Fetch()) {
            $arID[] = $ar["ID"];
        }
    }

    foreach ($arID as $ID) {
        $ID = IntVal($ID);
        if ($ID <= 0) {
            continue;
        }

        $result = new \Bitrix\Main\Entity\Result();
        switch ($_REQUEST["action"]) {
            case "delete":
                if ($can_edit) {
                    $result = $sDataClassName::delete($ID);
                }
                break;
        }

        if (!$result->isSuccess()) {
            $arErrors = [];
            foreach ($result->getErrors() as $error) {
                $arErrors[] = $error->getMessage();
            }
            $lAdmin->AddGroupError(join("<br>", $arErrors), $ID);
        }
    }
}

$lAdmin->AddHeaders($arHeaders);

$arSelect = [];

foreach ($arHeaders as $val) {
    if (in_array($val["id"], $lAdmin->GetVisibleHeaderColumns())) {
        $arSelect[$val["id"]] = (is_set($val, "sort") ? $val["sort"] : $val["id"]);
    }
}

if (!in_array($sPrimaryKey, $arSelect)) {
    $arSelect[$sPrimaryKey] = $sPrimaryKey;
}

$rsData = $sDataClassName::getList(
    [
        "select" => $arSelect,
        "filter" => $arFilter,
        //	"order" => array(
        //		$by => strtoupper($order)
        //	),
    ]
);

$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();

$lAdmin->NavText($rsData->GetNavPrint($title));

while ($data = $rsData->NavNext(true, "f_")) {
    $arActions = [];
    $row = &$lAdmin->AddRow(
        $f_ID,
        $data,
        $strEditPath."&ID=".$f_ID,
        GetMessage("MAIN_ADMIN_MENU_EDIT"),
        true
    );

    foreach ($sDataClassName::getEntity()->getFields() as $field) {
        if (
            ($field instanceof Bitrix\Main\Entity\ScalarField)
            && in_array($field->getColumnName(), $lAdmin->GetVisibleHeaderColumns())
            && !$field->isPrimary()
        ) {
            switch ($field->getColumnName()) {
                case "TYPE":
                    $arType = NodeManager::getRegisteredNodeById($data["TYPE"]);
                    $row->AddViewField($field->getColumnName(), $arType["NAME"]);
                    break;

                default:
                    if ($can_edit) {
                        switch ($field->getDataType()) {
                            case "string":
                            case "integer":
                                $row->AddInputField($field->getColumnName());
                                break;

                            case "boolean":
                                $vals = [];
                                foreach ($field->getValues() as $val) {
                                    $vals[$val] = $val;
                                }

                                if (count(array_intersect($vals, ["Y", "N"])) == 2) {
                                    $row->AddCheckField($field->getColumnName());
                                } else {
                                    $row->AddSelectField($field->getColumnName(), $vals);
                                }
                                break;
                        }
                    } else {
                        switch ($field->getDataType()) {
                            case "boolean":
                                $vals = [];
                                foreach ($field->getValues() as $val) {
                                    $vals[$val] = $val;
                                }

                                if (count(array_intersect($vals, ["Y", "N"])) == 2) {
                                    $row->AddViewField(
                                        $field->getColumnName(), GetMessage($data[$field->getColumnName()] == "Y" ? "MAIN_YES" : "MAIN_NO")
                                    );
                                }
                                break;
                        }
                    }
                    break;
            }
        }
    }

    if ($can_edit) {
        $arActions[] = [
            'ICON' => 'edit',
            'TEXT' => Loc::getMessage('MAIN_ADMIN_MENU_EDIT'),
            'ACTION' => "(new BX.CAdminDialog({
					'content_url': '".CUtil::JSEscape($strEditPath)."&ID=".$f_ID."',
					'content_post': 'bxpublic=Y',
					'draggable': true,
					'resizable': true,
					'buttons': [BX.CAdminDialog.btnSave, BX.CAdminDialog.btnCancel]
				})).Show();",
            'DEFAULT' => true,
        ];
        $arActions[] = ["SEPARATOR" => "Y"];
        $arActions[] = [
            "ICON" => "delete",
            "TEXT" => GetMessage("MAIN_ADMIN_MENU_DELETE"),
            "ACTION" => "if(confirm('".GetMessageJS("XDEV_CP_NODE_LIST_DELETE_RECORD_CONFIRM")."')) ".$lAdmin->ActionDoGroup(
                    $f_ID, "delete", "&lang=".LANGUAGE_ID
                ),
        ];
    }

    $row->AddActions($arActions);
}

$lAdmin->AddFooter(
    [
        ["title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value" => $rsData->SelectedRowsCount()],
        ["counter" => true, "title" => GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value" => "0"],
    ]
);

$arParams = [];
$arGroupActions = [];
if ($can_edit) {
    $arGroupActions = [
        "delete" => true,
    ];
}
$lAdmin->AddGroupActionTable($arGroupActions, $arParams);

if (!isset($_REQUEST["mode"]) || ($_REQUEST["mode"] != 'excel' && $_REQUEST["mode"] != 'subsettings')) {
    ?>
    <script type="text/javascript">
    function ShowNewNode (id, type) {
        var PostParams = {
            lang    : '<? echo LANGUAGE_ID; ?>',
            PAGE_ID : id,
            TYPE    : type,
            ID      : 0,
            bxpublic: 'Y',
            sessid  : BX.bitrix_sessid(),
        };
        (new BX.CAdminDialog({
            'content_url' : '/bitrix/admin/xdev_cp_node_edit.php',
            'content_post': PostParams,
            'draggable'   : true,
            'resizable'   : true,
            'buttons'     : [BX.CAdminDialog.btnSave, BX.CAdminDialog.btnCancel],
        })).Show();
    }
    </script><?

    $aContext = [];
    if (!$nodesReadOnly) {
        $addSubMenu = [];
        foreach ($arRegisteredNodes as $arRegisteredNode) {
            $addSubMenu[] = [
                'TEXT' => $arRegisteredNode["NAME"],
                'TITLE' => $arRegisteredNode["NAME"],
                'LINK' => "javascript:ShowNewNode(".$pageID.", '".$arRegisteredNode["TYPE"]."')",
                'SHOW_TITLE' => true,
            ];
        }

        $aContext[] = [
            'TEXT' => Loc::getMessage('XDEV_CP_PROFILE_NODE_INSERT_NEW'),
            'TITLE' => Loc::getMessage('XDEV_CP_PROFILE_NODE_INSERT_NEW_TITLE'),
            'ICON' => 'btn_new',
            'MENU' => $addSubMenu,
        ];
    }
    $aContext[] = [
        'TEXT' => htmlspecialcharsex(Loc::getMessage('XDEV_CP_PROFILE_NODE_LIST_REFRESH')),
        'TITLE' => Loc::getMessage('XDEV_CP_PROFILE_NODE_LIST_REFRESH'),
        'ICON' => 'btn_sub_refresh',
        'LINK' => "javascript:".$lAdmin->ActionAjaxReload($lAdmin->GetListUrl(true)),
    ];

    $lAdmin->AddAdminContextMenu($aContext);
}
$lAdmin->CheckListMode();

$lAdmin->DisplayList(B_ADMIN_SUBCOUPONS_LIST);

if ($prologAbsent) {
    require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_popup_admin.php');
}