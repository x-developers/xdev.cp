<?
namespace Xdev\Cp\Queue;

use \Bitrix\Main\ArgumentException;
use \Bitrix\Main\Entity\EntityError as ErrorMessage;
use \Bitrix\Main\Entity\Event;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Type\DateTime;

use \Xdev\Cp\Admin\Tools;
use Xdev\Cp\Internal\DataManager;
use \Xdev\Cp\Internal\ListResult;
use \Xdev\Cp\Profile\ProfileTable;
use Xdev\Cp\Spider\ResultNodeTable;

Loc::loadMessages(__FILE__);

class SheduleTable extends DataManager
{
    const STATUS_NEW = "N";
    const STATUS_EXECUTE = "P";
    const STATUS_SUCCESS = "Y";
    const STATUS_FAIL = "F";
    const STATUS_REINDEX = "I";

    public static function getFilePath()
    {
        return __FILE__;
    }

    public static function getTableName()
    {
        return "xdev_cp_queue_shedule";
    }

    public static function getMap()
    {
        return [
            "ID" => [
                "data_type" => "integer",
                "primary" => true,
                "autocomplete" => true,
                "title" => Loc::getMessage("XDEV_CP_ID_FIELD"),
            ],
            "CACHE_TYPE" => [
                "data_type" => "enum",
                "values" => ["E", "D", "U"],
                "default" => "E",
                "title" => Loc::getMessage("XDEV_CP_CACHE_TYPE_FIELD"),
            ],
            "ACTIVE" => [
                "data_type" => "boolean",
                "values" => ["Y", "N"],
                "title" => Loc::getMessage("XDEV_CP_ACTIVE_FIELD"),
            ],
            "STATUS_ID" => [
                "data_type" => "boolean",
                "values" => array_keys(static::getStatusArray()),
                "title" => Loc::getMessage("XDEV_CP_SHEDULE_STATUS_ID_FIELD"),
            ],
            "STATUS_DATE" => [
                "data_type" => "datetime",
                "title" => Loc::getMessage("XDEV_CP_SHEDULE_STATUS_DATE_FIELD"),
            ],
            "PROFILE_ID" => [
                "data_type" => "integer",
                "required" => true,
                "title" => Loc::getMessage("XDEV_CP_SHEDULE_PROFILE_ID_FIELD"),
            ],
            "PROFILE" => [
                "data_type" => "\\xdev\\cp\\Profile\\Profile",
                "reference" => ["=this.PROFILE_ID" => "ref.ID"],
            ],
            "NEXT_EXEC" => [
                "data_type" => "datetime",
                "title" => Loc::getMessage("XDEV_CP_SHEDULE_NEXT_EXEC_FIELD"),
            ],
            "LAST_EXEC" => [
                "data_type" => "datetime",
                "title" => Loc::getMessage("XDEV_CP_SHEDULE_LAST_EXEC_FIELD"),
            ],
            "DATE_CREATE" => [
                "data_type" => "datetime",
                "default_value" => new DateTime(),
                "title" => Loc::getMessage("XDEV_CP_DATE_CREATE_FIELD"),
            ],
            "TIMESTAMP_X" => [
                "data_type" => "datetime",
                "default_value" => new DateTime(),
                "title" => Loc::getMessage("XDEV_CP_TIMESTAMP_X_FIELD"),
            ],
            "START_PAGE_ID" => [
                "data_type" => "string",
                "required" => true,
                "title" => Loc::getMessage("XDEV_CP_SHEDULE_START_PAGE_ID_FIELD"),
            ],
            "FULL_URL" => [
                "data_type" => "string",
                "title" => Loc::getMessage("XDEV_CP_QUEUE_SHEDULE_FULL_URL_FIELD"),
                "expression" => [
                    "(SELECT CONCAT(PROTO, '://', HOSTNAME) from `xdev_cp_prof_profile` WHERE (`xdev_cp_prof_profile`.ID = %s))",
                    "PROFILE_ID",
                ],
            ],
            "START_URL" => [
                "data_type" => "string",
                "required" => true,
                "title" => Loc::getMessage("XDEV_CP_QUEUE_SHEDULE_START_URL_FIELD"),
                "validation" => [__CLASS__, "validateUrl"],
            ],
            "START_FULL_URL" => [
                "data_type" => "string",
                "title" => Loc::getMessage("XDEV_CP_QUEUE_SHEDULE_FULL_URL_FIELD"),
                "expression" => [
                    "(SELECT CONCAT(PROTO, '://', HOSTNAME, %s) from `xdev_cp_prof_profile` WHERE (`xdev_cp_prof_profile`.ID = %s))",
                    "START_URL",
                    "PROFILE_ID",
                ],
            ],
            "QUEUE_NEW" => [
                "data_type" => "string",
                "title" => Loc::getMessage("XDEV_CP_QUEUE_SHEDULE_QUEUE_NEW_FIELD"),
                "expression" => [
                    "(SELECT COUNT(1) from `xdev_cp_queue_page` WHERE (`xdev_cp_queue_page`.SHEDULE_ID = %s AND `xdev_cp_queue_page`.STATUS_ID='"
                    .QueueTable::STATUS_NEW
                    ."'))",
                    "ID",
                ],
            ],
            "QUEUE_SUCCESS" => [
                "data_type" => "string",
                "title" => Loc::getMessage("XDEV_CP_QUEUE_SHEDULE_QUEUE_SUCCESS_FIELD"),
                "expression" => [
                    "(SELECT COUNT(1) from `xdev_cp_queue_page` WHERE (`xdev_cp_queue_page`.SHEDULE_ID = %s AND `xdev_cp_queue_page`.STATUS_ID='"
                    .QueueTable::STATUS_SUCCESS
                    ."'))",
                    "ID",
                ],
            ],
            "QUEUE_FAIL" => [
                "data_type" => "string",
                "title" => Loc::getMessage("XDEV_CP_QUEUE_SHEDULE_QUEUE_FAIL_FIELD"),
                "expression" => [
                    "(SELECT COUNT(1) from `xdev_cp_queue_page` WHERE (`xdev_cp_queue_page`.SHEDULE_ID = %s AND `xdev_cp_queue_page`.STATUS_ID='"
                    .QueueTable::STATUS_FAIL
                    ."'))",
                    "ID",
                ],
            ],
            "QUEUE_CNT" => [
                "data_type" => "string",
                "title" => Loc::getMessage("XDEV_CP_QUEUE_SHEDULE_QUEUE_CNT_FIELD"),
                "expression" => [
                    "(SELECT COUNT(1) from `xdev_cp_queue_page` WHERE (`xdev_cp_queue_page`.SHEDULE_ID = %s))",
                    "ID",
                ],
            ],
            "QUEUE_BARCODE" => [
                "data_type" => "string",
                "title" => Loc::getMessage("XDEV_CP_QUEUE_SHEDULE_QUEUE_BARCODE_FIELD"),
                "expression" => [
                    "(SELECT COUNT(1) from `xdev_cp_queue_page` WHERE (`xdev_cp_queue_page`.SHEDULE_ID = %s AND `xdev_cp_queue_page`.BARCODE IS NOT NULL))",
                    "ID",
                ],
            ],
            "DESCRIPTION" => [
                "data_type" => "string",
                "title" => Loc::getMessage("XDEV_CP_QUEUE_SHEDULE_DESCRIPTION_FIELD"),
            ],
            "COOKIES" => [
                "data_type" => "string",
                "title" => Loc::getMessage("XDEV_CP_QUEUE_SHEDULE_COOKIES_FIELD"),
            ],
        ];
    }

    public static function validateUrl()
    {
        return [
            // new UniqueUrlValidator("PROFILE_ID"),
        ];
    }

    public static function getStatusArray()
    {
        $res = [];
        foreach (
            [
                static::STATUS_NEW,
                static::STATUS_EXECUTE,
                static::STATUS_FAIL,
                static::STATUS_SUCCESS,
                static::STATUS_REINDEX,
            ] as $val
        ) {
            $res[$val] = Loc::getMessage(sprintf("XDEV_CP_SHEDULE_STATUS_%s_FIELD", $val));
        }

        return $res;
    }

    /**
     * @param int    $id
     * @param string $status
     *
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function setStatus($id, $status)
    {
        $ar = static::getStatusArray();
        if (!is_set($ar, $status)) {
            throw new ArgumentException(sprintf("Shedule status #%s not found", $status), $status);
        }

        $ar = static::getRow(
            [
                "select" => [
                    "ID",
                    "STATUS_ID",
                ],
                "filter" => [
                    "ID" => $id,
                ],
            ]
        );
        if (!$ar) {
            throw new ArgumentException(sprintf("Shedule #%s not found", $id), $id);
        }

        if ($ar["STATUS_ID"] != $status) {
            static::update($id, ["STATUS_ID" => $status, "STATUS_DATE" => new DateTime()]);
        }

        return true;
    }

    /**
     * @param int $id
     *
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getStatus($id)
    {
        $ar = static::getRow(
            [
                "select" => ["STATUS_ID"],
                "filter" => ["ID" => intval($id)],
            ]
        );

        return $ar && $ar["STATUS_ID"] ? $ar["STATUS_ID"] : false;
    }

    public static function getLampStatus($status)
    {
        $ar = [
            static::STATUS_NEW => "grey",
            static::STATUS_EXECUTE => "yellow",
            static::STATUS_FAIL => "red",
            static::STATUS_SUCCESS => "green",
            static::STATUS_REINDEX => "yellow",
        ];

        return isset($ar[$status]) ? $ar[$status] : "grey";
    }

    public static function add(array $data)
    {
        if (isset($data["STATUS_ID"]) && !isset($data["STATUS_DATE"])) {
            $data["STATUS_DATE"] = new DateTime();
        }

        return parent::add($data);
    }

    public static function isActive($ID)
    {
        return !in_array(static::getStatus($ID), [static::STATUS_NEW]);
    }

    /**
     * Adds start page to queue
     *
     * @param integer $id shedule id
     *
     * @return \Bitrix\Main\Entity\AddResult
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function addQueue($id)
    {
        $rsS = static::getList(
            [
                "select" => [
                    "ID",
                    "START_PAGE_ID",
                    "START_FULL_URL",
                    "COOKIES",
                ],
                "filter" => [
                    "ID" => $id,
                    "ACTIVE" => "Y",
                ],
            ]
        );
        if (!$ar = $rsS->Fetch()) {
            throw new ArgumentException("Shedule not found", "$id", 1);
        }

        $rsQ = QueueTable::getList(
            [
                "select" => [
                    "ID",
                    "PAGE_ID",
                ],
                "filter" => [
                    "SHEDULE_ID" => $id,
                ],
            ]
        );
        if ($arQ = $rsQ->Fetch()) {
            throw new \Exception("Queue already exists", 1);
        }

        return QueueTable::addQueue($ar["ID"], $ar["START_PAGE_ID"], $ar["START_FULL_URL"], $ar["COOKIES"]);
    }

    /**
     * @param int      $id
     * @param null|int $maxExecTime
     *
     * @return \Bitrix\Main\Entity\AddResult
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function start($id, $maxExecTime = null)
    {
        $res = static::addQueue($id);
        if ($res->isSuccess()) {
            static::setStatus($id, static::STATUS_EXECUTE);
            if ($maxExecTime !== null) {
                static::execute($id, $maxExecTime);
            }
        }

        return $res;
    }

    /**
     * @param int $id
     *
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function stop($id)
    {
        return static::reset($id);
    }

    /**
     * Clear queue and set status to NEW
     *
     * @param integer $id shedule id
     *
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function reset($id)
    {
        $rs = QueueTable::getList(
            [
                "select" => ["ID"],
                "filter" => [
                    "SHEDULE_ID" => $id,
                ],
            ]
        );
        while ($ar = $rs->Fetch()) {
            QueueTable::delete($ar["ID"]);
        }

        static::setStatus($id, static::STATUS_NEW);

        return true;
    }

    public static function getRowParam($id, $name)
    {
        $rs = static::getList(
            [
                "select" => ["ID", $name],
                "filter" => ["ID" => $id],
            ]
        );
        if ($ar = $rs->Fetch()) {
            return $ar[$name];
        }

        return null;
    }

    /**
     * @param int  $id
     * @param bool $maxExecTime
     *
     * @return \Xdev\Cp\Internal\ListResult
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function execute($id, $maxExecTime = null)
    {
        $id = intval($id);

        static::setStatus($id, static::STATUS_EXECUTE);
        static::update(
            $id, [
                "LAST_EXEC" => new \Bitrix\Main\Type\DateTime(),
            ]
        );

        $result = new ListResult();
        try {
            $data = [
                "SUCCESS" => 0,
                "ERRORS" => 0,
            ];

            $time = time();
            while (true) {
                // execute new active queue
                $res = QueueTable::executeRow($id);
                if (is_object($res)) {
                    if ($res->isSuccess()) {
                        $data["SUCCESS"]++;
                    } else {
                        $data["ERRORS"]++;
                        foreach ($res->getErrors() as $error) {
                            $result->addError($error);
                        }
                    }
                } else {
                    break;
                }

                if ($maxExecTime && (time() - $time) >= $maxExecTime) {
                    break;
                }
            }

            $result->setData($data);
        } catch (\Exception $e) {
            $result->addError(new ErrorMessage($e->getMessage()));
        }

        if ($result->isSuccess(true)) {
            $rs = QueueTable::getList(
                [
                    "select" => ["ID"],
                    "filter" => [
                        "SHEDULE_ID" => $id,
                        "STATUS_ID" => [
                            QueueTable::STATUS_NEW,
                            QueueTable::STATUS_EXECUTE,
                        ],
                    ],
                ]
            );
            if (!$rs->Fetch()) {
                static::reindex($id);
            }
        } else {
            static::setStatus($id, static::STATUS_FAIL);
        }

        return $result;
    }

    public static function executeAgent()
    {
        $rs = static::getList(
            [
                "select" => [
                    "ID",
                    "STATUS_ID",
                ],
                "filter" => [
                    "ACTIVE" => "Y",
                    "<=NEXT_EXEC" => new \Bitrix\Main\Type\DateTime(),
                    "STATUS_ID" => [
                        static::STATUS_NEW,
                        static::STATUS_EXECUTE,
                    ],
                ],
            ]
        );
        while ($ar = $rs->Fetch()) {
            if ($ar["STATUS_ID"] == static::STATUS_NEW) {
                static::start($ar["ID"]);
            } else {
                static::execute($ar["ID"]);
            }
        }

        return __CLASS__."::executeAgent();";
    }

    public static function linkResultNodes($arFilter = [])
    {
        $rs = static::getList(
            [
                "select" => [
                    "ID",
                    "PROFILE_ID",
                ],
                "filter" => $arFilter,
            ]
        );
        while ($ar = $rs->Fetch()) {
            $rsNode = \xdev\cp\profile\NodeTable::getList(
                [
                    "order" => [
                        "TO_PAGE_ID" => "ASC",
                    ],
                    "select" => [
                        "TO_PAGE_ID",
                        "PAGE_ID",
                    ],
                    "filter" => [
                        "ACTIVE" => "Y",
                        "PAGE.PROFILE_ID" => $ar["PROFILE_ID"],
                        "!TO_PAGE_ID" => false,
                    ],
                    "group" => ["TO_PAGE_ID"],
                ]
            );
            $arPages = [];
            while ($arNode = $rsNode->Fetch()) {
                // skip pagenav element
                if ($arNode["PAGE_ID"] <> $arNode["TO_PAGE_ID"]) {
                    $arPages[$arNode["PAGE_ID"]] = $arNode["PAGE_ID"];
                }
            }
            foreach ($arPages as $pageId) {
                \Xdev\Cp\Spider\ResultNodeTable::linkNodes($ar["ID"], $pageId);
            }
        }
    }

    public static function reindex($id)
    {
        static::setStatus($id, static::STATUS_REINDEX);
        ResultNodeTable::resort($id);
        static::setStatus($id, static::STATUS_SUCCESS);
    }

    public static function checkIndex($id)
    {
        $parameters = [
            "select" => ["CNT"],
            'runtime' => [
                'CNT' => [
                    'data_type' => 'integer',
                    'expression' => [
                        'COUNT(1)',
                    ],
                ],
            ],
        ];
        $parameters["filter"] = ["SHEDULE_ID" => $id];
        $rs = \Xdev\Cp\Spider\ResultNodeTable::getList($parameters);
        if ($ar = $rs->Fetch()) {
            $res["TOTAL"] = $ar["CNT"];
        }

        $parameters["filter"] = ["SHEDULE_ID" => $id, "INDEXED" => "Y"];
        $rs = \Xdev\Cp\Spider\ResultNodeTable::getList($parameters);
        if ($ar = $rs->Fetch()) {
            $res["SUCCESS"] = $ar["CNT"];
        }

        if ($res["TOTAL"] == $res["SUCCESS"]) {
            static::setStatus($id, static::STATUS_SUCCESS);
        }

        return $res;
    }

    public static function onDelete(Event $event)
    {
        static::reset($event->getParameter("id"));
    }

    public static function getDropDownListEx($ID, $strProfileName, $strSheduleName, $arFilter = [], $maxLength = 50)
    {
        $arrItems = [];
        $rs = ProfileTable::getList(
            [
                "select" => [
                    "ID",
                    "NAME",
                ],
                "order" => ["NAME" => "ASC"],
            ]
        );
        while ($ar = $rs->Fetch()) {
            $arr = [
                "NAME" => $ar["NAME"]." [".$ar["ID"]."]",
                "ITEMS" => [],
            ];
            $rsIBlocks = static::getList(
                [
                    "select" => [
                        "ID",
                        "DESCRIPTION",
                        "PROFILE_ID",
                        "START_URL",
                    ],
                    "filter" => array_merge(
                        $arFilter, [
                            "PROFILE_ID" => $ar["ID"],
                        ]
                    ),
                    "order" => ["DESCRIPTION" => "ASC"],
                ]
            );
            while ($arIBlock = $rsIBlocks->Fetch()) {
                $name = (!empty($arIBlock["DESCRIPTION"]) ? $arIBlock["DESCRIPTION"] : urldecode($arIBlock["START_URL"]));
                if (strlen($name) > $maxLength) {
                    $val = substr($name, 0, $maxLength - 8);
                    $name = $val."..".substr($name, -5);
                }

                $arr["ITEMS"][$arIBlock["ID"]] = $name." [".$arIBlock["ID"]."]";
            }
            if (!empty($arr["ITEMS"])) {
                $arrItems[$ar["ID"]] = $arr;
            }
        }

        return tools::getDropDownList(
            $arrItems, $ID, $strProfileName, $strSheduleName, GetMessage("XDEV_CP_SHEDULE_CHOOSE_PAGE_TYPE"),
            GetMessage("XDEV_CP_SHEDULE_CHOOSE_PAGE")
        );
    }

    /**
     * @param int $id
     *
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function canExport($id)
    {
        return static::getStatus($id) === static::STATUS_SUCCESS;
    }
}
