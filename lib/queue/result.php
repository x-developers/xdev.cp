<?
namespace xdev\cp\queue;

class Result extends \Bitrix\Main\Entity\Result
{
	private $resultItems = array();

	public function addResultItem($item)
	{
		$this->resultItems[] = $item;

		return $this;
	}

	public function getResultTree()
	{
		return $this->resultItems;
	}
}
?>