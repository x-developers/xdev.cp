<?
namespace xdev\cp\queue;

use \Bitrix\Main\ArgumentException;
use \Bitrix\Main\Entity\Event;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Type\DateTime;

use \Xdev\Cp\Internal\DataManager;
use \Xdev\Cp\Internal\ListResult;

use \Xdev\Cp\Spider\ResultNodeTable;
use \Xdev\Cp\Spider\Manager as SM;

Loc::loadMessages(__FILE__);

class QueueTable extends DataManager
{
    const STATUS_NEW = "N";
    const STATUS_EXECUTE = "P";
    const STATUS_SUCCESS = "Y";
    const STATUS_FAIL = "F";

    public static function getFilePath()
    {
        return __FILE__;
    }

    public static function getTableName()
    {
        return "xdev_cp_queue_page";
    }

    public static function getMap()
    {
        return [
            "ID" => [
                "data_type" => "integer",
                "primary" => true,
                "autocomplete" => true,
                "title" => Loc::getMessage("XDEV_CP_ID_FIELD"),
            ],
            "SHEDULE_ID" => [
                "data_type" => "integer",
                "title" => Loc::getMessage("XDEV_CP_QUEUE_SHEDULE_ID_FIELD"),
            ],
            "SHEDULE" => [
                "data_type" => "\\xdev\\cp\\Queue\\Shedule",
                "reference" => ["=this.SHEDULE_ID" => "ref.ID"],
            ],
            "CACHE_TYPE" => [
                "data_type" => "string",
                "title" => Loc::getMessage("XDEV_CP_QUEUE_SHEDULE_CACHE_TYPE"),
                "expression" => [
                    '%s',
                    'SHEDULE.CACHE_TYPE',
                ],
            ],
            "PAGE_ID" => [
                "data_type" => "integer",
                "title" => Loc::getMessage("XDEV_CP_QUEUE_PAGE_ID_FIELD"),
            ],
            "CREATED_NODE_ID" => [
                "data_type" => "string",
            ],
            "PAGE" => [
                "data_type" => "\\xdev\\cp\\Profile\\Page",
                "reference" => ["=this.PAGE_ID" => "ref.ID"],
            ],
            "PAGE_NAME" => [
                "data_type" => "string",
                "title" => Loc::getMessage("XDEV_CP_QUEUE_PAGE_NAME_FIELD"),
                "expression" => [
                    '%s',
                    'PAGE.NAME',
                ],
            ],
            "URL" => [
                "data_type" => "string",
                "title" => Loc::getMessage("XDEV_CP_QUEUE_URL_FIELD"),
            ],
            "DATE_CREATE" => [
                "data_type" => "datetime",
                "title" => Loc::getMessage("XDEV_CP_DATE_CREATE_FIELD"),
            ],
            "TIMESTAMP_X" => [
                "data_type" => "datetime",
                "title" => Loc::getMessage("XDEV_CP_TIMESTAMP_X_FIELD"),
            ],
            "STATUS_ID" => [
                "data_type" => "boolean",
                "values" => ["N", "Y", "P", "F"],
                "title" => Loc::getMessage("XDEV_CP_QUEUE_STATUS_ID_FIELD"),
            ],
            "STATUS_DATE" => [
                "data_type" => "datetime",
                "title" => Loc::getMessage("XDEV_CP_QUEUE_STATUS_DATE_FIELD"),
            ],
            "QUEUE_CNT" => [
                "data_type" => "string",
                "title" => Loc::getMessage("XDEV_CP_QUEUE_NODE_CNT_FIELD"),
                "expression" => [
                    "(SELECT COUNT('x') from `xdev_cp_queue_result` WHERE (`xdev_cp_queue_result`.QUEUE_ID = %s))",
                    "ID",
                ],
            ],
        ];
    }

    public static function getStatusArray()
    {
        $res = [];
        foreach (
            [
                static::STATUS_NEW,
                static::STATUS_EXECUTE,
                static::STATUS_FAIL,
                static::STATUS_SUCCESS,
            ] as $val
        ) {
            $res[$val] = Loc::getMessage(sprintf("XDEV_CP_QUEUE_STATUS_%s_FIELD", $val));
        }

        return $res;
    }

    public static function setStatus($id, $status)
    {
        $ar = static::getStatusArray();
        if (!is_set($ar, $status)) {
            throw new ArgumentException("Queue status #%s not found", $status);
        }

        return static::update($id, ["STATUS_ID" => $status, "STATUS_DATE" => new DateTime()]);
    }

    public static function getLampStatus($status)
    {
        $ar = [
            static::STATUS_NEW => "grey",
            static::STATUS_EXECUTE => "yellow",
            static::STATUS_FAIL => "red",
            static::STATUS_SUCCESS => "green-s",
        ];

        return isset($ar[$status]) ? $ar[$status] : "grey";
    }

    public static function getStatus($id)
    {
        $ar = static::getRow(
            [
                "select" => ["STATUS_ID"],
                "filter" => ["ID" => intval($id)],
            ]
        );

        return $ar && $ar["STATUS_ID"] ? $ar["STATUS_ID"] : false;
    }

    /**
     * Adds page to queue
     *
     * @param integer $sheduleId shedule id
     * @param integer $pageId    page id
     * @param string  $url       absolute url
     *
     * @return \Bitrix\Main\Entity\AddResult Contains ID of inserted row
     *
     * @throws \Exception
     */
    public static function addQueue($sheduleId, $pageId, $url, $fromResultnodeId = null)
    {
        return static::add(
            [
                "SHEDULE_ID" => $sheduleId,
                "PAGE_ID" => $pageId,
                "URL" => $url,
                "STATUS_ID" => static::STATUS_NEW,
                "CREATED_NODE_ID" => $fromResultnodeId,
            ]
        );
    }

    public static function execute($id)
    {
        $arQueue = static::getList(
            [
                'filter' => [
                    'ID' => $id,
                ],
                'select' => [
                    'ID',
                    'URL',
                    'PAGE_ID',
                    'SHEDULE_ID',
                    'CREATED_NODE_ID',
                    'CACHE_TYPE',
                ],
            ]
        )->fetch();

        $result = new ListResult();
        try {
            $arNodes = (new SM($arQueue))->parse();
            // save nodes and add new pages to queue
            foreach ($arNodes as $node) {
                $arCurrentParams = [];
                if (!empty($arQueue["CREATED_NODE_ID"])) {
                    $arCurrentParams["PARENT_ID"] = $arQueue["CREATED_NODE_ID"];
                }

                $res = $node->save(["QUEUE_ID" => $id], $arCurrentParams);

                if ($res->isSuccess()) {
                    foreach ($node->getTree() as $childNode) {
                        if (
                        ($childNode instanceof \Xdev\Cp\Spider\AncorNode)
                        ) {
                            if (!$childNode->isValid()) {
                                throw new \Exception(sprintf("Error add page to queue. Invalid url \"%s\"", $childNode->payload()), 1);
                            }

                            static::addQueue(
                                $arQueue["SHEDULE_ID"],
                                $childNode->getProfileNode()->getToPageId(),
                                $childNode->payload(),
                                $childNode->getId()
                            );
                        }
                    }
                }
            }

            static::setStatus($arQueue["ID"], static::STATUS_SUCCESS);
        } catch (\Exception $e) {
            $result->addError(new \Bitrix\Main\Entity\EntityError($e->getMessage()));

            static::setStatus($arQueue["ID"], static::STATUS_FAIL);
        }

        return $result;
    }

    /**
     * Executes queue with lock row
     *
     * @param integer $shedule_id shedule id
     *
     * @return \Xdev\Cp\Internal\ListResult
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\Db\SqlQueryException
     * @throws \Bitrix\Main\SystemException
     * @throws \Exception
     */
    public static function executeRow($shedule_id)
    {
        $connection = static::getEntity()->getConnection();
        $connection->query(sprintf("LOCK TABLES `%s` WRITE;", static::getTableName()));

        try {
            $rs = $connection->query(
                sprintf(
                    "SELECT ID FROM `%s` WHERE SHEDULE_ID = %s AND STATUS_ID = '%s' LIMIT 1;", static::getTableName(), intval($shedule_id),
                    static::STATUS_NEW
                )
            );
            if ($ar = $rs->Fetch()) {
                $connection->query(
                    sprintf("UPDATE `%s` SET STATUS_ID = '%s' WHERE ID = %s", static::getTableName(), static::STATUS_EXECUTE, $ar["ID"])
                );
                $connection->query("UNLOCK TABLES;");

                return static::execute($ar["ID"]);
            } else {
                $connection->query("UNLOCK TABLES;");
            }
        } catch (\Exception $e) {
            $connection->query("UNLOCK TABLES;");

            throw $e;
        }
    }

    public static function onDelete(Event $event)
    {
        $primary = $event->getParameter("id");
        $rs = ResultNodeTable::getList(
            [
                "select" => ["ID"],
                "filter" => [
                    "QUEUE_ID" => $primary["ID"],
                ],
            ]
        );
        while ($ar = $rs->Fetch()) {
            ResultNodeTable::delete($ar["ID"]);
        }
    }

    /**
     * @param int $id
     *
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function clear($id)
    {
        $row = static::getRowById($id);
        if (!is_array($row)) {
            throw new \Bitrix\Main\ArgumentException("Queue not found", 1);
        }

        $rs = ResultNodeTable::getList(
            [
                "select" => ["ID"],
                "filter" => [
                    "QUEUE_ID" => $row["ID"],
                ],
            ]
        );
        while ($ar = $rs->Fetch()) {
            ResultNodeTable::delete($ar["ID"]);
        }

        static::setStatus($row["ID"], static::STATUS_NEW);

        SheduleTable::setStatus($row["SHEDULE_ID"], SheduleTable::STATUS_EXECUTE);
    }
}
