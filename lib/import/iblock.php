<?
namespace Xdev\Cp\Import;

class Iblock extends Base
{
	protected $moduleId = "iblock";

	public function import()
	{
		$result = parent::import(array("IBLOCK_ID" => $this->getProfile("IBLOCK_ID")));

		if($this->getTypeId() == Iblock::SECTION_TYPE && empty($result["DATA"]) && \CModule::IncludeModule($this->moduleId))
			\CIBlockSection::resort($this->getProfile("IBLOCK_ID"));

		return $result;
	}
}
?>