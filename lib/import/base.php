<?
namespace Xdev\Cp\Import;

use \Bitrix\Main\Type\Collection;

use Xdev\Cp\Export;

abstract class base
{
    const SECTION_TYPE = "S";
    const PROPERTY_TYPE = "P";
    const ELEMENT_TYPE = "E";
    protected $ns;
    protected $profile = null;
    protected $moduleId = null;
    protected $typeId;
    protected $exportId;
    protected $sum = [
        "TOTAL" => 0,
        "SUCCESS" => 0,
        "ERRORS" => 0,
    ];

    public function __construct(&$NS)
    {
        $this->ns = &$NS;
    }

    /**
     * Clear temporary data
     *
     * @return void
     */
    public function reset()
    {
        $this->ns["ENTITY_INDEX"] = 0;

        foreach (
            [
                "SECTION_DATA",
                "PROPERTY_DATA",
                "ELEMENT_DATA",
                "ENTITY_DATA",
            ] as $val
        ) {
            $this->ns[$val] = false;
        }
        foreach (["ENTITY_MAP", "RESULT_MAP"] as $val) {
            $this->ns[$val] = [];
        }
    }

    /**
     * Collect entity to import
     *
     * @return $this
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function init()
    {
        $this->ns["ENTITY_INDEX"] = 0;

        $rs = Export\EntityTable::getList(
            [
                "filter" => [
                    "EXPORT_ID" => $this->getExportId(),
                ],
                "order" => [
                    "LEFT_MARGIN" => "ASC",
                ],
            ]
        );
        while ($ar = $rs->Fetch()) {
            $this->ns["ENTITY_MAP"][] = $ar;
        }

        return $this;
    }

    /**
     * Gets current entity
     *
     * @return array[string]|false
     */
    protected function getCurrent()
    {
        return isset($this->ns["ENTITY_MAP"][$this->ns["ENTITY_INDEX"]]) ? $this->ns["ENTITY_MAP"][$this->ns["ENTITY_INDEX"]] : false;
    }

    /**
     * Gets total records of current entity
     *
     * @return int
     * @throws \Exception
     */
    public function getTotal()
    {
        if ($arEntity = $this->getCurrent()) {
            $node = new Export\ExportEntity($this->getExportId(), $arEntity["ID"]);

            return $node->getTotal($this->getProfile("SHEDULE_ID"));
        } else {
            throw new \Exception("Error Processing Request", 1);
        }
    }

    /**
     * Gets total records of type
     *
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     * @throws \Exception
     */
    public function getTotalRecords()
    {
        $rs = Export\EntityTable::getList(
            [
                "filter" => [
                    "EXPORT_ID" => $this->getExportId(),
                    "TYPE_ID" => $this->getTypeId(),
                ],
            ]
        );

        $result = 0;
        while ($ar = $rs->Fetch()) {
            $node = new Export\ExportEntity($this->getExportId(), $ar["ID"]);

            $result += $node->getTotal($this->getProfile("SHEDULE_ID"));
        }

        return $result;
    }

    /**
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getExportId()
    {
        return $this->getProfile("ID");
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getModuleId()
    {
        if (null === $this->moduleId) {
            throw new \Exception("Module not set", 1);
        }

        return $this->moduleId;
    }

    public function getTypeId()
    {
        $arEntity = $this->getCurrent();

        return $arEntity["TYPE_ID"];
    }

    /**
     * @param null|string $param
     *
     * @return array|mixed|null
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    protected function getProfile($param = null)
    {
        if (!is_array($this->profile)) {
            $this->profile = Export\ProfileTable::getRowById($this->ns["ID"]);
        }

        if (!is_array($this->profile)) {
            throw new \Exception("Profile export not found", 1);
        }

        return $param !== null ? $this->profile[$param] : $this->profile;
    }

    protected function getListDataManager($arFilter = [])
    {
        return Entity\ManagerTable::getList(
            [
                "filter" => array_merge(
                    [
                        "MODULE_ID" => $this->getModuleId(),
                    ], $arFilter
                ),
            ]
        );
    }

    public function getDataManager()
    {
        $ar = $this->getListDataManager(
            [
                "TYPE_ID" => $this->getTypeId(),
            ]
        )->Fetch();

        return $ar["CLASS"];
    }

    protected function findParent(array $element)
    {
        $result = [];
        $arEntity = $this->getCurrent();
        if ($arEntity["PARENT_ID"] && is_array($this->ns["RESULT_MAP"][$arEntity["PARENT_ID"]])) {
            foreach ($this->ns["RESULT_MAP"][$arEntity["PARENT_ID"]] as $arSection) {
                if (
                    ($element["RIGHT_MARGIN"] <= $arSection["RIGHT_MARGIN"])
                    && ($element["LEFT_MARGIN"] >= $arSection["LEFT_MARGIN"])
                ) {
                    if (!isset($result[$arSection["ID"]])) {
                        $result[$arSection["ID"]] = $arSection;
                    }
                }
            }
            Collection::sortByColumn($result, ["DEPTH_LEVEL" => SORT_DESC]);
        }

        return !empty($result) ? $result[0] : false;
    }

    public function getNext()
    {
        $this->ns["LAST_ID"] = 0;

        if (isset($this->ns["ENTITY_MAP"][$this->ns["ENTITY_INDEX"] + 1])) {
            $this->ns["ENTITY_INDEX"]++;

            return true;
        }

        return false;
    }

    protected function setSum(array $data)
    {
        foreach ($this->sum as $key => $val) {
            if (!isset($data[$key])) {
                $data[$key] = $val;
            }
        }

        $this->ns["DATA"][$this->getTypeId()] = $data;

        return $this;
    }

    public function getSum()
    {
        if (!isset($this->ns["DATA"][$this->getTypeId()]) || empty($this->ns["DATA"][$this->getTypeId()])) {
            $this->ns["DATA"][$this->getTypeId()] = [
                "TOTAL" => $this->getTotalRecords(),
                "SUCCESS" => 0,
                "ERRORS" => 0,
            ];
        }

        return $this->ns["DATA"][$this->getTypeId()];
    }

    public function import($arFields = [])
    {
        $sum = $this->getSum();
        if ($this->getTotal() > 0) {
            $arEntity = $this->getCurrent();

            if (!isset($this->ns["RESULT_MAP"][$arEntity["ID"]])) {
                $this->ns["RESULT_MAP"][$arEntity["ID"]] = [];
            }

            $node = new Export\ExportEntity($this->getProfile("ID"), $arEntity["ID"]);
            $res = $node->exportTree(
                $this->getProfile("SHEDULE_ID"), $this->getDataManager(), $arFields, $this->ns["LAST_ID"], $this->ns["INTERVAL"]
            );
            $this->ns["LAST_ID"] = $node->getLastId();

            foreach ($res as $element) {
                if ($parent = $this->findParent($element)) {
                    $className = $this->getDataManager();
                    $className::setParent($element, $parent);
                }
                $this->ns["RESULT_MAP"][$arEntity["ID"]][$element["ID"]] = $element;
            }

            $result = ["DATA" => $res, "ERRORS" => $node->getErrors()];

            $sum["SUCCESS"] += count($result["DATA"]);
            $sum["ERRORS"] += count($result["ERRORS"]);
        }

        $this->setSum($sum);

        return $result;
    }
}
