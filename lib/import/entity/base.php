<?
namespace Xdev\Cp\Import\Entity;

use \Xdev\Cp\Internal\DataManager;

class Base extends DataManager
{
	protected static $typeId;

	protected static $xmlIdFieldname = "XML_ID";

	protected static $fileFields = array();

	protected static $defaults = array(
		"ACTIVE" => "Y",
		"SORT" => 500
	);

	public static function getByXML_ID($xmlId, $arFilter = array())
	{
		$res = static::getList(array(
			"select" => array("ID"),
			"filter" => array_merge(
					array(static::$xmlIdFieldname => $xmlId),
					$arFilter
				)
			)
		);

		return $res ? $res["ID"] : false;
	}

	protected static function isAssoc(array $data)
	{
		foreach($data as $key => $val)
		{
			if(!is_numeric($key))
				return true;
		}

		return false;
	}
	protected static function modifyBeforeSave(&$data)
	{
		if(!isset($data["MULTIPLE"]) || $data["MULTIPLE"] == "N")
		{
			foreach($data as $key => $val)
			{
				if(is_array($val) && !static::isAssoc($val))
				{
					if(count($val) > 1)
						throw new \Exception("Multiple value set on non muliple entity", 1);

					$data[$key] = !empty($val) ? $val[0] : "";
				}
			}
		}

		$arMap = static::getMap();
		foreach($data as $key => $val)
			if(!isset($arMap[$key]))
				unset($data[$key]);

		foreach(static::$defaults as $key => $val)
			if(!isset($data[$key]))
				$data[$key] = static::$defaults[$key];

		foreach(static::$fileFields as $key)
		{
			if(isset($data[$key]) && !is_array($data[$key]))
			{
				$data[$key] = \CFile::MakeFileArray($data[$key]);
			}
		}
	}

	public static function save(array $data, $parentId = null)
	{
		static::modifyBeforeSave($data);

		$id = 0; $matchFieldName = "";
		if(isset($data["ID"]))
		{
			$id = intval($data["ID"]);
		}
		else
		{
			$matchFields = array(static::$xmlIdFieldname, "TMP_ID");
			foreach($matchFields as $val)
			{
				if(isset($data[$val]))
				{
					$matchFieldName = $val;
					break;
				}
			}
		}

		if($id <= 0 && !empty($matchFieldName))
		{
			$rs = static::getList(array(
				"select" => array("ID"),
				"filter" => array(
					"IBLOCK_ID" => $data["IBLOCK_ID"],
					$matchFieldName => $data[$matchFieldName]
				)
			));
			if($ar = $rs->Fetch())
				$id = $ar["ID"];
		}

		if($id > 0)
		{
			$result = static::update($id, $data);
		}
		else
		{
			$result = static::add($data);
		}

		return $result;
	}
}
?>