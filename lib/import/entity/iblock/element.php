<?
namespace Xdev\Cp\Import\Entity\Iblock;

use \Bitrix\Main\Entity as BitrixEntity;
use \Bitrix\Main\Localization\Loc;

use Xdev\Cp\Import\Entity;

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/iblock/lib/element.php");
Loc::loadMessages(__FILE__);

class ElementTable extends Entity\Base
{
	protected static $fileFields = array("PREVIEW_PICTURE", "DETAIL_PICTURE");

	protected static $defaults = array(
		"ACTIVE" => "Y",
		"SORT" => 500,
		"IN_SECTIONS" => "N"
	);

	const TYPE_TEXT = "text";
	const TYPE_HTML = "html";

	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return "b_iblock_element";
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			"ID" => new BitrixEntity\IntegerField("ID", array(
				"primary" => true,
				"autocomplete" => true,
				"title" => Loc::getMessage("ELEMENT_ENTITY_ID_FIELD"),
			)),
			"TIMESTAMP_X" => new BitrixEntity\DatetimeField("TIMESTAMP_X", array(
				"default_value" => new \Bitrix\Main\Type\DateTime(),
				"title" => Loc::getMessage("ELEMENT_ENTITY_TIMESTAMP_X_FIELD"),
			)),
			"MODIFIED_BY" => new BitrixEntity\IntegerField("MODIFIED_BY", array(
				"title" => Loc::getMessage("ELEMENT_ENTITY_MODIFIED_BY_FIELD"),
			)),
			"DATE_CREATE" => new BitrixEntity\DatetimeField("DATE_CREATE", array(
				"default_value" => new \Bitrix\Main\Type\DateTime(),
				"title" => Loc::getMessage("ELEMENT_ENTITY_DATE_CREATE_FIELD"),
			)),
			"CREATED_BY" => new BitrixEntity\IntegerField("CREATED_BY", array(
				"title" => Loc::getMessage("ELEMENT_ENTITY_CREATED_BY_FIELD"),
			)),
			"IBLOCK_ID" => new BitrixEntity\IntegerField("IBLOCK_ID", array(
				"required" => true,
				"title" => Loc::getMessage("ELEMENT_ENTITY_IBLOCK_ID_FIELD"),
			)),
			"IBLOCK_SECTION_ID" => new BitrixEntity\IntegerField("IBLOCK_SECTION_ID", array(
				"title" => Loc::getMessage("ELEMENT_ENTITY_IBLOCK_SECTION_ID_FIELD"),
			)),
			"ACTIVE" => new BitrixEntity\BooleanField("ACTIVE", array(
				"values" => array("N", "Y"),
				"default_value" => "Y",
				"title" => Loc::getMessage("ELEMENT_ENTITY_ACTIVE_FIELD"),
			)),
			"ACTIVE_FROM" => new BitrixEntity\DatetimeField("ACTIVE_FROM", array(
				"title" => Loc::getMessage("ELEMENT_ENTITY_ACTIVE_FROM_FIELD"),
			)),
			"ACTIVE_TO" => new BitrixEntity\DatetimeField("ACTIVE_TO", array(
				"title" => Loc::getMessage("ELEMENT_ENTITY_ACTIVE_TO_FIELD"),
			)),
			"SORT" => new BitrixEntity\IntegerField("SORT", array(
				"default_value" => 500,
				"title" => Loc::getMessage("ELEMENT_ENTITY_SORT_FIELD"),
			)),
			"NAME" => new BitrixEntity\StringField("NAME", array(
				"required" => true,
				"validation" => array(__CLASS__, "validateName"),
				"title" => Loc::getMessage("ELEMENT_ENTITY_NAME_FIELD"),
			)),
			"PREVIEW_PICTURE" => new BitrixEntity\IntegerField("PREVIEW_PICTURE", array(
				"title" => Loc::getMessage("ELEMENT_ENTITY_PREVIEW_PICTURE_FIELD"),
			)),
			"PREVIEW_TEXT" => new BitrixEntity\TextField("PREVIEW_TEXT", array(
				"title" => Loc::getMessage("ELEMENT_ENTITY_PREVIEW_TEXT_FIELD"),
			)),
			"PREVIEW_TEXT_TYPE" => new BitrixEntity\EnumField("PREVIEW_TEXT_TYPE", array(
				"values" => array(self::TYPE_TEXT, self::TYPE_HTML),
				"default_value" => self::TYPE_TEXT,
				"title" => Loc::getMessage("ELEMENT_ENTITY_PREVIEW_TEXT_TYPE_FIELD"),
			)),
			"DETAIL_PICTURE" => new BitrixEntity\IntegerField("DETAIL_PICTURE", array(
				"title" => Loc::getMessage("ELEMENT_ENTITY_DETAIL_PICTURE_FIELD"),
			)),
			"DETAIL_TEXT" => new BitrixEntity\TextField("DETAIL_TEXT", array(
				"title" => Loc::getMessage("ELEMENT_ENTITY_DETAIL_TEXT_FIELD"),
			)),
			"DETAIL_TEXT_TYPE" => new BitrixEntity\EnumField("DETAIL_TEXT_TYPE", array(
				"values" => array(self::TYPE_TEXT, self::TYPE_HTML),
				"default_value" => self::TYPE_TEXT,
				"title" => Loc::getMessage("ELEMENT_ENTITY_DETAIL_TEXT_TYPE_FIELD"),
			)),
			"SEARCHABLE_CONTENT" => new BitrixEntity\TextField("SEARCHABLE_CONTENT", array(
				"title" => Loc::getMessage("ELEMENT_ENTITY_SEARCHABLE_CONTENT_FIELD"),
			)),
			"WF_STATUS_ID" => new BitrixEntity\IntegerField("WF_STATUS_ID", array(
				"title" => Loc::getMessage("ELEMENT_ENTITY_WF_STATUS_ID_FIELD"),
			)),
			"WF_PARENT_ELEMENT_ID" => new BitrixEntity\IntegerField("WF_PARENT_ELEMENT_ID", array(
				"title" => Loc::getMessage("ELEMENT_ENTITY_WF_PARENT_ELEMENT_ID_FIELD"),
			)),
			"WF_NEW" => new BitrixEntity\EnumField("WF_NEW", array(
				"values" => array("N", "Y"),
				"title" => Loc::getMessage("ELEMENT_ENTITY_WF_NEW_FIELD"),
			)),
			"WF_LOCKED_BY" => new BitrixEntity\IntegerField("WF_LOCKED_BY", array(
				"title" => Loc::getMessage("ELEMENT_ENTITY_WF_LOCKED_BY_FIELD"),
			)),
			"WF_DATE_LOCK" => new BitrixEntity\DatetimeField("WF_DATE_LOCK", array(
				"title" => Loc::getMessage("ELEMENT_ENTITY_WF_DATE_LOCK_FIELD"),
			)),
			"WF_COMMENTS" => new BitrixEntity\TextField("WF_COMMENTS", array(
				"title" => Loc::getMessage("ELEMENT_ENTITY_WF_COMMENTS_FIELD"),
			)),
			"IN_SECTIONS" => new BitrixEntity\BooleanField("IN_SECTIONS", array(
				"values" => array("N", "Y"),
				"title" => Loc::getMessage("ELEMENT_ENTITY_IN_SECTIONS_FIELD"),
			)),
			"XML_ID" => new BitrixEntity\StringField("XML_ID", array(
				"validation" => array(__CLASS__, "validateXmlId"),
				"title" => Loc::getMessage("ELEMENT_ENTITY_XML_ID_FIELD"),
			)),
			"CODE" => new BitrixEntity\StringField("CODE", array(
				"validation" => array(__CLASS__, "validateCode"),
				"title" => Loc::getMessage("ELEMENT_ENTITY_CODE_FIELD"),
			)),
			"TAGS" => new BitrixEntity\StringField("TAGS", array(
				"validation" => array(__CLASS__, "validateTags"),
				"title" => Loc::getMessage("ELEMENT_ENTITY_TAGS_FIELD"),
			)),
			"TMP_ID" => new BitrixEntity\StringField("TMP_ID", array(
				"validation" => array(__CLASS__, "validateTmpId"),
				"title" => Loc::getMessage("ELEMENT_ENTITY_TMP_ID_FIELD"),
			)),
			"SHOW_COUNTER" => new BitrixEntity\IntegerField("SHOW_COUNTER", array(
				"default_value" => 0,
				"title" => Loc::getMessage("ELEMENT_ENTITY_SHOW_COUNTER_FIELD"),
			)),
			"SHOW_COUNTER_START" => new BitrixEntity\DatetimeField("SHOW_COUNTER_START", array(
				"title" => Loc::getMessage("ELEMENT_ENTITY_SHOW_COUNTER_START_FIELD"),
			)),
			"PREVIEW_PICTURE_FILE" => new BitrixEntity\ReferenceField(
				"PREVIEW_PICTURE_FILE",
				"Bitrix\File\File",
				array("=this.PREVIEW_PICTURE" => "ref.ID"),
				array("join_type" => "LEFT")
			),
			"DETAIL_PICTURE_FILE" => new BitrixEntity\ReferenceField(
				"DETAIL_PICTURE_FILE",
				"Bitrix\File\File",
				array("=this.DETAIL_PICTURE" => "ref.ID"),
				array("join_type" => "LEFT")
			),
			"IBLOCK" => new BitrixEntity\ReferenceField(
				"IBLOCK",
				"Bitrix\Iblock\Iblock",
				array("=this.IBLOCK_ID" => "ref.ID"),
				array("join_type" => "LEFT")
			),
			"WF_PARENT_ELEMENT" => new BitrixEntity\ReferenceField(
				"WF_PARENT_ELEMENT",
				"Bitrix\Iblock\Element",
				array("=this.WF_PARENT_ELEMENT_ID" => "ref.ID"),
				array("join_type" => "LEFT")
			),
			"IBLOCK_SECTION" => new BitrixEntity\ReferenceField(
				"IBLOCK_SECTION",
				"Bitrix\Iblock\Section",
				array("=this.IBLOCK_SECTION_ID" => "ref.ID"),
				array("join_type" => "LEFT")
			),
			"MODIFIED_BY_USER" => new BitrixEntity\ReferenceField(
				"MODIFIED_BY_USER",
				"Bitrix\User\User",
				array("=this.MODIFIED_BY" => "ref.ID"),
				array("join_type" => "LEFT")
			),
			"CREATED_BY_USER" => new BitrixEntity\ReferenceField(
				"CREATED_BY_USER",
				"Bitrix\User\User",
				array("=this.CREATED_BY" => "ref.ID"),
				array("join_type" => "LEFT")
			),
			"WF_LOCKED_BY_USER" => new BitrixEntity\ReferenceField(
				"WF_LOCKED_BY_USER",
				"Bitrix\User\User",
				array("=this.WF_LOCKED_BY" => "ref.ID"),
				array("join_type" => "LEFT")
			),
		);
	}
	/**
	 * Returns validators for NAME field.
	 *
	 * @return array
	 */
	public static function validateName()
	{
		return array(
			new BitrixEntity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for XML_ID field.
	 *
	 * @return array
	 */
	public static function validateXmlId()
	{
		return array(
			new BitrixEntity\Validator\Length(null, 255),
		);
	}
	/**
	 * Returns validators for CODE field.
	 *
	 * @return array
	 */
	public static function validateCode()
	{
		return array(
			new BitrixEntity\Validator\Length(null, 255),
		);
	}
	/**
	 * Returns validators for TAGS field.
	 *
	 * @return array
	 */
	public static function validateTags()
	{
		return array(
			new BitrixEntity\Validator\Length(null, 255),
		);
	}
	/**
	 * Returns validators for TMP_ID field.
	 *
	 * @return array
	 */
	public static function validateTmpId()
	{
		return array(
			new BitrixEntity\Validator\Length(null, 40),
		);
	}

	public static function add(array $data)
	{
		\Bitrix\Main\Loader::includeModule("iblock");

		$result = new BitrixEntity\AddResult();

		$ob = new \CIBlockElement();
		$res = $ob->add($data);
		if($res)
		{
			$data["ID"] = $result->getId();
			$result->setId($res);
		}
		else
		{
			$result->addError(new BitrixEntity\EntityError($ob->LAST_ERROR));
		}
		$result->setData($data);

		return $result;
	}

	public static function update($primary, array $data)
	{
		\Bitrix\Main\Loader::includeModule("iblock");

		$result = new BitrixEntity\UpdateResult();
		$result->setPrimary($primary);

		$ob = new \CIBlockElement();
		$res = $ob->update($primary, $data);
		if($res)
		{
			$data["ID"] = $result->getPrimary();
		}
		else
		{
			$result->addError(new BitrixEntity\EntityError($ob->LAST_ERROR));
		}

		$result->setData($data);

		return $result;
	}

	public static function save(array $data, $parentId = null)
	{
		static::modifyBeforeSave($data);

		if(null !== $parentId)
			$data["IBLOCK_SECTION_ID"] = $parentId;

		if(!isset($data["XML_ID"]) && isset($data["NAME"]))
			$data["XML_ID"] = $data["IBLOCK_ID"]."|".md5($data["NAME"]);

		return parent::save($data, $parentId);
	}

	public static function setParent(array $data, array $parent)
	{
		return static::update($data["ID"], array("IBLOCK_SECTION_ID" => $parent["ID"]));
	}
}
?>