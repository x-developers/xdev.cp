<?
namespace Xdev\Cp\Import\Entity\Iblock;

use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc;

use Xdev\Cp\Import\Entity;

class ElementPropertyTable extends Entity\Base
{
	protected static $typeId = "V";

	protected static $xmlIdFieldname = "";

	protected static $defaults = array();

	/**
	 * Returns path to the file which contains definition of the class.
	 *
	 * @return string
	 */
	public static function getFilePath()
	{
		return __FILE__;
	}

	/**
	 * Returns DB table name for entity
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return "b_iblock_element_property";
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			"IBLOCK_PROPERTY_ID" => array(
				"data_type" => "integer",
				"primary" => true,
			),
			"IBLOCK_ELEMENT_ID" => array(
				"data_type" => "integer",
				"primary" => true,
			),
			"VALUE" => array(
				"data_type" => "string",
				"requried" => true,
				"validation" => array(__CLASS__, "validateValue"),
			),
		);
	}

	/**
	 * Returns validators for VALUE field.
	 *
	 * @return array
	 */
	public static function validateValue()
	{
		return array(
			new Main\Entity\Validator\Length(null, 2048),
		);
	}
}
?>