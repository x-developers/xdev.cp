<?
namespace Xdev\Cp\Import\Entity\Iblock;

use \Bitrix\Main\Entity as BitrixEntity;
use \Bitrix\Main\Localization\Loc;

use Xdev\Cp\Import\Entity;

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/iblock/lib/section.php");
Loc::loadMessages(__FILE__);

class SectionTable extends Entity\Base
{
	/**
	 * Returns path to the file which contains definition of the class.
	 *
	 * @return string
	 */
	public static function getFilePath()
	{
		return __FILE__;
	}

	/**
	 * Returns DB table name for entity
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return "b_iblock_section";
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			"ID" => array(
				"data_type" => "integer",
				"primary" => true,
				"autocomplete" => true,
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_ID_FIELD"),
			),
			"TIMESTAMP_X" => array(
				"data_type" => "datetime",
				"required" => true,
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_TIMESTAMP_X_FIELD"),
			),
			"MODIFIED_BY" => array(
				"data_type" => "integer",
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_MODIFIED_BY_FIELD"),
			),
			"DATE_CREATE" => array(
				"data_type" => "datetime",
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_DATE_CREATE_FIELD"),
			),
			"CREATED_BY" => array(
				"data_type" => "integer",
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_CREATED_BY_FIELD"),
			),
			"IBLOCK_ID" => array(
				"data_type" => "integer",
				"required" => true,
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_IBLOCK_ID_FIELD"),
			),
			"IBLOCK_SECTION_ID" => array(
				"data_type" => "integer",
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_IBLOCK_SECTION_ID_FIELD"),
			),
			"ACTIVE" => array(
				"data_type" => "boolean",
				"values" => array("N", "Y"),
				"default_value" => "Y",
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_ACTIVE_FIELD"),
			),
			"GLOBAL_ACTIVE" => array(
				"data_type" => "boolean",
				"values" => array("N", "Y"),
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_GLOBAL_ACTIVE_FIELD"),
			),
			"SORT" => array(
				"data_type" => "integer",
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_SORT_FIELD"),
			),
			"NAME" => array(
				"data_type" => "string",
				"required" => true,
				"validation" => array(__CLASS__, "validateName"),
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_NAME_FIELD"),
			),
			"PICTURE" => array(
				"data_type" => "integer",
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_PICTURE_FIELD"),
			),
			"LEFT_MARGIN" => array(
				"data_type" => "integer",
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_LEFT_MARGIN_FIELD"),
			),
			"RIGHT_MARGIN" => array(
				"data_type" => "integer",
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_RIGHT_MARGIN_FIELD"),
			),
			"DEPTH_LEVEL" => array(
				"data_type" => "integer",
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_DEPTH_LEVEL_FIELD"),
			),
			"DESCRIPTION" => array(
				"data_type" => "text",
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_DESCRIPTION_FIELD"),
			),
			"DESCRIPTION_TYPE" => array(
				"data_type" => "enum",
				"required" => true,
				"values" => array("text", "html"),
				"default_value" => "text",
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_DESCRIPTION_TYPE_FIELD"),
			),
			"SEARCHABLE_CONTENT" => array(
				"data_type" => "text",
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_SEARCHABLE_CONTENT_FIELD"),
			),
			"CODE" => array(
				"data_type" => "string",
				"validation" => array(__CLASS__, "validateCode"),
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_CODE_FIELD"),
			),
			"XML_ID" => array(
				"data_type" => "string",
				"validation" => array(__CLASS__, "validateXmlId"),
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_XML_ID_FIELD"),
			),
			"TMP_ID" => array(
				"data_type" => "string",
				"validation" => array(__CLASS__, "validateTmpId"),
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_TMP_ID_FIELD"),
			),
			"DETAIL_PICTURE" => array(
				"data_type" => "integer",
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_DETAIL_PICTURE_FIELD"),
			),
			"SOCNET_GROUP_ID" => array(
				"data_type" => "integer",
				"title" => Loc::getMessage("IBLOCK_SECTION_ENTITY_SOCNET_GROUP_ID_FIELD"),
			),
			"IBLOCK" => array(
				"data_type" => "\Bitrix\Iblock\Iblock",
				"reference" => array("=this.IBLOCK_ID" => "ref.ID"),
			),
			"PARENT_SECTION" => array(
				"data_type" => "\Bitrix\Iblock\Section",
				"reference" => array("=this.IBLOCK_SECTION_ID" => "ref.ID"),
			),
			"CREATED_BY_USER" => array(
				"data_type" => "\Bitrix\Main\User",
				"reference" => array("=this.CREATED_BY" => "ref.ID"),
			),
			"MODIFIED_BY_USER" => array(
				"data_type" => "\Bitrix\Main\User",
				"reference" => array("=this.MODIFIED_BY" => "ref.ID"),
			),
		);
	}

	/**
	 * Returns validators for NAME field.
	 *
	 * @return array
	 */
	public static function validateName()
	{
		return array(
			new BitrixEntity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for CODE field.
	 *
	 * @return array
	 */
	public static function validateCode()
	{
		return array(
			new BitrixEntity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for XML_ID field.
	 *
	 * @return array
	 */
	public static function validateXmlId()
	{
		return array(
			new BitrixEntity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for TMP_ID field.
	 *
	 * @return array
	 */
	public static function validateTmpId()
	{
		return array(
			new BitrixEntity\Validator\Length(null, 40),
		);
	}

	public static function save(array $data, $parentId = null)
	{
		static::modifyBeforeSave($data);

		if(null !== $parentId)
		{
			$data["IBLOCK_SECTION_ID"] = $parentId;

			if(!isset($data["XML_ID"]) && isset($data["NAME"]))
			{
				$rs = static::getList(array(
					"select" => array(
						"ID", "NAME", "DEPTH_LEVEL"
					),
					"filter" => array(
						"ID" => $parentId
					)

				));
				if($ar = $rs->Fetch())
				{
					$data["XML_ID"] = $data["IBLOCK_ID"]."|".md5($ar["NAME"]."|".$ar["DEPTH_LEVEL"])."|".md5($data["NAME"]);
				}
			}
		}

		if(!isset($data["XML_ID"]) && isset($data["NAME"]))
			$data["XML_ID"] = $data["IBLOCK_ID"]."|".md5($data["NAME"]);

		return parent::save($data, $parentId);
	}

	public static function setParent(array $data, array $parent)
	{
		return static::update($data["ID"], array("IBLOCK_SECTION_ID" => $parent["ID"]));
	}
}
?>