<?
namespace Xdev\Cp\Import\Entity;

use \Xdev\Cp\Internal\DataManager;

class ManagerTable extends DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return "xdev_cp_import_datamanager";
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			"MODULE_ID" => array(
				"data_type" => "string",
				"primary" => true,
			),
			"TYPE_ID" => array(
				"data_type" => "string",
				"primary" => true,
			),
			"CLASS" => array(
				"data_type" => "string",
				"required" => true,
			),
			"NAME" => array(
				"data_type" => "string",
			),
			"PRIORITY" => array(
				"data_type" => "integer",
				"required" => true,
			),
		);
	}

	public static function getManagerClassName($module_id, $type)
	{
		$ar = static::getRow(array(
			"filter" => array(
				"MODULE_ID" => $module_id,
				"TYPE_ID" => $type
			)
		));
		if(!is_array($ar))
			throw new \Exception("Entity manager not found", 1);

		return $ar["CLASS"];
	}

	public static function getImportModuleId()
	{
		return "iblock";
	}

	public static function getModuleDropdownList($name, $value)
	{
		$result = '<select name="' . htmlspecialchars($name) . '">';

		foreach(array("iblock") as $moduleId)
		{
			$result .= '<option value="' . $moduleId. '">' . $moduleId. '</option>';
		}

		$result .= '</select>';

		return $result;
	}
}
?>