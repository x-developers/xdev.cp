<?
namespace Xdev\Cp\Spider;

class AncorNode extends AttrNode
{
	protected static $defaultAttributte = "href";

	public static function getDescription()
	{
		return array(
			"TYPE" => "L",
			"NAME" => GetMessage("XDEV_CP_NODE_TYPE_L"),
			"CLASS" => __CLASS__,
		);
	}
}
?>