<?
namespace Xdev\Cp\Spider;

use Bitrix\Main\Localization\Loc;
use Xdev\Cp\Internal\DataManager;

Loc::loadMessages(__FILE__);

class UserAgentTable extends DataManager
{
	public static function getFilePath()
	{
		return __FILE__;
	}

	public static function getTableName()
	{
		return "xdev_cp_user_agent";
	}

	public static function getMap()
	{
		return array(
			"ID" => array(
				"data_type" => "integer",
				"primary" => true,
				"autocomplete" => true,
				"title" => Loc::getMessage("XDEV_CP_ID_FIELD"),
			),
			"ACTIVE" => array(
				"data_type" => "boolean",
				"values" => array("Y", "N"),
				"title" => Loc::getMessage("XDEV_CP_ACTIVE_FIELD"),
			),
			"NAME" => array(
				"data_type" => "string",
				"required" => true,
				"title" => Loc::getMessage("XDEV_CP_NAME_FIELD"),
			),
		);
	}
}
?>