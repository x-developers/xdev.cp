<?
namespace Xdev\Cp\Spider;

class ImageNode extends AttrNode
{
	protected static $defaultAttributte = "src";

	public static function getDescription()
	{
		return array(
			"TYPE" => "I",
			"NAME" => GetMessage("XDEV_CP_NODE_TYPE_I"),
			"CLASS" => __CLASS__,
		);
	}

	public function save($arParams=array(), $arCurrentParams=array())
	{
		if($this->isValid())
		{
			$arFile = \CFile::MakeFileArray($this->payload());
			if($arFile)
				$arCurrentParams["FILE_ID"] = \CFile::SaveFile($arFile, "xdev.cp/files");
		}

		return parent::save($arParams, $arCurrentParams);
	}
}
?>