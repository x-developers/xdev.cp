<?
namespace Xdev\Cp\Spider;

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Entity\Event;

use \Xdev\Cp\Internal\DataManager;
use \Xdev\Cp\Queue;

Loc::loadMessages(__FILE__);

class ResultNodeTable extends DataManager
{
	public static function getFilePath()
	{
		return __FILE__;
	}

	public static function getTableName()
	{
		return "xdev_cp_queue_result";
	}

	public static function getMap()
	{
		return array(
			"ID" => array(
				"data_type" => "integer",
				"primary" => true,
				"autocomplete" => true,
				"title" => Loc::getMessage("XDEV_CP_ID_FIELD"),
			),
			"NAME" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_NAME_FIELD")
			),
			"VALUE" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_VALUE_FIELD")
			),
			"ATTRIBUTES" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_QUEUE_RESULT_ATTRIBUTES_FIELD")
			),
			"FILE_ID" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_QUEUE_RESULT_FILE_ID_FIELD")
			),
			"NODE_ID" => array(
				"data_type" => "integer",
				"requried" => true,
				"title" => Loc::getMessage("XDEV_CP_QUEUE_RESULT_NODE_ID_FIELD")
			),
			"NODE" => array(
				"data_type" => "\\xdev\\cp\\Profile\\Node",
				"reference" => array("=this.NODE_ID" => "ref.ID")
			),
			"NODE_PAGE_ID" => array(
				"data_type" => "string",
				"expression" => array(
					"%s", "NODE.PAGE_ID"
				)
			),
			"NODE_TO_PAGE_ID" => array(
				"data_type" => "string",
				"expression" => array(
					"%s", "NODE.TO_PAGE_ID"
				)
			),
			"PARENT_ID" => array(
				"data_type" => "integer",
				"title" => Loc::getMessage("XDEV_CP_QUEUE_RESULT_PARENT_ID_FIELD")
			),
			"QUEUE_ID" => array(
				"data_type" => "integer",
				"requried" => true
			),
			"QUEUE" => array(
				"data_type" => "\\xdev\\cp\\Queue\\Queue",
				"reference" => array("=this.QUEUE_ID" => "ref.ID")
			),
			"SHEDULE_ID" => array(
				"data_type" => "string",
				"expression" => array(
					"%s", "QUEUE.SHEDULE_ID"
				)
			),
			"DATE_CREATE" => array(
				"data_type" => "datetime",
				"title" => Loc::getMessage("XDEV_CP_DATE_CREATE_FIELD"),
			),
			"TIMESTAMP_X" => array(
				"data_type" => "datetime",
				"title" => Loc::getMessage("XDEV_CP_TIMESTAMP_X_FIELD"),
			),
			"DEPTH_LEVEL" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_DEPTH_LEVEL_FIELD")
			),
			"RIGHT_MARGIN" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_RIGHT_MARGIN_FIELD")
			),
			"LEFT_MARGIN" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_LEFT_MARGIN_FIELD")
			),
			"INDEXED" => array(
				"data_type" => "string",
				"expression" => array(
					"CASE WHEN %s IS NULL THEN 'N' ELSE 'Y' END", "RIGHT_MARGIN"
				)
			),
		);
	}

	public static function add(array $data)
	{
		if(isset($data["ATTRIBUTES"]) && is_array($data["ATTRIBUTES"]))
			$data["ATTRIBUTES"] = serialize($data["ATTRIBUTES"]);

//        $data['VALUE'] = preg_replace('/[^\x20-\x7E]/', '', $data['VALUE']);

		return parent::add($data);
	}

	public static function update($primary, array $data)
	{
		if(isset($data["ATTRIBUTES"]) && is_array($data["ATTRIBUTES"]))
			$data["ATTRIBUTES"] = serialize($data["ATTRIBUTES"]);

		return parent::update($primary, $data);
	}

	public static function onDelete(Event $event)
	{
		$rs = static::getByPrimary(
			$event->getParameter("id"),
			array(
				"select" => array("ID", "FILE_ID"),
				"filter" => array("!FILE_ID" => false)
			)
		);
		if($ar = $rs->Fetch())
			\CFile::delete($ar["FILE_ID"]);
	}

	public static function resort($id)
    {
        $GLOBALS["DB"]->Query('UPDATE `xdev_cp_queue_result` r, `xdev_cp_queue_page` p SET r.`LEFT_MARGIN` = null, r.`RIGHT_MARGIN` = null WHERE r.QUEUE_ID = p.ID AND p.SHEDULE_ID = ' . intval($id));

        static::treeReSort($id);
    }

	public static function treeReSort($SHEDULE_ID, $ID = 0, $cnt = 0, $depth = 0)
	{
		$SHEDULE_ID = intval($SHEDULE_ID);

		if($ID > 0)
			static::update($ID, array("RIGHT_MARGIN" => $cnt, "LEFT_MARGIN" => $cnt));

		$cnt++;

		$res = static::getList(array(
			"select" => array("ID"),
			"filter" => array(
				"SHEDULE_ID" => $SHEDULE_ID,
				"PARENT_ID" => ($ID ? $ID : false),
			),
			"order" => array("ID" => "asc")
		));
		while($arr = $res->Fetch())
			$cnt = static::treeReSort($SHEDULE_ID, $arr["ID"], $cnt, $depth + 1);

		if($ID == 0)
			return true;

		static::update($ID, array("RIGHT_MARGIN" => $cnt, "DEPTH_LEVEL" => $depth));

		return $cnt + 1;
	}

	public static function _treeReSort($QUEUE_ID, $ID = 0, $cnt = 0, $depth = 0)
	{
		$QUEUE_ID = intval($QUEUE_ID);

		if($ID > 0)
			static::update($ID, array("RIGHT_MARGIN" => $cnt, "LEFT_MARGIN" => $cnt));

		$cnt++;

		$res = static::getList(array(
			"select" => array("ID"),
			"filter" => array(
				"QUEUE_ID" => $QUEUE_ID,
				"PARENT_ID" => ($ID ? $ID : false),
			)
		));
		while($arr = $res->Fetch())
			$cnt = static::_treeReSort($QUEUE_ID, $arr["ID"], $cnt, $depth + 1);

		if($ID == 0)
			return true;

		static::update($ID, array("RIGHT_MARGIN" => $cnt, "DEPTH_LEVEL" => $depth));

		return $cnt + 1;
	}

	public static function getChildList($id, $parameters = array())
	{
		$id = intval($id);
		if(!$id) throw new \Exception("Error Processing Request", 1);

		$rs = static::getList(array(
			"select" => array(
				"ID", "QUEUE_ID", "LEFT_MARGIN", "RIGHT_MARGIN"
			),
			"filter" => array(
				"ID" => $id
			)
		));

		if($ar = $rs->Fetch())
		{
			$arFilter = array(
				"QUEUE_ID" => $ar["QUEUE_ID"],
				">=LEFT_MARGIN" => ($ar["LEFT_MARGIN"] + 1),
				"<=RIGHT_MARGIN" => ($ar["RIGHT_MARGIN"] - 1),
			);

			if(!is_array($parameters["order"]))
				$parameters["order"] = array("LEFT_MARGIN" => "ASC");

			$parameters["filter"] = is_array($parameters["filter"])
				? array_merge($arFilter, $parameters["filter"])
				: $arFilter;
			return static::getList($parameters);
		}
		else
		{
			return static::getList(array("filter" => array("ID" => -1)));
			// throw new \Exception("Error Processing Request", 1);
		}
	}

	public static function getTreeList($parameters = array(), $id = false)
	{
		$arFilter = array();
		if($id)
		{
			$rs = static::getList(array(
				"select" => array(
					"ID", "QUEUE_ID",
					"LEFT_MARGIN", "RIGHT_MARGIN"
				),
				"filter" => array(
					"ID" => intval($id)
				),
				"limit" => 1
			));

			if($ar = $rs->Fetch())
			{
				$rsQ = Queue\QueueTable::getList(array(
					"select" => array("ID"),
					"filter" => array(
						"CREATED_NODE_ID" => $ar["ID"]
					)
				));
				$arQueue = array($ar["QUEUE_ID"]);
				while($arQ = $rsQ->Fetch())
					$arQueue[] = $arQ["ID"];

				$arFilter = array(
					"QUEUE_ID" => $arQueue,
					// "QUEUE_ID" => $ar["QUEUE_ID"],
					">=LEFT_MARGIN" => ($ar["LEFT_MARGIN"] + 1),
					"<=RIGHT_MARGIN" => ($ar["RIGHT_MARGIN"] - 1),
				);
			}
		}

		if(!is_array($parameters["order"]))
			$parameters["order"] = array("LEFT_MARGIN" => "ASC");

		$parameters["filter"] = is_array($parameters["filter"])
			? array_merge($arFilter, $parameters["filter"])
			: $arFilter;

		return static::getList($parameters);
	}

	public static function linkNodes($sheduleId, $pageId)
	{
		$rsLinkedNode = static::getList(array(
				"select" => array(
					"ID", "QUEUE_ID", "VALUE",
					"NODE_PAGE_ID", "NODE_TO_PAGE_ID"
				),
				"filter" => array(
					"!VALUE" => false,
					"!NODE_TO_PAGE_ID" => false,
					"NODE_PAGE_ID" => $pageId,
					"SHEDULE_ID" => $sheduleId
				)
			));
		$arUpdatedQueue = array();
		while($arLinkedNode = $rsLinkedNode->Fetch())
		{
			// skip pagenav element
			if($arLinkedNode["NODE_PAGE_ID"] <> $arLinkedNode["NODE_TO_PAGE_ID"])
			{
				$url = $arLinkedNode["VALUE"];
				$rsNode = static::getList(array(
						"select" => array(
							"ID", "QUEUE_ID", "VALUE",
							"NODE_PAGE_ID", "NODE_TO_PAGE_ID"
						),
						"filter" => array(
							"QUEUE.URL" => $url,
							"NODE.PARENT_ID" => false,
							"SHEDULE_ID" => $sheduleId
						)
					));
				while($arNode = $rsNode->Fetch())
				{
					if(!isset($arUpdatedQueue[$arNode["QUEUE_ID"]]))
					{
						$arUpdatedQueue[$arNode["QUEUE_ID"]] = Queue\QueueTable::update(
							$arNode["QUEUE_ID"],
							array(
								"CREATED_NODE_ID" => $arLinkedNode["ID"]
							)
						);
					}
					static::update($arNode["ID"], array("PARENT_ID" => $arLinkedNode["ID"]));
				}
			}
		}
		unset($arUpdatedQueue);
	}

	public static function saveFiles($arFilter = array(), $DIR_NAME = false)
	{
		if(false === $DIR_NAME)
			$DIR_NAME = "xdev.cp/files";

		$rs = static::getList(array(
			"select" => array("ID", "VALUE", "FILE_ID"),
			"filter" => array_merge(
				$arFilter,
				array(
					"NODE.TYPE" => "I",
					"!VALUE" => false,
					// "FILE_ID" => false
				)
			)
		));
		$count = 0;
		while($ar = $rs->Fetch())
		{
			$bSave = true;
			if($ar["FILE_ID"])
			{
				$path = \CFile::GetPath($ar["FILE_ID"]);
				if(empty($path))
				{
					\CFile::delete($ar["FILE_ID"]);
				}
				else
				{
					$bSave = false;
				}
			}

			if($bSave)
			{
				$arFile = \CFile::MakeFileArray($ar["VALUE"]);
				if(is_array($arFile))
				{
					$fileId = \CFile::SaveFile($arFile, $DIR_NAME);
					if($fileId)
					{
						$count++;
						static::update($ar["ID"], array("FILE_ID" => $fileId));
					}
				}
			}
		}

		return $count;
	}
}
?>