<?
namespace Xdev\Cp\Spider;

use Xdev\Cp\Spider\UserAgentTable;

class HttpClient extends \Bitrix\Main\Web\HttpClient
{
	private static $arUserAgents = null;

	public function __construct(array $options = null)
	{
		parent::__construct(array_merge(
			$options,
			array(
				"version" => static::HTTP_1_1
			)
		));
		// $this->setCookies(array("PHPSESSID" => "id6uib1v0q21p3ol89gceduml1"));
	}

	public function setRandomUserAgent()
	{
		if(!is_array(static::$arUserAgents))
		{
			static::$arUserAgents = array();

			$rs = UserAgentTable::getList(array(
				"filter" => array("ACTIVE" => "Y")
			));
			while($ar = $rs->Fetch())
				static::$arUserAgents[] = $ar["NAME"];
		}

		if(!empty(static::$arUserAgents))
		{
			$key = array_rand(static::$arUserAgents);
			$this->setHeader("User-Agent", static::$arUserAgents[$key]);
		}

		return $this;
	}
}
?>