<?
namespace Xdev\Cp\Spider;

class AttrNode extends ScalarNode
{
	protected static $attrRegexp = '@\[(?P<payload>[a-z-]+)(:?[^\]]+)?\]@';

	protected static $defaultAttributte;

	public function getPayloadAttribute()
	{
		if(preg_match(static::$attrRegexp, $this->profile->getCSSselector(), $matches))
			return $matches["payload"];

		return static::$defaultAttributte;
	}

	public function payload()
	{
		$url = $this->rel2abs(
			$this->domElement->getAttribute(
				$this->getPayloadAttribute()
		));

		$exp = $this->getProfileNode()->get("EXPRESSION");
		if(!empty($exp))
			$url = sprintf($exp, $url);

		return $url;
	}

	public function isValid()
	{
		return preg_match("@^http[s]?://@", $this->payload());
	}
}
?>