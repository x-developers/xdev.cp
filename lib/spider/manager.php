<?
namespace Xdev\Cp\Spider;

use \Bitrix\Main\ArgumentException;

use \Xdev\Cp\Profile\NodeTable as ProfileNodeTable;

class Manager
{
    protected static $baseUriRegexp = '@(?P<payload>http[s]?://.+?\.(?:ru|com|net|su))@';
    /**
     * @var array
     */
    protected $queue;

    public function __construct(array $queue)
    {
        $this->queue = $queue;
    }

    /**
     * @param string $url
     *
     * @return string
     */
    private function getCacheFilePath($url)
    {
        return $_SERVER['DOCUMENT_ROOT'].'/upload/xdev.cp/cache/'.md5($url).'.html';
    }

    /**
     * @param string $url
     * @param string $html
     *
     * @return bool
     */
    private function saveCacheFile($url, $html)
    {
        CheckDirPath($this->getCacheFilePath($url));
        return !!file_put_contents($this->getCacheFilePath($url), $html);
    }

    /**
     * @param string $url
     *
     * @return string
     */
    private function getFromCache($url)
    {
        return file_get_contents($this->getCacheFilePath($url));
    }

    /**
     * @param string $url
     *
     * @return string
     */
    private function cacheFileExists($url)
    {
        return file_exists($this->getCacheFilePath($url));
    }

    /**
     * Send HTTP GET request
     *
     * @param string  $url            absolute url
     * @param boolean $bRandUserAgent set random user-agent
     * @param string  $cacheType      enum (E - cache enabled, D - non't cache ,U - don't use cache)
     *
     * @return string        html response
     * @throws \Exception
     */
    private function get($url, $bRandUserAgent = true, $cacheType = 'E')
    {
        if ($cacheType == 'E' && ($result = $this->getFromCache($url))) {
            return $result;
        }

        $client = new HttpClient();
        if ($bRandUserAgent) {
            $client->setRandomUserAgent();
        }

        $result = $client->get($url);

        if (!$result || !strlen($result)) {
            throw new \Exception("Http request error. Empty response", 1);
        }

        if ($cacheType != 'D') {
            $this->saveCacheFile($url, $result);
        }

        return $result;
    }

    private function baseFromUrl($url)
    {
        if (preg_match(static::$baseUriRegexp, $url, $matches)) {
            return $matches["payload"];
        }

        throw new ArgumentException("Error get base url", $url);
    }

    /**
     * Parse profile page by url
     *
     * @param integer $pageId    profile page id
     * @param string  $url       absolute url
     * @param array   $requestParams
     * @param string  $cacheType enum (E - cache enabled, D - non't cache ,U - don't use cache)
     * @param array
     *
     * @return ScalarNode[]
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function parseProfilePage($pageId, $url, $requestParams = [], $cacheType = 'E')
    {
        $url = preg_replace("#/+#", "/", $url);
        $url = preg_replace("#:/#", "://", $url);

        $content = $this->get($url, $requestParams, $cacheType);

        $rsProfileNodes = ProfileNodeTable::getList(
            [
                "filter" => [
                    "ACTIVE" => "Y",
                    "PAGE_ID" => $pageId,
                    "PARENT_ID" => null,
                ],
            ]
        );

        $result = [];
        while ($arProfileNode = $rsProfileNodes->Fetch()) {
            $node = new NodeProvider($arProfileNode["ID"], $this->baseFromUrl($url));

            $node->resolve($content);

            foreach ($node->getChildren() as $arRes) {
                $result[] = $arRes;
            }

            unset($node);
        }

        return $result;
    }

    /**
     * @return \Xdev\Cp\Spider\ScalarNode[]
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function parse()
    {
        return $this->parseProfilePage($this->queue['PAGE_ID'], $this->queue['URL'], $this->queue['CACHE_TYPE']);
    }
}
