<?
namespace Xdev\Cp\Spider;

use \Xdev\Cp\Profile\NodeTable as ProfileNodeTable;
use \Xdev\Cp\Profile\NodeObject as ProfileNodeObject;

use \Xdev\Cp\Spider\ScalarNode;
use \Xdev\Cp\Spider\NodeManager;

use \Bitrix\Main\ArgumentException;

class NodeProvider
{
	/**
	 * @var ProfileNodeObject
	 */
	private $profile;

	/**
	 * @var array[ProfileNodeObject]
	 */
	private $childProfile = array();

	/**
	 * @var array[Node]
	 */
	private $nodes = array();

	/**
	 * @var string
	 */
	private $context;

	/**
	 * @var integer
	 */
	private $depthLevel;

	/**
	 * @var string
	 */
	private $baseURI;

	private $doc;

	/**
	 * @param integer $profileNodeId profile node ID
	 * @param integer $depthLevel
	 */
	public function __construct($profileNodeId, $baseURI = null)
	{
		if($profileNodeId)
			$this->setProfileNode($profileNodeId);

		if($baseURI !== null)
			$this->baseURI = $baseURI;
	}

	public function __destruct()
	{
		if($this->doc)
		{
			$this->doc->unloadDocument();
			unset($this->doc);
		}
	}

	public function setBaseURI($baseURI)
	{
		$this->baseURI = $baseURI;

		return $this;
	}

	/**
	 * Sets profile node to parse
	 * @param integer $profileNodeId profile node ID
	 * @param array  $arParams additional parameters
	 * @throws ArgumentException
	 * @return NodeProvider
	 */
	public function setProfileNode($nodeId, $arParams=array())
	{
		$row = ProfileNodeTable::getRowById($nodeId);
		if(!is_array($row))
			throw new ArgumentException("ProfileNode not found", $nodeId);

		$this->profile = new ProfileNodeObject(array_merge($row, $arParams));
		$this->childProfile = array();

		$rs = ProfileNodeTable::getList(array(
			"filter" => array(
				"PARENT_ID" => $row["ID"],
				"PAGE_ID" => $row["PAGE_ID"],
			)
		));
		while($ar = $rs->Fetch())
			$this->childProfile[] = new ProfileNodeObject($ar);

		return $this;
	}

	/**
	 * @return \Xdev\Cp\Profile\NodeObject
	 */
	public function getProfileNode()
	{
		return $this->profile;
	}

	/**
	 * Resolve context
	 * @param  string|\DomNode $context search context
	 * @return NodeProvider
	 */
	private function resolveContext($context)
	{
		if(is_object($context) && $context instanceof \DomNode)
		{
			$doc = new \DOMDocument();

			// remove root node
			foreach($context->childNodes as $childNode)
				$doc->appendChild($doc->importNode($childNode, true));

			$this->context = $doc->saveHTML();
		}
		else
		{
			$this->context = $context;
		}

		return $this;
	}

	private function createDomNode($element)
	{
		return NodeManager::create($element, $this->profile, $this->baseURI);
	}

	/**
	 * @param  string|\DomNode $context search context
	 * @return NodeProvider
	 */
	public function resolve($context)
	{
		$this->resolveContext($context);

		$this->nodes = array();
//TODO: regexp value
		foreach($this->find($this->profile->getCSSselector()) as $index => $element)
		{
			$findNode = $this->createDomNode($element);

			// find recursive
			foreach($this->childProfile as $profileNode)
			{
				$node = (new self($profileNode->getId(), $this->baseURI))
						->resolve($element);

				foreach($node->getChildren() as $childenNode)
					$findNode->appendChild($childenNode);
			}

			$id = md5($findNode->getId()."|index:".$index);
			$findNode->setId($id);

			$this->nodes[] = $findNode;
//TODO: remove it
			// if(count($this->nodes) > 1) break;
		}

		return $this;
	}

	/**
	 * @return array[DomElement]
	 */
	public function getChildren()
	{
		return $this->nodes;
	}

	/**
	 * @return array[array] (@see Node)
	 */
	public function getNodeTree($depthLevel = 1)
	{
		$result = array();

		foreach($this->nodes as $domNode)
		{
			foreach($domNode->getTree($depthLevel) as $val)
				$result[] = $val;
		}

		return $result;
	}

	/**
	 * @param  string css selector
	 * @return DomDocument
	 */
	private function find($selector)
	{
		require_once("phpquery.php");

		$this->doc = \phpQuery::newDocument($this->context);

		return \phpQuery::pq($selector);
	}
}
?>