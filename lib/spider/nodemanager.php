<?
namespace Xdev\Cp\Spider;

class NodeManager
{
	const DEFAULT_TYPE = "S";

	private static $nodeMap = array(
		"S" => "\\Xdev\\Cp\\Spider\\ScalarNode",
		"L" => "\\Xdev\\Cp\\Spider\\AncorNode",
		"I" => "\\Xdev\\Cp\\Spider\\ImageNode"
	);

	public static function getRegisteredNodes()
	{
		$result = array();
		foreach(static::$nodeMap as $classname)
		{
			$ar = $classname::getDescription();
			$result[$ar["TYPE"]] = $ar;
		}

		return $result;
	}

	public static function getRegisteredNodeById($value)
	{
		$ar = static::getRegisteredNodes();

		return isset($ar[$value]) ? $ar[$value] : null;
	}

	public static function create($element, $profile, $baseURI)
	{
		$ar = static::getRegisteredNodeById($profile->get("TYPE"));
		if(!$ar)
			$ar = static::getRegisteredNodeById(static::DEFAULT_TYPE);

		$classname = $ar["CLASS"];

		return new $classname($element, $profile, $baseURI);
	}
}