<?
namespace Xdev\Cp\Spider;

use \Xdev\Cp\internal\HasId;
use \Xdev\Cp\Profile\NodeObject;

class ScalarNode extends HasId
{
    /**
     * @var NodeObject
     */
    protected $profile;
    /**
     * @var \DOMElement
     */
    protected $domElement;
    /**
     * @var array[Node]
     */
    protected $childNodes = [];
    /**
     * @var string
     */
    protected $baseURI;

    public static function getDescription()
    {
        return [
            "TYPE" => "S",
            "NAME" => GetMessage("XDEV_CP_NODE_TYPE_S"),
            "CLASS" => __CLASS__,
        ];
    }

    public function __construct(\DOMElement $domElement, NodeObject $profileNode, $baseURI)
    {
        $this->domElement = $domElement;
        $this->profile = $profileNode;
        $this->baseURI = $baseURI;
    }

    public function getBaseURI()
    {
        return $this->baseURI;
    }

    /**
     * @return string
     */
    public function getId()
    {
        if (empty($this->id)) {
            $this->id = md5(
                serialize(
                    [
                        $this->profile->getId(),
                        $this->getAttributes(),
                    ]
                )
            );
        }

        return $this->id;
    }

    /**
     * @return \DOMElement
     */
    public function getdomElement()
    {
        return $this->domElement;
    }

    /**
     * @param NodeObject $node
     *
     * @return ScalarNode
     */
    public function setProfileNode(NodeObject $node)
    {
        $this->profile = $node;

        return $this;
    }

    /**
     * @return NodeObject
     */
    public function getProfileNode()
    {
        return $this->profile;
    }

    /**
     * @return array[ScalarNode]
     */
    public function getChildNodes()
    {
        return $this->childNodes;
    }

    /**
     * @param ScalarNode $child
     *
     * @return ScalarNode
     */
    public function appendChild(ScalarNode $child)
    {
        $this->childNodes[] = $child;

        return $this;
    }

    /**
     * @return array[string][string]
     */
    public function getAttributes()
    {
        $attr = [];
        foreach ($this->domElement->attributes as $ob) {
            $attr[$ob->name] = $ob->value;
        }

        return $attr;
    }

    /**
     * @param integer $depthLevel root depth level
     *
     * @return array[string]
     */
    private function getArrayNode($depthLevel = 1)
    {
        return [
            // "TMP_ID" => $this->getId(),
            "NAME" => toUpper($this->domElement->tagName),
            "VALUE" => $this->payload(),
            "ATTRIBUTES" => $this->getAttributes(),
            "NODE_ID" => $this->profile->getId(),
            "DEPTH_LEVEL" => $depthLevel,
        ];
    }

    /**
     * @param integer $depthLevel root depth level
     *
     * @return array[array]
     */
    public function getTree($depthLevel = 0)
    {
        $result = [$this];
        foreach ($this->childNodes as $childNode) {
            foreach ($childNode->getTree($depthLevel + 1) as $val) {
                $result[] = $val;
            }
        }

        return $result;
    }

    public function save($arParams = [], $arCurrentParams = [])
    {
        $result = ResultNodeTable::add(array_merge($this->getArrayNode(), $arCurrentParams, $arParams));
        $this->id = $result->getId();

        if ($result->isSuccess(true)) {
            foreach ($this->childNodes as $childNode) {
                $res = $childNode->save($arParams, ["PARENT_ID" => $result->getId()]);
                if (!$res->isSuccess()) {
                    foreach ($res->getErrors() as $error) {
                        $result->addError($error);
                    }
                }
            }
        }

        return $result;
    }

    public function payload()
    {
        return trim($this->domElement->textContent);
    }

    protected function rel2abs($uri)
    {
        if (strpos($uri, "#") === 0) {
            return "";
        }

        $uri = preg_replace("#^//#", "http://", $uri);
        if (!preg_match("#http[s]?://#i", $uri)) {
            $baseURI = rtrim($this->getBaseURI(), "/");
            $uri = $baseURI."/".$uri;
        }

        $uri = preg_replace("#/+#", "/", $uri);
        $uri = preg_replace("#:/#", "://", $uri);

        return $uri;
    }

    public function isValid()
    {
        return true;
    }
}
