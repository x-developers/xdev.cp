<?
namespace xdev\cp\admin;

use Bitrix\Main\Localization\Loc;
use Xdev\Cp\Entity\DataManager;

Loc::loadMessages(__FILE__);

class tools
{
        public static function onBuildGlobalMenuHandler(&$aGlobalMenu, &$aModuleMenu)
        {
		if(!isset($aGlobalMenu["global_menu_xdev_cp"]))
		{
			$aGlobalMenu["global_menu_xdev_cp"] = array(
				"menu_id" => "xdev_cp",
				"text" => GetMessage("XDEV_CP_MENU_GLOBAL_TEXT"),
				"title" => GetMessage("XDEV_CP_MENU_GLOBAL_TITLE"),
				"sort" => 500,
				"items_id" => "global_menu_xdev_cp",
				"items" => array()
			);

			$RIGHTS = $GLOBALS["APPLICATION"]->GetGroupRight("xdev.cp");
			if($RIGHTS > "D")
			{
				$arItems = array();
				if($RIGHTS >= "W")
				{
					$arItems[] = array(
						"text" => GetMessage("XDEV_CP_MENU_SERVER_PROFILE_LIST"),
						"url" => "xdev_cp_profile_list.php?lang=".LANGUAGE_ID,
						"title" => GetMessage("XDEV_CP_MENU_SERVER_PROFILE_LIST_ALT"),
						"more_url" => array("xdev_cp_profile_edit.php")
					);
					$arItems[] = array(
						"text" => GetMessage("XDEV_CP_MENU_SERVER_PAGE_LIST"),
						"url" => "xdev_cp_page_list.php?lang=".LANGUAGE_ID,
						"title" => GetMessage("XDEV_CP_MENU_SERVER_PAGE_LIST_ALT"),
						"more_url" => array("xdev_cp_page_edit.php")
					);
				}
				if($RIGHTS >= "M")
				{
					$arItems[] = array(
						"text" => GetMessage("XDEV_CP_MENU_SERVER_SHEDULE_LIST"),
						"url" => "xdev_cp_shedule_list.php?lang=".LANGUAGE_ID,
						"title" => GetMessage("XDEV_CP_MENU_SERVER_SHEDULE_LIST_ALT"),
						"more_url" => array("xdev_cp_shedule_detail.php", "xdev_cp_queue_list.php", "xdev_cp_queue_node_list.php")
					);
				}

				$aMenu = array(
					"parent_menu" => "global_menu_xdev_cp",
					"sort" => 100,
					"text" => GetMessage("XDEV_CP_MENU_SERVER"),
					"title"=> GetMessage("XDEV_CP_MENU_SERVER_ALT"),
					"icon" => "cluster_menu_icon",
					"items_id" => "menu_xdev.cp_server",
					"items" => $arItems
				);

				if(!empty($aMenu["items"]))
					$aGlobalMenu["global_menu_xdev_cp"]["items"][] = $aMenu;

				$arItems = array();
				if($RIGHTS >= "M")
				{
					$arItems[] = array(
						"text" => GetMessage("XDEV_CP_MENU_CLIENT_UAGENT_LIST"),
						"url" => "xdev_cp_uagent_list.php?lang=".LANGUAGE_ID,
						"title" => GetMessage("XDEV_CP_MENU_CLIENT_UAGENT_LIST_ALT"),
						"more_url" => array("xdev_cp_uagent_edit.php")
					);
				}
				$aMenu = array(
					"parent_menu" => "global_menu_xdev_cp",
					"sort" => 200,
					"text" => GetMessage("XDEV_CP_MENU_CLIENT"),
					"title"=> GetMessage("XDEV_CP_MENU_CLIENT_ALT"),
					"icon" => "clouds_menu_icon",
					"items_id" => "menu_xdev.cp_client",
					"items" => $arItems
				);

				if(!empty($aMenu["items"]))
					$aGlobalMenu["global_menu_xdev_cp"]["items"][] = $aMenu;

				$arItems = array();
				if($RIGHTS >= "M")
				{
					$arItems[] = array(
						"text" => GetMessage("XDEV_CP_MENU_SERVER_EXPORT_LIST"),
						"url" => "xdev_cp_export_list.php?lang=".LANGUAGE_ID,
						"title" => GetMessage("XDEV_CP_MENU_SERVER_EXPORT_LIST_ALT"),
						"more_url" => array("xdev_cp_export_detail.php")
					);
					$arItems[] = array(
						"text" => GetMessage("XDEV_CP_MENU_SERVER_EXPORT_ENTITY_LIST"),
						"url" => "xdev_cp_export_entity_list.php?lang=".LANGUAGE_ID,
						"title" => GetMessage("XDEV_CP_MENU_SERVER_EXPORT_ENTITY_LIST_ALT"),
						"more_url" => array("xdev_cp_export_entity_edit.php", "xdev_cp_export_field_edit.php")
					);
				}
				$aMenu = array(
					"parent_menu" => "global_menu_xdev_cp",
					"sort" => 200,
					"text" => GetMessage("XDEV_CP_MENU_EXPORT"),
					"title"=> GetMessage("XDEV_CP_MENU_EXPORT_ALT"),
					"icon" => "workflow_menu_icon",
					"items_id" => "menu_xdev.cp_export",
					"items" => $arItems
				);

				if(!empty($aMenu["items"]))
					$aGlobalMenu["global_menu_xdev_cp"]["items"][] = $aMenu;
			}
		}
        }

        public static function drawFormFieldRow(&$tabControl, &$field, $can_edit)
        {
		if($can_edit)
		{
			if($field instanceof \Bitrix\Main\Entity\BooleanField)
			{
				$tabControl->BeginCustomField($field->getName(), $field->getTitle() . ":", $field->isRequired());

				?><tr>
					<td width="40%"><? echo $tabControl->GetCustomLabelHTML(); ?></td>
					<td><?

					?><input type="checkbox" name="<? echo $field->getName(); ?>" value="Y"<? if($GLOBALS["str_" . $field->getName()] == "Y") echo " checked"; ?>><?

					?></td>
				</tr><?

				$tabControl->EndCustomField($field->getName());

			}
			elseif($field instanceof \Bitrix\Main\Entity\EnumField)
			{
				$tabControl->BeginCustomField($field->getName(), $field->getTitle() . ":", $field->isRequired());

				?><tr>
					<td width="40%"><? echo $tabControl->GetCustomLabelHTML(); ?></td>
					<td><?

					$arr = array(
						"REFERENCE_ID" => array(),
						"REFERENCE" => array(),
					);
					foreach($field->getValues() as $val)
					{
						$arr["REFERENCE"][] = ($val == "Y" ? GetMessage("MAIN_YES") : ($val == "N" ? GetMessage("MAIN_NO") : $val));
						$arr["REFERENCE_ID"][] = $val;
					}
					echo SelectBoxFromArray($field->getName(), $arr,  $GLOBALS["str_" . $field->getName()]);

					?></td>
				</tr><?

				$tabControl->EndCustomField($field->getName());

			}
			elseif($field instanceof \Bitrix\Main\Entity\DatetimeField)
			{
				$tabControl->AddCalendarField($field->getName(), $field->getTitle() . ":", $GLOBALS["str_" . $field->getName()], $field->isRequired());
			}
			else
			{
				$tabControl->AddEditField($field->getName(), $field->getTitle() . ":", $field->isRequired(), array("size" => (in_array($field->getName(), array("SORT", "TIME_LIMIT")) ? 4 : 50), "maxlength" => 255), $GLOBALS["str_" . $field->getName()]);
			}
		}
		else
		{
			$val = $GLOBALS["str_" . $field->getName()];

			$tabControl->BeginCustomField($field->getName(), $field->getTitle() . ":", $field->isRequired());

			?><tr>
				<td width="40%"><? echo $tabControl->GetCustomLabelHTML(); ?></td>
				<td><?

				if($field instanceof \Bitrix\Main\Entity\BooleanField)
				{
					echo GetMessage($val == "Y" ? "MAIN_YES" : "MAIN_NO");
				}
				else
				{
					echo htmlspecialchars($val);
				}
					?><input type="hidden" name="<? echo $field->getName(); ?>" value="<? echo htmlspecialchars($val); ?>"><?
				?></td>
			</tr><?

			$tabControl->EndCustomField($field->getName());
		}
        }

        public static function isBooleanField($field)
        {
        	if($field->getDataType() != "boolean")
        		return false;

        	if(
        		(count($field->getValues()) == 2)
        		&& in_array("Y", $field->getValues())
        		&& in_array("N", $field->getValues())
        	)
        		return true;
        }
	public static function getDropDownList(array $arrItems, $value, $strFirstName, $strSecondName, $strFirstChoose, $strSecondChoose, $onChangeFirst = '', $onChangeSecond = '', $strAddFirst = '', $strAddSecond = '')
	{
		$value = intval($value);

		$filterId = md5(serialize(array($strFirstName, $strSecondName, $strFirstChoose, $strSecondChoose)));

		$html = '
		<script type="text/javascript">
		function OnType_'.$filterId.'_Changed(typeSelect, iblockSelectID)
		{
			var arIBlocks = '.\CUtil::PhpToJSObject($arrItems).';
			var iblockSelect = BX(iblockSelectID);
			if(!!iblockSelect)
			{
				for(var i=iblockSelect.length-1; i > 0; i--)
					iblockSelect.remove(i);
				for(var j in arIBlocks[typeSelect.value].ITEMS)
				{
					var newOption = new Option(arIBlocks[typeSelect.value].ITEMS[j], j, false, false);
					iblockSelect.options.add(newOption);
				}
			}
		}
		</script>
		';

		$PROFILE = false;
		if($value > 0)
		{
			foreach($arrItems as $id => $ar)
			{
				if(array_key_exists($value, $ar["ITEMS"]))
				{
					$PROFILE = $id;
					break;
				}
			}
		}

		$htmlTypeName = htmlspecialcharsbx($strFirstName);
		$htmlIBlockName = htmlspecialcharsbx($strSecondName);
		$onChangeFirst = 'OnType_'.$filterId.'_Changed(this, \''.\CUtil::JSEscape($strSecondName).'\');'.$onChangeFirst.';';
		$onChangeSecond = trim($onChangeSecond);

		$html .= '<select name="'.$htmlTypeName.'" id="'.$htmlTypeName.'" onchange="'.htmlspecialcharsbx($onChangeFirst).'" '.$strAddFirst.'>'."\n";
		$html 	.= '<option value="">'.htmlspecialcharsEx($strFirstChoose).'</option>'."\n";

		foreach($arrItems as $key => $val)
		{
			if($PROFILE === false)
				$PROFILE = $key;
			$html .= '<option value="'.htmlspecialcharsbx($key).'"'.($PROFILE===$key? ' selected': '').'>'.htmlspecialcharsEx($val["NAME"]).'</option>'."\n";
		}
		$html .= "</select>\n";
		$html .= "&nbsp;\n";
		$html .= '<select name="'.$htmlIBlockName.'" id="'.$htmlIBlockName.'"'.($onChangeSecond != ''? ' onchange="'.htmlspecialcharsbx($onChangeSecond).'"': '').' '.$strAddSecond.'>'."\n";
		$html 	.= '<option value="">'.htmlspecialcharsEx($strSecondChoose).'</option>'."\n";
		foreach($arrItems[$PROFILE]["ITEMS"] as $key => $val)
		{
			$html .= '<option value="'.htmlspecialcharsbx($key).'"'.($value==$key? ' selected': '').'>'.htmlspecialcharsEx($val).'</option>'."\n";
		}
		$html .= "</select>\n";

		return $html;
	}
}
?>