<?
namespace xdev\cp\internal;

abstract class HasId
{
	protected $id;

	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	public function getId()
	{
		return $this->id;
	}
}
?>