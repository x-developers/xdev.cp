<?
namespace Xdev\Cp\Internal;

use Bitrix\Main\Entity;
use Bitrix\Main\Type;

class DataManager extends Entity\DataManager
{
	/**
	 * Adds row to entity table
	 *
	 * @param array $data
	 *
	 * @return \Bitrix\Main\Entity\AddResult Contains ID of inserted row
	 *
	 * @throws \Exception
	 */
	public static function add(array $data)
	{
		$arMap = static::getMap();

		if(isset($arMap["DATE_CREATE"]))
		{
			$data["DATE_CREATE"] = new Type\DateTime();
		}

		if(isset($arMap["TIMESTAMP_X"]))
		{
			$data["TIMESTAMP_X"] = new Type\DateTime();
		}

		return parent::add($data);
	}

	/**
	 * Updates row in entity table by primary key
	 *
	 * @param mixed $primary
	 * @param array $data
	 *
	 * @return \Bitrix\Main\Entity\UpdateResult
	 *
	 * @throws \Exception
	 */
	public static function update($primary, array $data)
	{
		$arMap = static::getMap();

		if(isset($arMap["TIMESTAMP_X"]))
		{
			$data["TIMESTAMP_X"] = new Type\DateTime();
		}

		return parent::update($primary, $data);
	}

	/**
	 * Returns validators for XML_ID field.
	 *
	 * @return array
	 */
	public static function validateXmlId()
	{
		return array(
			new Entity\Validator\Unique(),
			new Entity\Validator\Length(null, 200),
		);
	}

	/**
	 * @param \Bitrix\Main\Entity\Event
	 *
	 * @return \Bitrix\Main\Entity\EventResult
	 */
	public static function __onDelete(\Bitrix\Main\Entity\Event $event)
	{
	}
}
?>