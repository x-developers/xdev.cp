<?
namespace Xdev\Cp\Internal;

class ListResult extends \Bitrix\Main\Entity\Result
{
	private $resultItems = array();

	public function addResultItem($item)
	{
		$this->resultItems[] = $item;

		return $this;
	}

	public function getResultTree()
	{
		return $this->resultItems;
	}
}
?>