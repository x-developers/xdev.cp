<?
namespace Xdev\Cp\Internal\Validator;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class UniqueUrl extends \Bitrix\Main\Entity\Validator\Base
{
	protected $primaryName;

	public function __construct($primaryName)
	{
		$this->primaryName = $primaryName;
		parent::__construct();
	}

	/**
	 * @var string
	 */
	protected $errorPhraseCode = 'XDEV_CP_ENTITY_VALIDATOR_UNIQUE_URL';

	public function validate($value, $primary, array $row, Entity\Field $field)
	{
		$primaryValue = $row[$this->primaryName];
		if(isset($primaryValue))
		{
			$entity = $field->getEntity();
			$query = new Entity\Query($entity);

			$query->setSelect(array_merge(
				$entity->getPrimaryArray(),
				array($this->primaryName, $field->getName())
			));

			$arFilter = array('='.$this->primaryName => $primaryValue, '='.$field->getName() => $value);
			if(isset($primary))
			{
				foreach($primary as $k => $v)
					$arFilter["!" . $k] = $v;
			}
			$query->setFilter($arFilter);

			$query->setLimit(1);

			$result = $query->exec();
			if($existing = $result->fetch())
			{
				return $this->getErrorMessage($value, $field);
			}
		}

		return true;
	}
}
