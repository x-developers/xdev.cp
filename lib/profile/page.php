<?
namespace xdev\cp\profile;

use Bitrix\Main\Localization\Loc;

use Xdev\Cp\Internal\DataManager;
use Xdev\Cp\Admin\Tools;

Loc::loadMessages(__FILE__);

class PageTable extends DataManager
{
	public static function getFilePath()
	{
		return __FILE__;
	}

	public static function getTableName()
	{
		return "xdev_cp_prof_page";
	}

	public static function getMap()
	{
		return array(
			"ID" => array(
				"data_type" => "integer",
				"primary" => true,
				"autocomplete" => true,
				"title" => Loc::getMessage("XDEV_CP_ID_FIELD"),
			),
			"NAME" => array(
				"data_type" => "string",
				"required" => true,
				"title" => Loc::getMessage("XDEV_CP_NAME_FIELD"),
			),
			"PROFILE_ID" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_PAGE_PROFILE_ID_FIELD"),
			),
			"TIMESTAMP_X" => array(
				"data_type" => "datetime",
				"title" => Loc::getMessage("XDEV_CP_TIMESTAMP_X_FIELD"),
			),
			"DATE_CREATE" => array(
				"data_type" => "datetime",
				"title" => Loc::getMessage("XDEV_CP_DATE_CREATE_FIELD"),
			),
		);
	}

	public static function getDropDownListEx($ID, $strProfileName, $strPageName, $arFilter = array())
	{
		$arrItems = array();
		$rs = ProfileTable::getList(array(
			"select" => array(
				"ID", "NAME"
			),
			"order" => array("NAME" => "ASC")
		));
		while($ar = $rs->Fetch())
		{
			$arr = array(
				"NAME" => $ar["NAME"]." [".$ar["ID"]."]",
				"ITEMS" => array()
			);
			$rsIBlocks = PageTable::getList(array(
				"select" => array(
					"ID", "NAME", "PROFILE_ID"
				),
				"filter" => array_merge($arFilter, array(
					"PROFILE_ID" => $ar["ID"]
				)),
				"order" => array("NAME" => "ASC")
			));
			while($arIBlock = $rsIBlocks->Fetch())
			{
				$arr["ITEMS"][$arIBlock["ID"]] = $arIBlock["NAME"]." [".$arIBlock["ID"]."]";
			}
			$arrItems[$ar["ID"]] = $arr;
		}

		return Tools::getDropDownList($arrItems, $ID, $strProfileName, $strPageName, GetMessage("XDEV_CP_PAGE_CHOOSE_PAGE_TYPE"), GetMessage("XDEV_CP_PAGE_CHOOSE_PAGE"));
	}
}
?>
