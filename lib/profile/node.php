<?
namespace xdev\cp\profile;

use Bitrix\Main\Localization\Loc;

use \Xdev\Cp\Internal\DataManager;
use \Xdev\Cp\Admin\Tools;

Loc::loadMessages(__FILE__);

class NodeTable extends DataManager
{
	public static function getFilePath()
	{
		return __FILE__;
	}

	public static function getTableName()
	{
		return "xdev_cp_prof_node";
	}

	public static function getMap()
	{
		return array(
			"ID" => array(
				"data_type" => "integer",
				"primary" => true,
				"autocomplete" => true,
				"title" => Loc::getMessage("XDEV_CP_ID_FIELD"),
			),
			"PAGE_ID" => array(
				"data_type" => "integer",
				"required" => true,
				"title" => Loc::getMessage("XDEV_CP_NODE_PAGE_ID_FIELD"),
			),
			"PAGE" => array(
				"data_type" => "\\xdev\\cp\\Profile\\Page",
				"reference" => array("=this.PAGE_ID" => "ref.ID"),
			),
			"TO_PAGE_ID" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_NODE_TO_PAGE_ID_FIELD"),
			),
			"PARENT_ID" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_NODE_PARENT_ID_FIELD"),
			),
			"NAME" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_NAME_FIELD"),
			),
			"CSS_SELECTOR" => array(
				"data_type" => "string",
				"required" => true,
				"title" => Loc::getMessage("XDEV_CP_NODE_CSS_SELECTOR_FIELD"),
			),
			"TYPE" => array(
				"data_type" => "string",
				"required" => true,
				"title" => Loc::getMessage("XDEV_CP_NODE_TYPE_FIELD"),
			),
			"REGEXP" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_NODE_REGEXP_FIELD"),
			),
			"EXPRESSION" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_NODE_EXPRESSION_FIELD"),
			),
			"DATE_CREATE" => array(
				"data_type" => "datetime",
				"title" => Loc::getMessage("XDEV_CP_DATE_CREATE_FIELD"),
			),
			"TIMESTAMP_X" => array(
				"data_type" => "datetime",
				"title" => Loc::getMessage("XDEV_CP_TIMESTAMP_X_FIELD"),
			),
			"ACTIVE" => array(
				"data_type" => "boolean",
				"values" => array("Y", "N"),
				"title" => Loc::getMessage("XDEV_CP_ACTIVE_FIELD"),
			),
		);
	}
	public static function getDropDownListEx($ID, $strProfileName, $strSheduleName, $arFilter = array())
	{
		$arrItems = array();
		$rs = PageTable::getList(array(
			"select" => array(
				"ID", "NAME"
			),
			"order" => array("NAME" => "ASC")
		));
		while($ar = $rs->Fetch())
		{
			$arr = array(
				"NAME" => $ar["NAME"]." [".$ar["ID"]."]",
				"ITEMS" => array()
			);
			$rsIBlocks = static::getList(array(
				"select" => array(
					"ID", "CSS_SELECTOR"
				),
				"filter" => array_merge($arFilter, array(
					"PAGE_ID" => $ar["ID"]
				)),
				"order" => array("ID" => "ASC")
			));
			while($arIBlock = $rsIBlocks->Fetch())
			{
				$arr["ITEMS"][$arIBlock["ID"]] = $arIBlock["CSS_SELECTOR"]." [".$arIBlock["ID"]."]";
			}
			if(!empty($arr["ITEMS"]))
				$arrItems[$ar["ID"]] = $arr;
		}

		return tools::getDropDownList($arrItems, $ID, $strProfileName, $strSheduleName, GetMessage("XDEV_CP_NODE_CHOOSE_PAGE"), GetMessage("XDEV_CP_NODE_CHOOSE_NODE"));
	}
}
?>