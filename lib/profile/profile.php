<?
namespace xdev\cp\profile;

use Bitrix\Main\Localization\Loc;
use Xdev\Cp\Internal\DataManager;

Loc::loadMessages(__FILE__);

class ProfileTable extends DataManager
{
	public static function getFilePath()
	{
		return __FILE__;
	}

	public static function getTableName()
	{
		return "xdev_cp_prof_profile";
	}

	public static function getMap()
	{
		return array(
			"ID" => array(
				"data_type" => "integer",
				"primary" => true,
				"autocomplete" => true,
				"title" => Loc::getMessage("XDEV_CP_ID_FIELD"),
			),
			"ACTIVE" => array(
				"data_type" => "boolean",
				"values" => array("Y", "N"),
				"title" => Loc::getMessage("XDEV_CP_ACTIVE_FIELD"),
			),
			"NAME" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_NAME_FIELD"),
				"expression" => array(
					"(REPLACE(%s, 'www.', ''))",
					"HOSTNAME"
				)
			),
			"HOSTNAME" => array(
				"data_type" => "string",
				"required" => true,
				"title" => Loc::getMessage("XDEV_CP_PROFILE_HOSTNAME_FIELD"),
			),
			"PROTO" => array(
				"data_type" => "enum",
				"required" => true,
				"values" => array("http", "https"),
				"title" => Loc::getMessage("XDEV_CP_PROFILE_PROTO_FIELD"),
			),
			"TIMESTAMP_X" => array(
				"data_type" => "datetime",
				"title" => Loc::getMessage("XDEV_CP_TIMESTAMP_X_FIELD"),
			),
			"DATE_CREATE" => array(
				"data_type" => "datetime",
				"title" => Loc::getMessage("XDEV_CP_DATE_CREATE_FIELD"),
			),
		);
	}
}
?>