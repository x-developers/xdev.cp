<?
namespace Xdev\Cp\Profile;

class nodeObject
{
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function get($name)
    {
        return isset($this->data[$name]) ? $this->data[$name] : null;
    }

    public function getId()
    {
        return $this->get("ID");
    }

    public function getPageId()
    {
        return $this->get("PAGE_ID");
    }

    public function getToPageId()
    {
        return $this->get("TO_PAGE_ID");
    }

    public function getCSSselector()
    {
        return $this->get("CSS_SELECTOR");
    }
}
