<?
namespace Xdev\Cp\Export;

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Entity\Event;

use \Xdev\Cp\Internal\DataManager;

Loc::loadMessages(__FILE__);

class EntityTable extends DataManager
{
 	public static function getFilePath()
	{
		return __FILE__;
	}

	public static function getTableName()
	{
		return "xdev_cp_export_entity";
	}

	public static function getMap()
	{
		return array(
			"ID" => array(
				"data_type" => "integer",
				"primary" => true,
				"autocomplete" => true,
				"title" => Loc::getMessage("XDEV_CP_ID_FIELD"),
			),
			"EXPORT_ID" => array(
				"data_type" => "string",
				"required" => true,
				"title" => Loc::getMessage("XDEV_CP_EXPORT_EXPORT_ID_FIELD"),
			),
			"EXPORT" => array(
				"data_type" => "\\xdev\\cp\\Export\\Export",
				"reference" => array("=this.EXPORT_ID" => "ref.ID")
			),
			"TYPE_ID" => array(
				"data_type" => "enum",
				"values" => array("S", "P", "E"),
				"required" => true,
				"title" => Loc::getMessage("XDEV_CP_EXPORT_TYPE_ID_FIELD"),
			),
			"MANAGER" => array(
				// "data_type" => "Xdev\Cp\Export\Entity\Manager",
				"data_type" => "Xdev\Cp\Import\Entity\Manager",
				"reference" => array(
					"=this.TYPE_ID" => "ref.TYPE_ID",
					"=ref.MODULE_ID" => array("'iblock'")
				),
			),
			"MANAGER_PRIORITY" => array(
				"data_type" => "integer",
				"expression" => array(
					'%s', 'MANAGER.PRIORITY'
				)
			),
			"PARENT_ID" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_EXPORT_PARENT_ID_FIELD"),
			),
			"DEPTH_LEVEL" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_DEPTH_LEVEL_FIELD")
			),
			"RIGHT_MARGIN" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_RIGHT_MARGIN_FIELD")
			),
			"LEFT_MARGIN" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_LEFT_MARGIN_FIELD")
			),
		);
	}

	public static function resort($id)
	{
		$row = static::getRowById($id);
		if($row["EXPORT_ID"])
			static::treeReSort($row["EXPORT_ID"]);
	}

	public static function treeReSort($EXPORT_ID, $ID = 0, $cnt = 0, $depth = 0)
	{
		$EXPORT_ID = intval($EXPORT_ID); $cnt = intval($cnt); $depth = intval($depth);

		$connection = static::getEntity()->getConnection();

		if($ID > 0)
			$connection->query(sprintf("UPDATE `%s` SET LEFT_MARGIN = %s, RIGHT_MARGIN = %s WHERE ID = %s", static::getTableName(), $cnt, $cnt, $ID));

		$cnt++;

		$res = static::getList(array(
			"select" => array("ID"),
			"filter" => array(
				"EXPORT_ID" => $EXPORT_ID,
				"PARENT_ID" => ($ID ? $ID : false),
			),
			"order" => array("ID" => "asc")
		));
		while($arr = $res->Fetch())
			$cnt = static::treeReSort($EXPORT_ID, $arr["ID"], $cnt, $depth + 1);

		if($ID == 0)
			return true;

		$connection->query(sprintf("UPDATE `%s` SET RIGHT_MARGIN = %s, DEPTH_LEVEL = %s WHERE ID = %s", static::getTableName(), $cnt, $depth, $ID));

		return $cnt + 1;
	}

	public static function getExportRow($exportId, $type, $parentId = null)
	{
		$arFilter = array(
			"TYPE_ID" => $type,
			"PARENT_ID" => $parentId,
			"EXPORT_ID" => $exportId
		);

		static $cache = array();

		$cacheId = md5(serialize($arFilter));

		if(!is_set($cache, $cacheId))
			$cache[$cacheId] = static::getRow(array(
				"filter" => $arFilter
			));

		return $cache[$cacheId];
	}

	public static function onAfterAdd(Event $event)
	{
		static::resort($event->getParameter("id"));
	}

	public static function onAfterUpdate(Event $event)
	{
		static::resort($event->getParameter("id"));
	}

	public static function getTypeArray()
	{
		$arDataManagers = array();
		$rs = \Xdev\Cp\Import\Entity\ManagerTable::getList(array(
			"filter" => array(
			)
		));
		while($ar = $rs->Fetch())
			$arDataManagers[$ar["TYPE_ID"]] = $ar["NAME"];

		return $arDataManagers;
	}
}
?>