<?
namespace Xdev\Cp\Export;

use Xdev\Cp\Spider\ResultNodeTable;

class ExportEntity
{
	/**
	 * export id
	 * @var integer
	 */
	protected $exportId;

	/**
	 * entity type
	 * @var string
	 */
	protected $type;

	/**
	 * saved entity id
	 * @var null|integer
	 */
	protected $id = null;

	/**
	 * export entity row
	 * @var array
	 */
	protected $entity;

	/**
	 * parent export entity
	 * @var null|exportEntity
	 */
	protected $parent = null;

	/**
	 * parent entity saved id
	 * @var null|integer
	 */
	protected $parentId = null;

	/**
	 * export fields
	 * @var array[string]
	 */
	protected $data = array();

	/**
	 * root result array
	 * @var array
	 */
	protected $rootData = null;

	protected $lastId = 0;

	protected $errors = array();

	/**
	 * @var exportEntity
	 */
	protected $child = null;

	/**
	 * @param integer           $id     export id
	 * @param string            $entityId entity id
	 * @param exportEntity|null $parent parent export
	 */
	public function __construct($id, $entityId, exportEntity $parent = null)
	{
		$this->exportId = $id;

		$this->type = $type;

		if(null !== $parent)
		{
			$this->parent = $parent;
			$this->parentId = $this->parent->getId();
		}

		$this->entity = EntityTable::getRowById(intval($entityId));
		if(!$this->entity)
			throw new \Exception("Entity not found", intval($entityId));

		$this->type = $this->entity["TYPE_ID"];
	}

	public function getLastId()
	{
		return $this->lastId;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getExportId()
	{
		return $this->exportId;
	}

	public function getTypeId()
	{
		return $this->type;
	}

	public function getParent()
	{
		return $this->parent;
	}

	public function getErrors()
	{
		$result = $this->errors;
		if($this->child)
		{
			foreach($this->child->getErrors() as $error)
				$result[] = $error;
		}

		return $result;
	}

	/**
	 * gets root result node id
	 * @return integer
	 */
	public function getRootNodeId()
	{
		if(null == $this->rootData && ($parentId = $this->getPayload("PARENT_ID")))
		{
			$rs = ResultNodeTable::getList(array(
				"select" => array(
					"ID", "NODE_ID", "PARENT_ID",
					"NAME", "VALUE"
				),
				"filter" => array("ID" => $parentId),
				"limit" => 1
			));
			if($ar = $rs->Fetch())
				$this->rootData = $ar;
		}

		return (null !== $this->rootData ? $this->rootData["ID"] : false);
	}

	public function getEntity($param = null)
	{
		return (null !== $param ? $this->entity[$param] : $this->entity);
	}

	public function getEntityId()
	{
		return $this->getEntity("ID");
	}

	public function getChildEntity()
	{
		return EntityTable::getExportRow(
			$this->getExportId(),
			$this->getTypeId(),
			$this->getEntityId()
		);
	}

	public function setPayload(array $data)
	{
		$this->data = $data;

		$this->parent = null;

		$this->rootData = null;

		return $this;
	}

	public function getPayload($param = null)
	{
		return (null !== $param ? $this->data[$param] : $this->data);
	}

	public function getPayloadId()
	{
		return $this->getPayload("ID");
	}

	/**
	 * gets current result tree list
	 * @return CDBResult
	 */
	public function getResultNodeList($parameters = array())
	{
		if(!isset($parameters["select"]))
			$parameters["select"] = array(
				"ID", "NODE_ID", "PARENT_ID",
				"NAME", "VALUE", "FILE_ID",
				"LEFT_MARGIN", "RIGHT_MARGIN", "DEPTH_LEVEL", "URL" => "QUEUE.URL"
			);
		if(!isset($parameters["order"]))
			$parameters["order"] = array("LEFT_MARGIN" => "ASC");

		return ResultNodeTable::getTreeList($parameters, $this->getRootNode("ID"));
	}

	public function getRootNode($name = null)
	{
		$parentId = ($this->getParent() !== null ? $this->getParent()->getRootNodeId() : $this->getRootNodeId());
		if($parentId)
		{
			static $parentCache = array();
			if(!isset($parentCache[$parentId]))
			{
				$parentCache[$parentId] = ResultNodeTable::getRow(array(
					"select" => array(
						"ID", "NODE_ID", "VALUE", "NAME",
						"LEFT_MARGIN", "RIGHT_MARGIN", "DEPTH_LEVEL"
					),
					"filter" => array("ID" => $parentId),
					"limit" => 1
				));
			}

			return (null !== $name) ? $parentCache[$parentId][$name] : $parentCache[$parentId];
		}

		return false;
	}

	/**
	 * map find fields
	 * @param  integer $sheduleId shedule to search
	 * @return array[string]
	 */
	public function getFields($sheduleId)
	{
		$result = FieldmapTable::mapFields(
			$this->getEntityId(),
			$this->getResultNodeList(array(
				"order" => array("ID" => "ASC"),
				"filter" => array(
					"NODE_ID" => FieldmapTable::getNodesId($this->getEntityId()),
					"SHEDULE_ID" => $sheduleId
				)
			))
		);
		// p($result,1);
		if($root = $this->getRootNode())
		{
			foreach(array("LEFT_MARGIN", "RIGHT_MARGIN", "DEPTH_LEVEL") as $key)
				$result[$key] = $root[$key];
		}

		return $result;
	}

	public function getTotal($sheduleId)
	{
		$rsData = $this->getResultNodeList(array(
			"order" => array("SHEDULE_ID" => "ASC"),
                        "select" => array("CNT"),
			"filter" => array(
				"NODE_ID" => FieldmapTable::getPrimaryFieldNodeId($this->getEntityId()),
				"SHEDULE_ID" => intval($sheduleId)
			),
                        'runtime' => array(
                                'CNT' => array(
                                        'data_type' => 'integer',
                                        'expression' => array(
                                                'COUNT(1)'
                                        )
                                )
                        ),
		));
		if($data = $rsData->Fetch())
			return $data["CNT"];

		return 0;
	}

	/**
	 * execute export
	 * @param  integer $sheduleId shedule to execute
	 * @return exportEntity
	 */
	public function exportTree($sheduleId, $datamanager = null, $arData = array(), $lastId = 0, $timeout = null)
	{
		$rsData = $this->getResultNodeList(array(
			"order" => array(
				"ID" => "ASC"
			),
			"filter" => array(
				">ID" => intval($lastId),
				"NODE_ID" => FieldmapTable::getPrimaryFieldNodeId($this->getEntityId()),
				"SHEDULE_ID" => $sheduleId
			)
		));
		$time = time();
		$arResultFields = array();
		while($data = $rsData->Fetch())
		{
			$arFields = $this->setPayload($data)
					->getFields($sheduleId);

			if($datamanager !== null)
			{
			    if(isset($arFields['DETAIL_TEXT']))
                {
                    $arFields['CODE'] = $data['URL'];
                }
				$res = $datamanager::save(
					array_merge($arData, $arFields),
					$this->parentId
				);
				if($res->isSuccess(true))
				{
					if($res instanceof \Bitrix\Main\Entity\UpdateResult)
						$id = $res->getPrimary();
					else
						$id = $res->getId();

					if(is_array($id))
						$id = $id["ID"];

					$this->id = $id;
					$arFields["ID"] = $id;

					$arResultFields[] = $arFields;
				}
				else
				{
					foreach($res->getErrors() as $error)
						$this->errors[] = $error;
				}
			}
			else
			{
				$arResultFields[] = $arFields;
			}

			if($this->getChildEntity() && 0)
			{
				$ar = $this->getChildEntity();
				$this->child = new exportEntity($ar["EXPORT_ID"], $ar["ID"], $this);

				$arrFields = $this->child->exportTree($sheduleId, $datamanager, $arData);

				foreach($arrFields as $ar)
					$arResultFields[] = $ar;
			}

			$this->lastId = $data["ID"];

			if(null !== $timeout && (time() - $time) >= $timeout)
				break;
		}

		return $arResultFields;
	}
}
?>