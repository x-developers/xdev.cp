<?
namespace Xdev\Cp\Export;

use \Bitrix\Main\Localization\Loc;

use \Xdev\Cp\Internal\DataManager;
use \Xdev\Cp\Spider\ResultNodeTable;

Loc::loadMessages(__FILE__);

class FieldmapTable extends DataManager
{
 	public static function getFilePath()
	{
		return __FILE__;
	}

	public static function getTableName()
	{
		return "xdev_cp_export_fieldmap";
	}

	public static function getMap()
	{
		return array(
			"ID" => array(
				"data_type" => "integer",
				"primary" => true,
				"autocomplete" => true,
				"title" => Loc::getMessage("XDEV_CP_ID_FIELD"),
			),
			"ENTITY_ID" => array(
				"data_type" => "string",
				"required" => true,
				"title" => Loc::getMessage("XDEV_CP_EXPORT_FIELDMAP_ENTITY_ID_FIELD"),
			),
			"ENTITY" => array(
				"data_type" => "\\xdev\\cp\\Export\\Entity",
				"reference" => array("=this.ENTITY_ID" => "ref.ID")
			),
			"NODE_ID" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_EXPORT_FIELDMAP_NODE_ID_FIELD"),
			),
			"NODE" => array(
				"data_type" => "\\xdev\\cp\\Profile\\Node",
				"reference" => array("=this.NODE_ID" => "ref.ID")
			),
			"NAME" => array(
				"data_type" => "string",
				"required" => true,
				"title" => Loc::getMessage("XDEV_CP_NAME_FIELD"),
			),
			"VALUE" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_VALUE_FIELD"),
			),
		);
	}

	public static function getNodesId($entityId)
	{
		$rs = FieldmapTable::getList(array(
			"filter" => array(
				"!NODE_ID" => false,
				"ENTITY_ID" => $entityId
			)
		));
		$result = array();
		while($ar = $rs->Fetch())
			$result[] = $ar["NODE_ID"];

		return $result;
	}

	public static function getPrimaryFieldNodeId($entityId)
	{
		static $cache = array();
		if(!isset($cache[$entityId]))
		{
			$cache[$entityId] = static::getRow(array(
				"filter" => array(
					"NAME" => "NAME",
					"ENTITY_ID" => intval($entityId)
				)
			));
		}
		if(!is_array($cache[$entityId]))
			return false;

		return $cache[$entityId]["NODE_ID"];
	}

	public static function mapFields($entityId, $rsField)
	{
		static $cache = array();
		if(!isset($cache[$entityId]))
		{
			$cache[$entityId] = array();
			$rs = FieldmapTable::getList(array(
				"filter" => array(
					"ENTITY_ID" => $entityId
				)
			));
			while($ar = $rs->Fetch())
				$cache[$entityId][] = $ar;
		}

		$arFields = array();
		while($arField = $rsField->Fetch())
		{
			if(!isset($arFields[$arField["NODE_ID"]]))
				$arFields[$arField["NODE_ID"]] = array();

			$arFields[$arField["NODE_ID"]][] = $arField;
		}

		$result = array();
		foreach($cache[$entityId] as $ar)
		{
			$val = array();
			if(!empty($ar["VALUE"]))
			{
				$val = $ar["VALUE"];
			}
			elseif(is_array($arFields[$ar["NODE_ID"]]) && !empty($arFields[$ar["NODE_ID"]]))
			{
				foreach($arFields[$ar["NODE_ID"]] as $val1)
				{
					if(!empty($val1["FILE_ID"]))
						$val[] = $val1["FILE_ID"];
					else
						$val[] = trim($val1["VALUE"]);
				}
			}
			$result[$ar["NAME"]] = $val;
		}

		return $result;
	}
}
?>