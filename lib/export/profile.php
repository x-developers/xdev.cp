<?
namespace Xdev\Cp\Export;

use \Bitrix\Main\Localization\Loc;

use \Xdev\Cp\Internal\DataManager;

Loc::loadMessages(__FILE__);

class ProfileTable extends DataManager
{
 	public static function getFilePath()
	{
		return __FILE__;
	}

	public static function getTableName()
	{
		return "xdev_cp_export";
	}

	public static function getMap()
	{
		return array(
			"ID" => array(
				"data_type" => "integer",
				"primary" => true,
				"autocomplete" => true,
				"title" => Loc::getMessage("XDEV_CP_ID_FIELD"),
			),
			"NAME" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_NAME_FIELD"),
				"expression" => array(
					'%s', 'PROFILE.NAME'
				)
			),
			"MODULE_ID" => array(
				"data_type" => "string",
				"required" => true,
				"title" => Loc::getMessage("XDEV_CP_EXPORT_MODULE_ID_FIELD"),
			),
			"PROFILE_ID" => array(
				"data_type" => "string",
				"required" => true,
				"title" => Loc::getMessage("XDEV_CP_EXPORT_PROFILE_ID_FIELD"),
			),
			"PROFILE" => array(
				"data_type" => "\\xdev\\cp\\Profile\\Profile",
				"reference" => array("=this.PROFILE_ID" => "ref.ID")
			),
			"IBLOCK_ID" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_EXPORT_IBLOCK_ID_FIELD"),
			),
			"IBLOCK" => array(
				"data_type" => "\Bitrix\Iblock\Iblock",
				"reference" => array("=this.IBLOCK_ID" => "ref.ID"),
			),
			"SHEDULE_ID" => array(
				"data_type" => "string",
				"title" => Loc::getMessage("XDEV_CP_EXPORT_SHEDULE_ID_FIELD"),
			),
		);
	}
}
?>