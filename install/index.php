<?
IncludeModuleLangFile(__FILE__);

class xdev_cp extends CModule
{
	const MODULE_ID = "xdev.cp";
	public $MODULE_ID = "xdev.cp";
	public $MODULE_VERSION;
	public $MODULE_VERSION_DATE;
	public $MODULE_NAME;
	public $MODULE_DESCRIPTION;
	public $MODULE_CSS;
	public $errors = false;

	public function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__)."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];

		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("XDEV_CP_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("XDEV_CP_MODULE_DESC");

		$this->PARTNER_NAME = GetMessage("XDEV_CP_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("XDEV_CP_PARTNER_URI");
	}

	public function InstallDB($arParams = array())
	{
		global $DB, $DBType, $APPLICATION;

		$errors = false;
		if(!array_key_exists("savedata", $arParams) || $arParams["savedata"] != "Y")
			$errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/db/".ToLower($DB->type)."/install.sql");
		if($errors !== false)
			throw new \Exception(implode("<br>", $errors));

		RegisterModule(self::MODULE_ID);

		RegisterModuleDependences("main", "OnBuildGlobalMenu",  self::MODULE_ID, "\\xdev\\cp\\admin\\tools", "onBuildGlobalMenuHandler");

		return true;
	}

	public function UnInstallDB($arParams = array())
	{
		global $DB, $DBType, $APPLICATION;

		$errors = false;
		if(!array_key_exists("savedata", $arParams) || $arParams["savedata"] != "Y")
			$errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/db/".ToLower($DB->type)."/uninstall.sql");
		if($errors !== false)
			throw new \Exception(implode("<br>", $errors));

		UnRegisterModuleDependences("main", "OnBuildGlobalMenu",  self::MODULE_ID, "\\xdev\\cp\\admin\\tools", "onBuildGlobalMenuHandler");

		UnRegisterModule(self::MODULE_ID);

		return true;
	}

	public function InstallEvents()
	{
		return true;
	}

	public function UnInstallEvents()
	{
		return true;
	}

	public function InstallAgents()
	{
		// CAgent::AddAgent("\\xdev\\ga\\http\\baseclient::checkAgent();", self::MODULE_ID, "N", 60);

		return true;
	}

	public function UnInstallAgents()
	{
		// CAgent::RemoveAgent("\\xdev\\ga\\http\\baseclient::checkAgent();", self::MODULE_ID);

		return true;
	}

	public function InstallUserTypes()
	{
		return true;
	}

	public function UnInstallUserTypes($arParams = array())
	{
		return true;
	}

	public function InstallFiles($arParams = array())
	{
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/admin/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/themes/.default/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes/.default");
		// CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/js/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/js", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/tools/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tools", true, true);

		return true;
	}

	public function UnInstallFiles()
	{
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/admin/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/themes/.default/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes/.default");
		// DeleteDirFilesEx("/bitrix/js/".self::MODULE_ID);
		DeleteDirFilesEx("/bitrix/tools/".self::MODULE_ID);

		return true;
	}

	public function DoInstall()
	{
		global $DB;
		$DB->StartTransaction();
		$this->errors = false;

		try
		{
			$this->InstallDB();
			\CModule::IncludeModule(self::MODULE_ID);

			// $this->InstallUserTypes();
			$this->InstallFiles();
			$this->InstallEvents();
			$this->InstallAgents();

			$DB->Commit();
		}
		catch(\Exception $e)
		{
			$DB->Rollback();
			$this->errors = $e->GetMessage();
			$GLOBALS["APPLICATION"]->ThrowException($this->errors);
		}

		return empty($this->errors);
	}

	public function DoUninstall()
	{
		global $DB, $DOCUMENT_ROOT, $APPLICATION, $step;
		$step = IntVal($step);

		$this->errors = false;
		if($step < 2)
		{
			$APPLICATION->IncludeAdminFile(GetMessage("XDEV_CP_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/unstep1.php");
		}
		elseif($step == 2)
		{
			$DB->StartTransaction();
			try
			{
				\CModule::IncludeModule(self::MODULE_ID);

				// $this->UnInstallUserTypes(array("savedata" => $_REQUEST["savedata"]));
				$this->UnInstallDB(array("savedata" => $_REQUEST["savedata"]));
				$this->UnInstallFiles();
				$this->UnInstallEvents();
				$this->UnInstallAgents();

				$DB->Commit();
			}
			catch(\Exception $e)
			{
				$DB->Rollback();
				$this->errors = $e->GetMessage();
				$APPLICATION->ThrowException($this->errors);
			}
			$APPLICATION->IncludeAdminFile(GetMessage("XDEV_CP_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/unstep2.php");
		}

		return empty($this->errors);
	}

        public function GetModuleRightList()
        {
                return array(
                        "reference_id" => array("D", "M", "W"),
                        "reference" => array(
                                "[D] ".GetMessage("XDEV_CP_ACCESS_DENIED"),
                                "[M] ".GetMessage("XDEV_CP_ACCESS_MODERATION"),
                                "[W] ".GetMessage("XDEV_CP_ACCESS_FULL")
                        )
		);
        }
}
?>