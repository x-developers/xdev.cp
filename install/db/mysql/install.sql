CREATE TABLE IF NOT EXISTS `xdev_cp_prof_profile`
(
    `ID`          int(11) unsigned NOT NULL AUTO_INCREMENT,
    `ACTIVE`      char(1) COLLATE utf8_unicode_ci      DEFAULT 'Y',
    `NAME`        varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `HOSTNAME`    varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    `PROTO`       varchar(5) COLLATE utf8_unicode_ci   DEFAULT 'http',
    `TIMESTAMP_X` timestamp        NULL                DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `DATE_CREATE` timestamp        NULL                DEFAULT NULL,
    PRIMARY KEY (`ID`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `xdev_cp_prof_page`
(
    `ID`          int(11) unsigned NOT NULL AUTO_INCREMENT,
    `NAME`        varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `PROFILE_ID`  int(11) unsigned                     DEFAULT NULL,
    `TIMESTAMP_X` timestamp        NULL                DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `DATE_CREATE` timestamp        NULL                DEFAULT NULL,
    `PARENT_ID`   int(11)                              DEFAULT '0',
    PRIMARY KEY (`ID`),
    KEY `PROFILE_ID` (`PROFILE_ID`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `xdev_cp_prof_node`
(
    `ID`           int(11) unsigned NOT NULL AUTO_INCREMENT,
    `PAGE_ID`      int(11) unsigned                     DEFAULT NULL,
    `PARENT_ID`    int(11) unsigned                     DEFAULT NULL,
    `TO_PAGE_ID`   int(11)                              DEFAULT NULL,
    `TYPE`         char(1) COLLATE utf8_unicode_ci      DEFAULT 'S',
    `CSS_SELECTOR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `REGEXP`       varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `TIMESTAMP_X`  timestamp        NULL                DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `DATE_CREATE`  timestamp        NULL                DEFAULT NULL,
    `NAME`         varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ACTIVE`       char(1) COLLATE utf8_unicode_ci      DEFAULT 'Y',
    `EXPRESSION`   varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    PRIMARY KEY (`ID`),
    KEY `PAGE_ID` (`PAGE_ID`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `xdev_cp_queue_shedule`
(
    `ID`            int(11) unsigned NOT NULL AUTO_INCREMENT,
    `ACTIVE`        char(1) COLLATE utf8_unicode_ci       DEFAULT 'Y',
    `CACHE_TYPE`    char(1) COLLATE utf8_unicode_ci       DEFAULT 'E',
    `PROFILE_ID`    int(11) unsigned                      DEFAULT NULL,
    `START_PAGE_ID` int(11)          NOT NULL,
    `STATUS_ID`     char(1) COLLATE utf8_unicode_ci       DEFAULT 'N',
    `NEXT_EXEC`     timestamp        NULL                 DEFAULT NULL,
    `LAST_EXEC`     timestamp        NULL                 DEFAULT NULL,
    `TIMESTAMP_X`   timestamp        NULL                 DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `DATE_CREATE`   timestamp        NULL                 DEFAULT NULL,
    `STATUS_DATE`   timestamp        NULL                 DEFAULT NULL,
    `START_URL`     varchar(2048) COLLATE utf8_unicode_ci DEFAULT NULL,
    `DESCRIPTION`   text COLLATE utf8_unicode_ci,
    `COOKIES`       text COLLATE utf8_unicode_ci,
    PRIMARY KEY (`ID`),
    KEY `PROFILE_ID` (`PROFILE_ID`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `xdev_cp_queue_page`
(
    `ID`              int(11) unsigned NOT NULL AUTO_INCREMENT,
    `SHEDULE_ID`      int(11) unsigned                      DEFAULT NULL,
    `PAGE_ID`         int(11) unsigned                      DEFAULT NULL,
    `URL`             varchar(1024) COLLATE utf8_unicode_ci DEFAULT '',
    `TIMESTAMP_X`     timestamp        NULL                 DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `DATE_CREATE`     timestamp        NULL                 DEFAULT NULL,
    `STATUS_ID`       char(1) COLLATE utf8_unicode_ci       DEFAULT NULL,
    `STATUS_DATE`     timestamp        NULL                 DEFAULT NULL,
    `BARCODE`         varchar(255) COLLATE utf8_unicode_ci  DEFAULT NULL,
    `TITLE`           varchar(255) COLLATE utf8_unicode_ci  DEFAULT NULL,
    `CREATED_NODE_ID` int(11)                               DEFAULT NULL,
    PRIMARY KEY (`ID`),
    KEY `PAGE_ID` (`PAGE_ID`),
    KEY `idx_xdev_cp_queue_page_shedule` (`ID`, `SHEDULE_ID`),
    KEY `xdev_cp_queue_page_s1` (`SHEDULE_ID`, `PAGE_ID`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `xdev_cp_queue_result`
(
    `ID`           int(11) unsigned NOT NULL AUTO_INCREMENT,
    `NAME`         varchar(50) COLLATE utf8_unicode_ci  DEFAULT NULL,
    `ATTRIBUTES`   varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `NODE_ID`      int(11) unsigned                     DEFAULT NULL,
    `VALUE`        longtext COLLATE utf8_unicode_ci,
    `TIMESTAMP_X`  timestamp        NULL                DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `DATE_CREATE`  timestamp        NULL                DEFAULT NULL,
    `PARENT_ID`    int(11) unsigned                     DEFAULT NULL,
    `TMP_ID`       varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    `QUEUE_ID`     int(11) unsigned NOT NULL,
    `FILE_ID`      int(11)                              DEFAULT NULL,
    `LEFT_MARGIN`  int(11)                              DEFAULT NULL,
    `RIGHT_MARGIN` int(11)                              DEFAULT NULL,
    `DEPTH_LEVEL`  int(11)                              DEFAULT NULL,
    PRIMARY KEY (`ID`),
    KEY `QUEUE_ID` (`QUEUE_ID`),
    KEY `idx_xdev_cp_queue_result_parentnode` (`NODE_ID`, `PARENT_ID`),
    KEY `xdev_cp_queue_result_margin` (`NODE_ID`, `QUEUE_ID`, `RIGHT_MARGIN`, `LEFT_MARGIN`),
    KEY `idx_xdev_cp_queue_result_parent_id` (`PARENT_ID`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `xdev_cp_user_agent`
(
    `ID`     int(11) unsigned NOT NULL AUTO_INCREMENT,
    `NAME`   varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ACTIVE` char(1) COLLATE utf8_unicode_ci      DEFAULT 'Y',
    PRIMARY KEY (`ID`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `xdev_cp_export`
(
    `ID`         int(11) unsigned NOT NULL AUTO_INCREMENT,
    `PROFILE_ID` int(11) unsigned DEFAULT NULL,
    `IBLOCK_ID`  int(11) unsigned DEFAULT NULL,
    `SHEDULE_ID` int(11) unsigned DEFAULT NULL,
    `MODULE_ID`  varchar(100)     DEFAULT NULL,
    PRIMARY KEY (`ID`),
    KEY `SHEDULE_ID` (`SHEDULE_ID`),
    KEY `PROFILE_ID` (`PROFILE_ID`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `xdev_cp_export_entity`
(
    `ID`           int(11) unsigned                NOT NULL AUTO_INCREMENT,
    `EXPORT_ID`    int(11) unsigned                NOT NULL,
    `TYPE_ID`      char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
    `PARENT_ID`    int(11) unsigned                         DEFAULT NULL,
    `DEPTH_LEVEL`  int(11)                                  DEFAULT NULL,
    `LEFT_MARGIN`  int(11)                                  DEFAULT NULL,
    `RIGHT_MARGIN` int(11)                                  DEFAULT NULL,
    PRIMARY KEY (`ID`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `xdev_cp_export_fieldmap`
(
    `ID`        int(11) unsigned NOT NULL AUTO_INCREMENT,
    `ENTITY_ID` int(11) unsigned NOT NULL,
    `NODE_ID`   int(11) unsigned                      DEFAULT NULL,
    `NAME`      varchar(50) COLLATE utf8_unicode_ci   DEFAULT NULL,
    `VALUE`     varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
    PRIMARY KEY (`ID`),
    KEY `xdev_cp_export_fieldmap_node` (`ENTITY_ID`, `NODE_ID`),
    KEY `NODE_ID` (`NODE_ID`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `xdev_cp_import_datamanager`
(
    `TYPE_ID`   char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
    `MODULE_ID` varchar(100) COLLATE utf8_unicode_ci     DEFAULT NULL,
    `CLASS`     varchar(255) COLLATE utf8_unicode_ci     DEFAULT NULL,
    `NAME`      varchar(255) COLLATE utf8_unicode_ci     DEFAULT NULL,
    `PRIORITY`  int(11)                         NOT NULL,
    UNIQUE KEY `idx_xdev_cp_export_entity_datamanager` (`MODULE_ID`, `TYPE_ID`)
) ENGINE = InnoDB;

INSERT INTO `xdev_cp_import_datamanager` (`TYPE_ID`, `MODULE_ID`, `CLASS`, `NAME`, `PRIORITY`)
VALUES ('P', 'iblock', '\\Xdev\\Cp\\Import\\Entity\\Iblock\\PropertyTable', 'Свойство', 2),
       ('E', 'iblock', '\\Xdev\\Cp\\Import\\Entity\\Iblock\\ElementTable', 'Товар', 3),
       ('S', 'iblock', '\\Xdev\\Cp\\Import\\Entity\\Iblock\\SectionTable', 'Раздел', 1);