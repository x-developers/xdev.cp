<?

use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;

use Xdev\cp\Import;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
Loc::loadMessages(__FILE__);

Loader::includeModule("xdev.cp") && Loader::includeModule("iblock");

/**
 * List redirect page
 *
 * @var string
 */
$strRedirect_admin = BX_ROOT."/admin/xdev_cp_export_list.php?lang=".LANGUAGE_ID;

/**
 * Success redirect page
 *
 * @var string
 */
$strRedirect = BX_ROOT."/admin/xdev_cp_export_detail.php?lang=".LANGUAGE_ID;

/**
 * Endity datamanager classname
 *
 * @var string
 */
$sDataClassName = "xdev\cp\Export\ProfileTable";

/**
 * Module id
 *
 * @var string
 */

$MODULE_RIGHT = $APPLICATION->GetGroupRight("xdev.cp");
if ($MODULE_RIGHT == "D") {
    $APPLICATION->AuthForm(loc::getMessage("ACCESS_DENIED"));
}
$can_edit = ($MODULE_RIGHT >= "M");

$ID = intval($_REQUEST["ID"]);

$rs = $sDataClassName::getList(
    [
        "select" => [
            "ID",
            "PROFILE_ID",
            "IBLOCK_ID",
            "SHEDULE_ID",
            "MODULE_ID",
        ],
        "filter" => ["ID" => $ID],
    ]
);
$rs = new CAdminResult($rs, "tbl_11");
if (!$rs->ExtractFields("str_")) {
    $ID = 0;
    $str_ACTIVE = "Y";
    $str_PROFILE_ID = isset($_REQUEST["PROFILE_ID"]) ? intval($_REQUEST["PROFILE_ID"]) : 0;
    $str_SHEDULE_ID = isset($_REQUEST["SHEDULE_ID"]) ? intval($_REQUEST["SHEDULE_ID"]) : 0;
} else {
    $arIblock = CIBlock::GetById(intval($str_IBLOCK_ID))->Fetch();
    $strIblockResultList = BX_ROOT
        ."/admin/iblock_list_admin.php?IBLOCK_ID="
        .$arIblock["ID"]
        ."&type="
        .$arIblock["IBLOCK_TYPE_ID"]
        ."&find_section_section=0&lang="
        .LANGUAGE_ID;
}

$APPLICATION->SetTitle(
    $ID > 0 ? loc::getMessage("XDEV_CP_EXPORT_EDIT_TITLE", ["#ID#" => $ID]) : loc::getMessage("XDEV_CP_EXPORT_NEW_TITLE")
);

$aTabs = [];
$aTabs[] = ["DIV" => "edit1", "TAB" => loc::getMessage("XDEV_CP_EXPORT_TAB1"), "TITLE" => loc::getMessage("XDEV_CP_EXPORT_TAB1_TITLE")];

$strError = "";
$arEditFields = [];
foreach ($sDataClassName::getEntity()->getFields() as $field) {
    if (
        $field instanceof Bitrix\Main\Entity\ScalarField
        && !$field->isPrimary()
    ) {
        $arEditFields[$field->getName()] = $field;
    }
}
$tabControl = new CAdminForm(preg_replace("/\\\+/", "_", $sDataClassName)."_edit", $aTabs);

$bFromForm = false;
if (
    $_SERVER["REQUEST_METHOD"] == "POST"
    && (
        $_REQUEST["save"] <> ''
        || $_REQUEST["apply"] <> ''
        || $_REQUEST["Update"] == "Y"
        || $_REQUEST["save_and_add"] <> ''
    )
    && check_bitrix_sessid()
    && $can_edit
) {
    $arFields = [];
    foreach ($arEditFields as $key => $field) {
        if (!in_array($key, ["TIMESTAMP_X", "DATE_CREATE", "STATUS_ID", "STATUS_DATE"])) {
            $value = $_REQUEST[$key];
            if ($field instanceof \Bitrix\Main\Entity\BooleanField) {
                if (count($field->getValues()) == 2) {
                    $value == "Y" ? "Y" : "N";
                }
            } elseif ($field instanceof \Bitrix\Main\Entity\DatetimeField) {
                $value = new \Bitrix\Main\Type\DateTime($value);
            }
            $arFields[$key] = $value;
        }
    }

    if ($ID) {
        $result = $sDataClassName::update($ID, $arFields);
    } else {
        $result = $sDataClassName::add($arFields);
    }

    if ($result->isSuccess()) {
        if (!$ID) {
            $ID = $result->getId();
        }

        if ($_REQUEST["save"] <> '') {
            LocalRedirect($strRedirect_admin);
        } elseif ($_REQUEST["apply"] <> '') {
            LocalRedirect($strRedirect."&ID=".$ID."&".$tabControl->ActiveTabParam());
        } elseif (strlen($save_and_add) > 0) {
            LocalRedirect($strRedirect."&ID=0&".$tabControl->ActiveTabParam());
        }
    } else {
        $arErrors = [];
        foreach ($result->getErrors() as $error) {
            $arErrors[] = $error->getMessage();
        }
        $strError = join("<br>", $arErrors);
        $bFromForm = true;
    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && $_REQUEST["Import"] == "Y") {
    $arAlltypes = ["S", "E", "P"];

    $INTERVAL = $_SESSION["CPE_INTERVAL"] = intval($INTERVAL);

    @set_time_limit(0);

    $start_time = time();
    $arErrors = [];

    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_js.php");

    $NS = &$_SESSION["XDEV_CP_EXPORT_NS"];
    if (!isset($_REQUEST["next"]) || !is_array($NS)) {
        $NS = [
            "STEP" => 1,
            "ID" => intval($_REQUEST["ID"]),
            "INTERVAL" => $INTERVAL,
            "DATA" => [],
        ];
        foreach ($arAlltypes as $val) {
            $NS["DATA"][$val] = [];
        }
    }

    if (!check_bitrix_sessid()) {
        $arErrors[] = loc::getMessage("ACCESS_DENIED");
    } elseif ($NS["ID"] > 0) {
        $import = new Import\Iblock($NS);
        if ($NS["STEP"] == 1) {
            $import->reset();

            $NS["STEP"]++;
        } elseif ($NS["STEP"] == 2) {
            $import->init();
            $NS["STEP"]++;
        } elseif ($NS["STEP"] == 3) {
            $res = $import->import();

            $counter = 0;
            foreach ($res as $key => $val) {
                $counter += count($val);
            }

            if (!$counter) {
                if (!$import->getNext()) {
                    $NS["STEP"]++;
                }
            }
        } elseif ($NS["STEP"] == 4) {
            $NS["STEP"]++;
        }

        if ($NS["STEP"] > 1) {
            $typeId = $import->getTypeId();
        }
    } else {
        $arErrors[] = loc::getMessage("ACCESS_DENIED");
    }
    ?>
    <script>
    CloseWaitWindow();
    </script>
    <?

    foreach ($arErrors as $strError) {
        CAdminMessage::ShowMessage($strError);
    }

    if (count($arErrors) == 0) {
        if ($NS["STEP"] < 5) {
            $progressItems = [];
            $progressTotal = 0;
            $progressValue = 0;

            $progressItems[] = loc::getMessage("XDEV_CP_EXPORT_DROP_TEMPORARY_SUCCESS");

            if ($NS["STEP"] < 2) {
                $progressItems[] = loc::getMessage("XDEV_CP_EXPORT_INIT");
            } elseif ($NS["STEP"] < 3) {
                $progressItems[] = "<b>".loc::getMessage("XDEV_CP_EXPORT_INIT")."</b>";
            } else {
                $progressItems[] = loc::getMessage("XDEV_CP_EXPORT_INIT_SUCCESS");
            }

            if ($NS["STEP"] < 3 || empty($typeId)) {
                foreach ($arAlltypes as $type) {
                    $progressItems[] = loc::getMessage(sprintf("XDEV_CP_EXPORT_%s", $type));
                }
            } else {
                foreach ($arAlltypes as $val) {
                    if ($typeId == $val) {
                        $progressTotal = intval($NS["DATA"][$val]["TOTAL"]);
                        $progressValue = intval($NS["DATA"][$val]["SUCCESS"] + $NS["DATA"][$val]["ERRORS"]);

                        $progressItems[] = "<b>".loc::getMessage(
                                sprintf("XDEV_CP_EXPORT_%s_PROGRESS", $val),
                                ["#PROGRESS_TOTAL#" => $progressTotal, "#PROGRESS_VALUE#" => $progressValue]
                            )."</b>";
                    } else {
                        $progressItems[] = loc::getMessage(sprintf("XDEV_CP_EXPORT_%s", $val));
                    }
                }
            }

            CAdminMessage::ShowMessage(
                [
                    "DETAILS" => "<p>".implode("</p><p>", $progressItems)."</p>#PROGRESS_BAR#",
                    "HTML" => true,
                    "TYPE" => "PROGRESS",
                    "PROGRESS_TOTAL" => $progressTotal,
                    "PROGRESS_VALUE" => $progressValue,
                    "MESSAGE" => loc::getMessage("XDEV_CP_EXPORT_PROGRESS_TITLE"),
                ]
            );

            if ($NS["STEP"] > 0) {
                echo '<script>DoNext({"next": "Y"});</script>';
            }
        } else {
            $progressItems = [];
            foreach ($arAlltypes as $str) {
                if (!empty($NS["DATA"][$str])) {
                    $data = [];
                    foreach ($NS["DATA"][$str] as $key => $val) {
                        $data["#".$key."#"] = $val;
                    }

                    $progressItems[] = loc::getMessage(sprintf("XDEV_CP_EXPORT_%s_STATUS", $str), $data);
                }
            }

            CAdminMessage::ShowMessage(
                [
                    "MESSAGE" => loc::getMessage("XDEV_CP_EXPORT_DONE_TITLE"),
                    "DETAILS" => "<p>".implode("</p><p>", $progressItems)."</p>",
                    "HTML" => true,
                    "TYPE" => "PROGRESS",
                ]
            );

            echo '<script>EndImport();</script>';
        }
    } else {
        echo '<script>EndImport();</script>';
    }

    require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin_js.php");
} else {
    if (isset($_SESSION["CPE_INTERVAL"])) {
        $INTERVAL = intval($_SESSION["CPE_INTERVAL"]);
    } else {
        $INTERVAL = 10;
    }
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$aMenu = [
    [
        "TEXT" => loc::getMessage("XDEV_CP_EXPORT_BACK_TO_ADMIN"),
        "LINK" => $strRedirect_admin,
        "ICON" => "btn_list",
    ],
];
if ($ID > 0) {
    $aMenu[] = ["SEPARATOR" => "Y"];
    $aMenu[] = [
        "TEXT" => loc::getMessage("XDEV_CP_EXPORT_NEW_RECORD"),
        "LINK" => $strRedirect,
        "ICON" => "btn_new",
        "TITLE" => loc::getMessage("XDEV_CP_EXPORT_NEW_RECORD"),
    ];
    $aMenu[] = [
        "TEXT" => loc::getMessage("XDEV_CP_EXPORT_DELETE_RECORD"),
        "LINK" => "javascript:if(confirm('"
            .GetMessageJS("XDEV_CP_EXPORT_DELETE_RECORD_CONFIRM")
            ."')) window.location='"
            .$strRedirect_admin
            ."&action=delete&ID="
            .$ID
            ."&"
            .bitrix_sessid_get()
            ."';",
        "ICON" => "btn_delete",
        "TITLE" => loc::getMessage("XDEV_CP_EXPORT_DELETE_RECORD"),
    ];
}

if ($bFromForm) {
    $DB->InitTableVarsForEdit($sDataClassName::getTableName(), "", "str_");
}

$context = new CAdminContextMenu($aMenu);
$context->Show();

if ($strError <> '') {
    $e = new CAdminException([["text" => $strError]]);
    $message = new CAdminMessage(loc::getMessage("MAIN_ERROR_SAVING"), $e);
    echo $message->Show();
}

$tabControl->BeginEpilogContent();

?>
<?= bitrix_sessid_post() ?>
    <input type="hidden" name="Update" value="Y"/><?

$tabControl->EndEpilogContent();

$tabControl->Begin(
    [
        "FORM_ACTION" => $APPLICATION->GetCurPage()."?ID=".intval($ID)."&lang=".LANG,
    ]
);

$tabControl->BeginNextFormTab();

if ($ID > 0) {
    $tabControl->AddViewField("ID", "ID:", $ID);

    if (is_set($arEditFields, "DATE_CREATE")) {
        $field = $arEditFields["DATE_CREATE"];
        unset($arEditFields["DATE_CREATE"]);

        $tabControl->AddViewField($field->getName(), $field->getTitle().":", ${"str_".$field->getName()});
    }
}

$tabControl->BeginCustomField("MODULE_ID", loc::getMessage("XDEV_CP_EXPORT_MODULE_ID_FIELD").":", true);
?>
    <tr>
        <td width="40%"><? echo $tabControl->GetCustomLabelHTML(); ?></td>
        <td><? echo Xdev\Cp\Import\Entity\ManagerTable::getModuleDropdownList("MODULE_ID", $str_MODULE_ID); ?></td>
    </tr><?
$tabControl->EndCustomField("MODULE_ID");

$tabControl->BeginCustomField("SHEDULE_ID", loc::getMessage("XDEV_CP_EXPORT_SHEDULE_ID_FIELD").":", true);
?>
    <tr>
        <td width="40%"><? echo $tabControl->GetCustomLabelHTML(); ?></td>
        <td><? echo \Xdev\Cp\Queue\SheduleTable::getDropDownListEx($str_SHEDULE_ID, "PROFILE_ID", "SHEDULE_ID"); ?></td>
    </tr><?
$tabControl->EndCustomField("SHEDULE_ID");

if ($str_MODULE_ID == "iblock" && Loader::includeModule("iblock")) {
    $tabControl->BeginCustomField("IBLOCK_ID", loc::getMessage("XDEV_CP_EXPORT_IBLOCK_ID_FIELD").":", true);
    ?>
    <tr>
    <td> <? echo $tabControl->GetCustomLabelHTML(); ?></td>
    <td><? echo GetIBlockDropDownList($str_IBLOCK_ID, "IBLOCK_TYPE_ID", "IBLOCK_ID"); ?></td>
    </tr><?
    $tabControl->EndCustomField("IBLOCK_ID");
}

if ($ID > 0) {
    $tabControl->BeginCustomField("IMPORT_CONT", "");
    ?>
    <tr>
        <td><? echo loc::getMessage("XDEV_CP_EXPORT_INTERVAL"); ?>:</td>
        <td><input type="text" name="INTERVAL" id="INTERVAL" value="<? echo $INTERVAL; ?>" size="10" maxlength="255"></td>
    </tr>
    <tr>
    <td></td>
    <td>
        <div id="tbl_cp_export_result_div"></div><?
        ?>
        <script language="JavaScript" type="text/javascript">
        var running = false;

        function DoNext (NS) {
            var queryString = 'Import=Y'
                + '&ID=<?echo $ID?>'
                + '&lang=<?echo LANG?>'
                + '&<?echo bitrix_sessid_get()?>'
                + '&INTERVAL=' + parseInt(BX('INTERVAL').value);
            if(running) {
                setTimeout(function () {
                    ShowWaitWindow();
                    BX.ajax.post(
                        'xdev_cp_export_detail.php?' + queryString,
                        NS,
                        function (result) {
                            BX('tbl_cp_export_result_div').innerHTML = result;
                        },
                    );
                }, 500);
            }
        }

        function StartImport () {
            running = BX('start_button').disabled = true;
            DoNext();
        }

        function EndImport () {
            running = BX('start_button').disabled = false;
        }
        </script>
        <input type="button" id="start_button" value="<?
        echo loc::getMessage("XDEV_CP_EXPORT_START") ?>" OnClick="StartImport();" class="adm-btn-save">
        <input type="button" id="stop_button" value="<?
        echo loc::getMessage("XDEV_CP_EXPORT_STOP") ?>" OnClick="EndImport();">
    </td>
    </tr><?
    $tabControl->EndCustomField("IMPORT_CONT");
}

$tabControl->Buttons(
    [
        "btnSaveAndAdd" => true,
        "back_url" => $strRedirect_admin,
    ]
);

$tabControl->Show();
$tabControl->ShowWarnings($tabControl->GetName(), $message);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>