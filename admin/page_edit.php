<?

use xdev\cp\admin\tools as AT;
use xdev\cp\Profile\ProfileTable;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/tools/prop_userid.php");
\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
\Bitrix\Main\Loader::includeModule("iblock");

/**
 * List redirect page
 *
 * @var string
 */
$strRedirect_admin = BX_ROOT."/admin/xdev_cp_page_list.php?lang=".LANGUAGE_ID."&PROFILE_ID=".intval($_REQUEST["PROFILE_ID"]);

/**
 * Success redirect page
 *
 * @var string
 */
$strRedirect = BX_ROOT."/admin/xdev_cp_page_edit.php?lang=".LANGUAGE_ID;

/**
 * Endity datamanager classname
 *
 * @var string
 */
$sDataClassName = "xdev\cp\Profile\PageTable";

/**
 * Module id
 *
 * @var string
 */
$module_id = "xdev.cp";

$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);
if ($MODULE_RIGHT < "W") {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}
$can_edit = ($MODULE_RIGHT >= "W");

CModule::IncludeModuleEx($module_id);

$sTableID = preg_replace("/\\\+/", "_", $sDataClassName)."_edit";

$ID = intval($_REQUEST["ID"]);
$copy = (isset($_REQUEST['action']) && (string) $_REQUEST['action'] == 'copy');

$rs = $sDataClassName::getByID($ID);
$rs = new CAdminResult($rs, $sTableID);
if (!$rs->ExtractFields("str_")) {
    $ID = 0;
    $copy = false;
    $str_PROFILE_ID = isset($_REQUEST["PROFILE_ID"]) ? intval($_REQUEST["PROFILE_ID"]) : 0;
}

$APPLICATION->SetTitle($ID > 0 ? GetMessage("XDEV_CP_PAGE_EDIT_TITLE", ["#ID#" => $ID]) : GetMessage("XDEV_CP_PAGE_NEW_TITLE"));

$aTabs = [];
$aTabs[] = ["DIV" => "edit1", "TAB" => GetMessage("XDEV_CP_PAGE_TAB1"), "TITLE" => GetMessage("XDEV_CP_PAGE_TAB1_TITLE")];

$strError = "";
$tabControl = new CAdminForm($sTableID, $aTabs);

$arEditFields = [];
foreach ($sDataClassName::getEntity()->getFields() as $field) {
    if (
        $field instanceof Bitrix\Main\Entity\ScalarField
        && !$field->isPrimary()
    ) {
        $arEditFields[$field->getName()] = $field;
    }
}

$bFromForm = false;
if (
    $_SERVER["REQUEST_METHOD"] == "POST"
    && (
        $_REQUEST["save"] <> ''
        || $_REQUEST["apply"] <> ''
        || $_REQUEST["Update"] == "Y"
        || $_REQUEST["save_and_add"] <> ''
    )
    && check_bitrix_sessid()
) {
    $arFields = [];
    foreach ($arEditFields as $key => $field) {
        if (!in_array($key, ["TIMESTAMP_X", "DATE_CREATE"])) {
            $value = $_REQUEST[$key];
            if ($field instanceof Bitrix\Main\Entity\BooleanField) {
                $value == "Y" ? "Y" : "N";
            } elseif ($field instanceof Bitrix\Main\Entity\DatetimeField) {
                try {
                    $value = new \Bitrix\Main\Type\DateTime($value);
                } catch (\Bitrix\Main\ObjectException $e) {
                }
            }

            $arFields[$key] = $value;
        }
    }

    if (null !== $sUfid) {
        $USER_FIELD_MANAGER->EditFormAddFields($sUfid, $arFields);
    }

    if ($ID) {
        $result = $sDataClassName::update($ID, $arFields);
    } else {
        $result = $sDataClassName::add($arFields);
    }

    if ($result->isSuccess()) {
        if (!$ID) {
            $ID = $result->getId();
        }

        if ($_REQUEST["save"] <> '') {
            LocalRedirect($strRedirect_admin);
        } elseif ($_REQUEST["apply"] <> '') {
            LocalRedirect($strRedirect."&ID=".$ID."&".$tabControl->ActiveTabParam());
        } elseif (strlen($save_and_add) > 0) {
            LocalRedirect($strRedirect."&ID=0&".$tabControl->ActiveTabParam());
        }
    } else {
        $arErrors = [];
        foreach ($result->getErrors() as $error) {
            $arErrors[] = $error->getMessage();
        }
        $strError = join("<br>", $arErrors);
        $bFromForm = true;
    }
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$aMenu = [
    [
        "TEXT" => GetMessage("XDEV_CP_PAGE_BACK_TO_ADMIN"),
        "LINK" => $strRedirect_admin,
        "ICON" => "btn_list",
    ],
];
if ($ID > 0) {
    $aMenu[] = [
        "TEXT" => GetMessage("XDEV_CP_PAGE_NEW_SHEDULE"),
        "LINK" => BX_ROOT."/admin/xdev_cp_shedule_detail.php?ID=0&PAGE_ID=".$ID."&lang=".LANGUAGE_ID,
        "ICON" => "btn_new",
        "TITLE" => GetMessage("XDEV_CP_PAGE_NEW_SHEDULE"),
    ];
    $aMenu[] = [
        "TEXT" => GetMessage("XDEV_CP_PAGE_NEW_RECORD"),
        "LINK" => $strRedirect,
        "ICON" => "btn_new",
        "TITLE" => GetMessage("XDEV_CP_PAGE_NEW_RECORD"),
    ];
    $aMenu[] = [
        "TEXT" => GetMessage("XDEV_CP_PAGE_DELETE_RECORD"),
        "LINK" => "javascript:if(confirm('"
            .GetMessageJS("XDEV_CP_PAGE_DELETE_RECORD_CONFIRM")
            ."')) window.location='"
            .$strRedirect_admin
            ."&action=delete&ID="
            .$ID
            ."&"
            .bitrix_sessid_get()
            ."';",
        "ICON" => "btn_delete",
        "TITLE" => GetMessage("XDEV_CP_PAGE_DELETE_RECORD"),
    ];
}

if ($bFromForm) {
    $DB->InitTableVarsForEdit($sDataClassName::getTableName(), "", "str_");
}

$context = new CAdminContextMenu($aMenu);
$context->Show();

if ($strError <> '') {
    $e = new CAdminException([["text" => $strError]]);
    $message = new CAdminMessage(GetMessage("MAIN_ERROR_SAVING"), $e);
    echo $message->Show();
}

//We have to explicitly call calendar and editor functions because
//first output may be discarded by form settings
$tabControl->BeginPrologContent();
if (method_exists($USER_FIELD_MANAGER, 'showscript')) {
    echo $USER_FIELD_MANAGER->ShowScript();
}

CAdminCalendar::ShowScript();
$tabControl->EndPrologContent();
$tabControl->BeginEpilogContent();
?>
<?= bitrix_sessid_post() ?>
    <input type="hidden" name="Update" value="Y"/>
<?
$tabControl->EndEpilogContent();

$tabControl->Begin(
    [
        "FORM_ACTION" => $APPLICATION->GetCurPage()."?ID=".intval($ID)."&lang=".LANG,
    ]
);

$tabControl->BeginNextFormTab();

if ($ID > 0) {
    $tabControl->AddViewField("ID", "ID:", $ID);

    if (is_set($arEditFields, "TIMESTAMP_X")) {
        $field = $arEditFields["TIMESTAMP_X"];
        unset($arEditFields["TIMESTAMP_X"]);

        $tabControl->AddViewField($field->getName(), $field->getTitle().":", ${"str_".$field->getName()});
    }

    if (is_set($arEditFields, "DATE_CREATE")) {
        $field = $arEditFields["DATE_CREATE"];
        unset($arEditFields["DATE_CREATE"]);

        $tabControl->AddViewField($field->getName(), $field->getTitle().":", ${"str_".$field->getName()});
    }
}

$tabControl->BeginCustomField("PROFILE_ID", GetMessage("XDEV_CP_PAGE_PROFILE_ID_FIELD").":", true);
?>
    <tr>
    <td> <? echo $tabControl->GetCustomLabelHTML(); ?></td>
    <td><?

        if ($can_edit) {
            try {
                $rs = ProfileTable::getList(
                    [
                        "select" => [
                            "ID",
                            "NAME",
                        ],
                        "filter" => [
                            "ACTIVE" => "Y",
                        ],
                    ]
                );
            } catch (\Bitrix\Main\ArgumentException $e) {
            }
            ?>
            <label>
            <select name="PROFILE_ID">
                <option value=""><? echo GetMessage("XDEV_CP_PAGE_CHOOSE_PROFILE_ID"); ?></option>
                <?

                while ($ar = $rs->Fetch()) {
                    ?>
                    <option value="<? echo $ar["ID"]; ?>"<? echo($str_PROFILE_ID == $ar["ID"] ? ' selected="selected"'
                        : ""); ?>><? echo htmlspecialchars($ar["NAME"]); ?> [<? echo $ar["ID"]; ?>]</option><?
                }

                ?></select>
            </label><?
        } elseif ($str_PROFILE_ID > 0) {
            $ar = ProfileTable::getRow(
                [
                    "select" => [
                        "ID",
                        "NAME",
                    ],
                    "filter" => [
                        "ID" => $str_PROFILE_ID,
                    ],
                ]
            );
            if ($MODULE_RIGHT >= "W") {
                echo '<a href="'.BX_ROOT.'/admin/xdev_cp_page_edit.php?ID='.$ar["ID"].'&lang='.LANGUAGE_ID.'">'.htmlspecialchars(
                        $ar["NAME"]
                    ).'</a>';
            } else {
                echo $ar["NAME"];
            }
            ?><input type="hidden" name="PROFILE_ID" value="<? echo htmlspecialchars($ar["ID"]); ?>"><?
        }

        ?></td>
    </tr><?
$tabControl->EndCustomField("PROFILE_ID");

AT::drawFormFieldRow($tabControl, $arEditFields["NAME"], $can_edit);

if ($ID > 0 && !$copy) {
    $pageID = $ID;
    define('B_ADMIN_SUBCOUPONS', 1);
    define('B_ADMIN_SUBCOUPONS_LIST', false);
    $nodesReadOnly = !$can_edit;
    $nodesAjaxPath = '/bitrix/tools/xdev.cp/node_list.php?lang='.LANGUAGE_ID.'&find_page_id='.$pageID;

    $tabControl->BeginCustomField("NODES", $content);
    ?>
    <tr id="tr_NODES">
    <td colspan="2"><?
        require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/xdev.cp/tools/node_list.php');
        ?></td></tr><?
    $tabControl->EndCustomField("NODES");
}

$tabControl->Buttons(
    [
        "btnSaveAndAdd" => true,
        "back_url" => $strRedirect_admin,
    ]
);

$tabControl->Show();
$tabControl->ShowWarnings($tabControl->GetName(), $message);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>