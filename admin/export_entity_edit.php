<?

use \Xdev\Cp\Admin\tools as AT;
use Xdev\Cp\Export;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/tools/prop_userid.php");
\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
\Bitrix\Main\Loader::includeModule("iblock");

$subWindow = defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1;

if ($subWindow) {
    require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/iblock/classes/general/subelement.php');
}

/**
 * Endity datamanager classname
 *
 * @var string
 */
$sDataClassName = "xdev\cp\Export\EntityTable";

/**
 * Module id
 *
 * @var string
 */
$module_id = "xdev.cp";

$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);
if ($MODULE_RIGHT < "W") {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}
$can_edit = ($MODULE_RIGHT >= "W");

CModule::IncludeModuleEx($module_id);

$ID = intval($_REQUEST["ID"]);

$rs = $sDataClassName::getByID($ID);
$rs = new CAdminResult($rs, $sTableID);
if (!$rs->ExtractFields("str_")) {
    $ID = 0;
    $str_PARENT_ID = isset($_REQUEST["PARENT_ID"]) ? intval($_REQUEST["PARENT_ID"]) : "";
    $str_EXPORT_ID = isset($_REQUEST["EXPORT_ID"]) ? intval($_REQUEST["EXPORT_ID"]) : 0;
    $str_TYPE_ID = isset($_REQUEST["TYPE_ID"]) ? $_REQUEST["TYPE_ID"] : "";
}

$str_EXPORT_ID = intval($str_EXPORT_ID);

$arExport = false;
if ($str_EXPORT_ID) {
    $arExport = Export\ProfileTable::getRowById($str_EXPORT_ID);
}

$arTypes = $sDataClassName::getTypeArray();

$APPLICATION->SetTitle(
    $ID > 0 ? GetMessage("XDEV_CP_EXPORT_ENTITY_EDIT_TITLE", ["#ID#" => $ID]) : GetMessage("XDEV_CP_EXPORT_ENTITY_NEW_TITLE")
);

$strRedirect_admin = BX_ROOT."/admin/xdev_cp_export_entity_list.php?lang=".LANGUAGE_ID."&EXPORT_ID=".$arExport["ID"];

$strRedirect = BX_ROOT."/admin/xdev_cp_export_entity_edit.php?lang=".LANGUAGE_ID."&EXPORT_ID=".$arExport["ID"];

$aTabs = [];
$aTabs[] = [
    "DIV" => "entity_edit1",
    "TAB" => GetMessage("XDEV_CP_EXPORT_ENTITY_TAB1"),
    "TITLE" => GetMessage("XDEV_CP_EXPORT_ENTITY_TAB1_TITLE"),
];

$strError = "";
$sTableID = preg_replace("/\\\+/", "_", $sDataClassName)."_sub_edit";
if ($subWindow) {
    $arPostParams = [
        'bxpublic' => 'Y',
        'sessid' => bitrix_sessid(),
    ];
    $listUrl = [
        'LINK' => $APPLICATION->GetCurPageParam(),
        'POST_PARAMS' => $arPostParams,
    ];
    $tabControl = new CAdminSubForm($sTableID, $aTabs, false, true, $listUrl, false);
} else {
    $tabControl = new CAdminForm($sTableID, $aTabs);
    $tabControl->SetShowSettings(false);
}

$arEditFields = [];
foreach ($sDataClassName::getEntity()->getFields() as $field) {
    if (
        $field instanceof Bitrix\Main\Entity\ScalarField
        && !$field->isPrimary()
    ) {
        $arEditFields[$field->getName()] = $field;
    }
}

$bFromForm = false;
if (
    $_SERVER["REQUEST_METHOD"] == "POST"
    && (
        $_REQUEST["save"] <> ''
        || $_REQUEST["apply"] <> ''
        || $_REQUEST["Update"] == "Y"
        || $_REQUEST["save_and_add"] <> ''
    )
    && check_bitrix_sessid()
) {
    $arFields = [];
    foreach ($arEditFields as $key => $field) {
        if (!in_array($key, ["TIMESTAMP_X", "DATE_CREATE"])) {
            $value = $_REQUEST[$key];
            if ($field instanceof \Bitrix\Main\Entity\BooleanField) {
                $value == "Y" ? "Y" : "N";
            } elseif ($field instanceof Bitrix\Main\Entity\DatetimeField) {
                $value = new \Bitrix\Main\Type\DateTime($value);
            }

            $arFields[$key] = $value;
        }
    }

    if (!intval($arFields["PARENT_ID"])) {
        $arFields["PARENT_ID"] = null;
    }

    if ($ID) {
        $result = $sDataClassName::update($ID, $arFields);
    } else {
        $result = $sDataClassName::add($arFields);
    }

    if ($result->isSuccess()) {
        if (!$ID) {
            $ID = $result->getId();
        }

        if ($subWindow) {
            ?>
            <script type="text/javascript">
            top.BX.closeWait();
            top.BX.WindowManager.Get().AllowClose();
            top.BX.WindowManager.Get().Close();
            </script><?
            die();
        } else {
            if ($_REQUEST["save"] <> '') {
                LocalRedirect($strRedirect_admin);
            } elseif ($_REQUEST["apply"] <> '') {
                LocalRedirect($strRedirect."&ID=".$ID."&".$tabControl->ActiveTabParam());
            } elseif (strlen($save_and_add) > 0) {
                LocalRedirect($strRedirect."&ID=0&".$tabControl->ActiveTabParam());
            }
        }
    } else {
        $arErrors = [];
        foreach ($result->getErrors() as $error) {
            $arErrors[] = $error->getMessage();
        }
        $strError = join("<br>", $arErrors);
        $bFromForm = true;
    }
} elseif ($subWindow) {
    if (!empty($_REQUEST['dontsave'])) {
        ?>
        <script type="text/javascript">top.BX.closeWait();
        top.BX.WindowManager.Get().AllowClose();
        top.BX.WindowManager.Get().Close();</script><?
        die();
    }
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$aMenu = [
    [
        "TEXT" => GetMessage("XDEV_CP_EXPORT_ENTITY_BACK_TO_ADMIN"),
        "LINK" => $strRedirect_admin,
        "ICON" => "btn_list",
    ],
];
if (!$subWindow && !$readOnly && $ID > 0) {
    $aMenu[] = [
        "TEXT" => GetMessage("XDEV_CP_EXPORT_ENTITY_NEW_RECORD"),
        "LINK" => $strRedirect,
        "ICON" => "btn_new",
        "TITLE" => GetMessage("XDEV_CP_EXPORT_ENTITY_NEW_RECORD"),
    ];
    $aMenu[] = [
        "TEXT" => GetMessage("XDEV_CP_EXPORT_ENTITY_NEW_CHILD_RECORD"),
        "LINK" => $strRedirect."&PARENT_ID=".$ID,
        "ICON" => "btn_new",
        "TITLE" => GetMessage("XDEV_CP_EXPORT_ENTITY_NEW_CHILD_RECORD"),
    ];
    $aMenu[] = [
        "TEXT" => GetMessage("XDEV_CP_EXPORT_ENTITY_DELETE_RECORD"),
        "LINK" => "javascript:if(confirm('"
            .GetMessageJS("XDEV_CP_EXPORT_ENTITY_DELETE_RECORD_CONFIRM")
            ."')) window.location='"
            .$strRedirect_admin
            ."&action=delete&ID="
            .$ID
            ."&"
            .bitrix_sessid_get()
            ."';",
        "ICON" => "btn_delete",
        "TITLE" => GetMessage("XDEV_CP_EXPORT_ENTITY_DELETE_RECORD"),
    ];
}

if ($bFromForm) {
    $DB->InitTableVarsForEdit($sDataClassName::getTableName(), "", "str_");
}

$context = new CAdminContextMenu($aMenu);

$context->Show();

if ($strError <> '') {
    $e = new CAdminException([["text" => $strError]]);
    $message = new CAdminMessage(GetMessage("MAIN_ERROR_SAVING"), $e);
    echo $message->Show();
}

$tabControl->BeginEpilogContent();

echo GetFilterHiddens("filter_");
?>
<?= bitrix_sessid_post() ?>
    <input type="hidden" name="Update" value="Y"/>
<?
$tabControl->EndEpilogContent();

$tabControl->Begin(
    [
        "FORM_ACTION" => $APPLICATION->GetCurPage()."?ID=".intval($ID)."&lang=".LANG,
    ]
);

$tabControl->BeginNextFormTab();

if ($ID > 0) {
    $tabControl->AddViewField("ID", "ID:", $ID);

    if (is_set($arEditFields, "TIMESTAMP_X")) {
        $field = $arEditFields["TIMESTAMP_X"];
        unset($arEditFields["TIMESTAMP_X"]);

        $tabControl->AddViewField($field->getName(), $field->getTitle().":", ${"str_".$field->getName()});
    }

    if (is_set($arEditFields, "DATE_CREATE")) {
        $field = $arEditFields["DATE_CREATE"];
        unset($arEditFields["DATE_CREATE"]);

        $tabControl->AddViewField($field->getName(), $field->getTitle().":", ${"str_".$field->getName()});
    }
}

$tabControl->BeginCustomField("TYPE_ID", GetMessage("XDEV_CP_EXPORT_TYPE_ID_FIELD").":", true);
?>
    <tr>
        <td> <? echo $tabControl->GetCustomLabelHTML(); ?></td>
        <td>
            <select name="TYPE_ID"><?

                foreach ($arTypes as $key => $val) {
                    ?>
                    <option value="<? echo $key; ?>"<? echo($str_TYPE_ID == $key ? ' selected="selected"'
                        : ""); ?>><? echo htmlspecialchars($val); ?></option><?
                }

                ?></select>

        </td>
    </tr><?
$tabControl->EndCustomField("TYPE_ID");

$tabControl->BeginCustomField("EXPORT_ID", GetMessage("XDEV_CP_EXPORT_EXPORT_ID_FIELD").":", true);
?>
    <tr>
        <td> <? echo $tabControl->GetCustomLabelHTML(); ?></td>
        <td><?

            if ($can_edit) {
                $rs = Export\ProfileTable::getList(
                    [
                        "select" => [
                            "ID",
                            "NAME",
                        ],
                        "filter" => [
                        ],
                        "order" => ["NAME" => "ASC"],
                    ]
                );
                ?>
                <select name="EXPORT_ID"><?

                while ($ar = $rs->Fetch()) {
                    ?>
                    <option value="<? echo $ar["ID"]; ?>"<? echo($str_EXPORT_ID == $ar["ID"] ? ' selected="selected"'
                        : ""); ?>><? echo htmlspecialchars($ar["NAME"]); ?> [<? echo $ar["ID"]; ?>]</option><?
                }

                ?></select><?
            } elseif ($str_EXPORT_ID > 0) {
                $ar = Export\ProfileTable::getRow(
                    [
                        "select" => [
                            "ID",
                            "NAME",
                        ],
                        "filter" => [
                            "ID" => $str_EXPORT_ID,
                        ],
                    ]
                );
                if ($MODULE_RIGHT >= "W") {
                    echo '<a href="'.BX_ROOT.'/admin/xdev_cp_export_detail.php?ID='.$ar["ID"].'&lang='.LANGUAGE_ID.'">'.htmlspecialchars(
                            $ar["NAME"]
                        ).' ['.$ar["ID"].']</a>';
                } else {
                    echo $ar["NAME"];
                }
                ?><input type="hidden" name="EXPORT_ID" value="<? echo htmlspecialchars($ar["ID"]); ?>"><?
            }

            ?></td>
    </tr><?
$tabControl->EndCustomField("EXPORT_ID");

if (is_array($arExport)) {
    $tabControl->BeginCustomField("PARENT_ID", GetMessage("XDEV_CP_EXPORT_PARENT_ID_FIELD").":");
    ?>
    <tr>
    <td> <? echo $tabControl->GetCustomLabelHTML(); ?></td>
    <td><?

        ?><select name="PARENT_ID">
            <option value="">(<? echo GetMessage("MAIN_NO"); ?>)</option><?

            $rs = $sDataClassName::getList(
                [
                    "filter" => [
                        "!ID" => $ID,
                        "EXPORT_ID" => $arExport["ID"],
                    ],
                ]
            );

            while ($ar = $rs->Fetch()) {
                ?>
                <option value="<? echo $ar["ID"]; ?>"<? echo($str_PARENT_ID == $ar["ID"] ? ' selected="selected"'
                    : ""); ?>><? echo htmlspecialchars($arTypes[$ar["TYPE_ID"]]); ?> [<? echo $ar["ID"]; ?>]</option><?
            }

            ?></select>

    </td>
    </tr><?
    $tabControl->EndCustomField("PARENT_ID");
} else {
    AT::drawFormFieldRow($tabControl, $arEditFields["PARENT_ID"], $can_edit);
}

if ($ID > 0 && !$copy) {
    $entityID = $ID;
    define('B_ADMIN_SUBCOUPONS', 1);
    define('B_ADMIN_SUBCOUPONS_LIST', false);
    $nodesReadOnly = !$can_edit;
    $nodesAjaxPath = '/bitrix/tools/xdev.cp/export_field_list.php?lang='.LANGUAGE_ID.'&find_entity_id='.$entityID;

    $tabControl->BeginCustomField("FIELDS", $content);
    ?>
    <tr id="tr_FIELDS">
    <td colspan="2"><?
        require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/xdev.cp/tools/export_field_list.php');
        ?></td></tr><?
    $tabControl->EndCustomField("FIELDS");
}

if ($subWindow) {
    $tabControl->Buttons(false, '');
} else {
    $tabControl->Buttons(
        [
            "btnSaveAndAdd" => true,
            "back_url" => $strRedirect_admin,
        ]
    );
}

?>
    <script type="text/javascript">
    <?
    if ($subWindow)
    {
    ?>top.BX.WindowManager.Get().adjustSizeEx();
    <?
    }
    ?></script><?

$tabControl->Show();
$tabControl->ShowWarnings($tabControl->GetName(), $message);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>