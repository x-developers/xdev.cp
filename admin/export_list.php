<?

use xdev\cp\Profile\ProfileTable;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
CPageOption::GetOptionString("main", "nav_page_in_session", "N");

/**
 * Entity edit page
 *
 * @var string
 */
$strEditPath = BX_ROOT."/admin/xdev_cp_export_detail.php?lang=".LANGUAGE_ID;

/**
 * Endity datamanager classname
 *
 * @var string
 */
$sDataClassName = "xdev\cp\Export\ProfileTable";

/**
 * Module id
 *
 * @var string
 */
$module_id = "xdev.cp";

$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);
if ($MODULE_RIGHT == "D") {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}
$can_edit = ($MODULE_RIGHT >= "W");

CModule::IncludeModuleEx($module_id);

$sTableID = preg_replace("/\\\+/", "_", $sDataClassName)."_list";

$arHeaders = $arFilterFields = $arInitFilter = [];
$sPrimaryKey = "ID";
foreach ($sDataClassName::getEntity()->getFields() as $field) {
    if (
        ($field instanceof Bitrix\Main\Entity\ScalarField || $field instanceof Bitrix\Main\Entity\ExpressionField)
        && in_array(
            $field->getName(), [
            "ID",
            "PROFILE_ID",
            "MODULE_ID",
        ]
        )
    ) {
        $id = $field->getColumnName();
        if ($field->isPrimary()) {
            $sPrimaryKey = $field->getColumnName();
        }

        if (strlen($field->getTitle())) {
            $title = GetMessage(sprintf("XDEV_CP_EXPORT_%s_FIELD_ADMIN", $field->getName()));
            $arHeaders[] = [
                "id" => $field->getName(),
                "content" => strlen($title) ? $title : $field->getTitle(),
                "sort" => $field->getName(),
                "default" => true,
            ];

            if ($field instanceof Bitrix\Main\Entity\DatetimeField) {
                $arInitFilter[$id."_FROM"] = "find_".$id."_from";
                $arInitFilter[$id."_TO"] = "find_".$id."_to";
                $arFilterFields["find_".$id."_from"] = $id;
                $arFilterFields["find_".$id."_to"] = $id;
            } else {
                $arInitFilter[$id] = "find_".$id;
                $arFilterFields["find_".$id] = $id;
            }
        }
    }
}

$rs = ProfileTable::getList(
    [
        "select" => ["ID", "NAME"],
        "order" => ["NAME" => "ASC"],
    ]
);
$arProfiles = [];
while ($ar = $rs->Fetch()) {
    $arProfiles[$ar["ID"]] = $ar["NAME"];
}

$lAdmin = new CAdminList(
    $sTableID,
    new CAdminSorting($sTableID, $sPrimaryKey, "asc")
);
// FILTER
$lAdmin->InitFilter($arInitFilter);

$arFilter = [];
foreach ($sDataClassName::getEntity()->getFields() as $field) {
    if ($field instanceof Bitrix\Main\Entity\ScalarField) {
        $id = $field->getColumnName();
        $fieldName = "find_".$id;

        if ($field instanceof Bitrix\Main\Entity\DatetimeField) {
            if (!empty(${$fieldName."_from"})) {
                $arFilter[">=".$id] = ${$fieldName."_from"};
            }
            if (!empty(${$fieldName."_to"})) {
                $arFilter["<=".$id] = ${$fieldName."_to"};
            }
        } else {
            if (!empty(${$fieldName})) {
                $arFilter[$id] = ${$fieldName};
            }
        }
    }
}

// ACTIONS
if ($lAdmin->EditAction() && $can_edit) {
    foreach ($FIELDS as $ID => $arFields) {
        if (!$lAdmin->IsUpdated($ID)) {
            continue;
        }

        $errors = "";
        $result = $sDataClassName::update($ID, $arFields);
        if (!$result->isSuccess()) {
            foreach ($result->getErrors() as $error) {
                $errors .= $error->getMessage()."<br>";
            }
        }
        if (!empty($errors)) {
            $lAdmin->AddUpdateError($errors, $ID);
        }
    }
}

if ($arID = $lAdmin->GroupAction()) {
    if ($_REQUEST["action_target"] == "selected") {
        $arID = [];
        $rs = $sDataClassName::getList(
            [
                "select" => ["ID"],
                "filter" => $arFilter,
            ]
        );
        while ($ar = $rs->Fetch()) {
            $arID[] = $ar["ID"];
        }
    }

    foreach ($arID as $ID) {
        $ID = IntVal($ID);
        if ($ID <= 0) {
            continue;
        }

        $result = new \Bitrix\Main\Entity\Result();
        switch ($_REQUEST["action"]) {
            case "delete":
                if ($can_edit) {
                    $result = $sDataClassName::delete($ID);
                }
                break;
        }

        if (!$result->isSuccess()) {
            $arErrors = [];
            foreach ($result->getErrors() as $error) {
                $arErrors[] = $error->getMessage();
            }
            $lAdmin->AddGroupError(join("<br>", $arErrors), $ID);
        }
    }
}

$lAdmin->AddHeaders($arHeaders);

$arSelect = [];
foreach ($arHeaders as $val) {
    if (in_array($val["id"], $lAdmin->GetVisibleHeaderColumns())) {
        $arSelect[$val["id"]] = (is_set($val, "sort") ? $val["sort"] : $val["id"]);
    }
}
if (!in_array($sPrimaryKey, $arSelect)) {
    $arSelect[$sPrimaryKey] = $sPrimaryKey;
}

$rsData = $sDataClassName::getList(
    [
        "select" => $arSelect,
        "filter" => $arFilter,
        "order" => [
            $by => strtoupper($order),
        ],
    ]
);

$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();

$lAdmin->NavText($rsData->GetNavPrint($title));

while ($data = $rsData->NavNext(true, "f_")) {
    $arActions = [];
    $row = &$lAdmin->AddRow($f_ID, $data);

    foreach ($sDataClassName::getEntity()->getFields() as $field) {
        if (
            ($field instanceof Bitrix\Main\Entity\ScalarField)
            && in_array($field->getColumnName(), $lAdmin->GetVisibleHeaderColumns())
            && !$field->isPrimary()
        ) {
            switch ($field->getColumnName()) {
                case "PROFILE_ID":
                    $val = $arProfiles[$f_PROFILE_ID];
                    if ($MODULE_RIGHT >= "W") {
                        $row->AddViewField(
                            $field->getColumnName(), '<a href="'
                            .BX_ROOT
                            .'/admin/xdev_cp_profile_edit.php?ID='
                            .$f_PROFILE_ID
                            .'&lang='
                            .LANGUAGE_ID
                            .'">'
                            .htmlspecialchars($val)
                            .'</a>'
                        );
                    } else {
                        $row->AddViewField($field->getColumnName(), $val);
                    }
                    break;

                default:
                    if ($can_edit) {
                        switch ($field->getDataType()) {
                            case "string":
                            case "integer":
                                $row->AddInputField($field->getColumnName());
                                break;

                            case "boolean":
                                $vals = [];
                                foreach ($field->getValues() as $val) {
                                    $vals[$val] = $val;
                                }

                                if (count(array_intersect($vals, ["Y", "N"])) == 2) {
                                    $row->AddCheckField($field->getColumnName());
                                } else {
                                    $row->AddSelectField($field->getColumnName(), $vals);
                                }
                                break;
                        }
                    } else {
                        switch ($field->getDataType()) {
                            case "boolean":
                                $vals = [];
                                foreach ($field->getValues() as $val) {
                                    $vals[$val] = $val;
                                }

                                if (count(array_intersect($vals, ["Y", "N"])) == 2) {
                                    $row->AddViewField(
                                        $field->getColumnName(), GetMessage($data[$field->getColumnName()] == "Y" ? "MAIN_YES" : "MAIN_NO")
                                    );
                                }
                                break;
                        }
                    }
                    break;
            }
        }
    }
    if ($can_edit) {
        $arActions[] = [
            "ICON" => "edit",
            "TEXT" => GetMessage("MAIN_ADMIN_MENU_EDIT"),
            "LINK" => $strEditPath."&ID=".$f_ID,
            "DEFAULT" => true,
        ];

        $arActions[] = [
            "ICON" => "new",
            "TEXT" => GetMessage("XDEV_CP_EXPORT_LIST_NEW_ENTITY"),
            "LINK" => BX_ROOT."/admin/xdev_cp_export_entity_edit.php?lang=".LANGUAGE_ID."&EXPORT_ID=".$f_ID,
        ];

        $arActions[] = ["SEPARATOR" => "Y"];
        $arActions[] = [
            "ICON" => "delete",
            "TEXT" => GetMessage("MAIN_ADMIN_MENU_DELETE"),
            "ACTION" => "if(confirm('".GetMessageJS("XDEV_CP_EXPORT_LIST_DELETE_RECORD_CONFIRM")."')) ".$lAdmin->ActionDoGroup(
                    $f_ID, "delete", "&lang=".LANGUAGE_ID
                ),
        ];
    } else {
        $arActions[] = [
            "ICON" => "view",
            "TEXT" => GetMessage("MAIN_ADMIN_MENU_VIEW"),
            "LINK" => $strEditPath."&ID=".$f_ID,
            "DEFAULT" => true,
        ];
    }

    $row->AddActions($arActions);
}

$lAdmin->AddFooter(
    [
        ["title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value" => $rsData->SelectedRowsCount()],
        ["counter" => true, "title" => GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value" => "0"],
    ]
);

$arParams = [];
$arGroupActions = ["delete" => true];

$lAdmin->AddGroupActionTable($arGroupActions, $arParams);

$aMenu = [];
if ($can_edit) {
    $aMenu[] = [
        "ICON" => "btn_new",
        "TEXT" => GetMessage("XDEV_CP_EXPORT_LIST_ADD_RECORD"),
        "LINK" => $strEditPath,
        "TITLE" => GetMessage("XDEV_CP_EXPORT_LIST_ADD_RECORD_TITLE"),
    ];
}
$lAdmin->AddAdminContextMenu($aMenu);

$lAdmin->CheckListMode();

$APPLICATION->SetTitle(GetMessage("XDEV_CP_EXPORT_TITLE_ADMIN"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

?>
    <form method="GET" action="<? echo $APPLICATION->GetCurPage(); ?>" name="find_form">
    <input type="hidden" name="lang" value="<? echo LANG ?>">
    <input type="hidden" name="filter" value="Y"><?

$arFindFields = [];
foreach ($arHeaders as $arHeader) {
    if (in_array($arHeader["id"], $arFilterFields) && $key = array_search($arHeader["id"], $arFilterFields)) {
        $arFindFields[$key] = $arHeader["content"];
    }
}
$oFilter = new CAdminFilter($sTableID."_filter", $arFindFields);
$oFilter->Begin();

foreach ($sDataClassName::getEntity()->getFields() as $field) {
    if (
        ($field instanceof Bitrix\Main\Entity\ScalarField || $field instanceof Bitrix\Main\Entity\ExpressionField)
        && in_array($field->getColumnName(), $arFilterFields)
    ) {
        $id = $field->getColumnName();
        $fieldName = "find_".$id;

        ?>
        <tr>
        <td><? echo HasMessage($module_id."_FILTER_".$id)
                ? GetMessage($module_id."_FILTER_".$id)
                : $field->getTitle();
            ?>:
        </td>
        <td><?

            $arr = [
                "REFERENCE_ID" => [],
                "REFERENCE" => [],
            ];

            if ($field->getColumnName() == "PROFILE_ID") {
                $rs = ProfileTable::getList(
                    [
                        "select" => ["ID", "NAME"],
                        "order" => ["NAME" => "ASC"],
                    ]
                );
                while ($ar = $rs->Fetch()) {
                    $arr["REFERENCE"][] = $ar["NAME"];
                    $arr["REFERENCE_ID"][] = $ar["ID"];
                }
            } elseif ($field instanceof Bitrix\Main\Entity\BooleanField) {
                if ($field->getColumnName() == "STATUS_ID") {
                    foreach ($sDataClassName::getStatusArray() as $key => $val) {
                        $arr["REFERENCE"][] = $val;
                        $arr["REFERENCE_ID"][] = $key;
                    }
                } elseif (\xdev\cp\admin\tools::isBooleanField($field)) {
                    foreach ($field->getValues() as $val) {
                        $arr["REFERENCE"][] = ($val == "Y" ? GetMessage("MAIN_YES") : ($val == "N" ? GetMessage("MAIN_NO") : $val));
                        $arr["REFERENCE_ID"][] = $val;
                    }
                } else {
                    foreach ($field->getValues() as $val) {
                        $arr["REFERENCE"][] = $val;
                        $arr["REFERENCE_ID"][] = $val;
                    }
                }
            }

            if (!empty($arr["REFERENCE_ID"])) {
                echo SelectBoxFromArray($fieldName, $arr, ${$fieldName}, GetMessage("MAIN_ALL"));
            } elseif ($field instanceof Bitrix\Main\Entity\DatetimeField) {
                echo CalendarPeriod(
                    $fieldName."_from", htmlspecialcharsbx(${$fieldName."_from"}), $fieldName."_to",
                    htmlspecialcharsbx(${$fieldName."_from"}), "find_form", true
                );
            } else {
                ?><input type="text" name="<? echo $fieldName; ?>" value="<? echo htmlspecialcharsbx(${$fieldName}); ?>" /><?
            }

            ?></td>
        </tr><?
    }
}

$oFilter->Buttons(["table_id" => $sTableID, "url" => $APPLICATION->GetCurPage(), "form" => "find_form"]);
$oFilter->End();

?></form><?

if (strlen($errorMessage)) {
    CAdminMessage::ShowMessage(["MESSAGE" => $errorMessage, "TYPE" => "ERROR"]);
}

$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>