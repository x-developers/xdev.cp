<?

use xdev\cp\admin\tools as AT;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/tools/prop_userid.php");
\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
\Bitrix\Main\Loader::includeModule("iblock");

/**
 * List redirect page
 *
 * @var string
 */
$strRedirect_admin = BX_ROOT."/admin/xdev_cp_profile_list.php?lang=".LANGUAGE_ID;

/**
 * Success redirect page
 *
 * @var string
 */
$strRedirect = BX_ROOT."/admin/xdev_cp_profile_edit.php?lang=".LANGUAGE_ID;

/**
 * Endity datamanager classname
 *
 * @var string
 */
$sDataClassName = "xdev\cp\Profile\ProfileTable";

/**
 * Module id
 *
 * @var string
 */
$module_id = "xdev.cp";

$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);
if ($MODULE_RIGHT < "W") {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}
$can_edit = ($MODULE_RIGHT >= "W");

CModule::IncludeModuleEx($module_id);

$sTableID = preg_replace("/\\\+/", "_", $sDataClassName)."_edit";
// $sUfid = $sDataClassName::getUfId();

$ID = intval($_REQUEST["ID"]);

$rs = $sDataClassName::getByID($ID);
$rs = new CAdminResult($rs, $sTableID);
if (!$rs->ExtractFields("str_")) {
    $ID = 0;
    $str_ACTIVE = "Y";
    $str_PROTO = "http";
}

$APPLICATION->SetTitle($ID > 0 ? GetMessage("XDEV_CP_PROFILE_EDIT_TITLE", ["#ID#" => $ID]) : GetMessage("XDEV_CP_PROFILE_NEW_TITLE"));

$aTabs = [];
$aTabs[] = ["DIV" => "edit1", "TAB" => GetMessage("XDEV_CP_PROFILE_TAB1"), "TITLE" => GetMessage("XDEV_CP_PROFILE_TAB1_TITLE")];

$strError = "";
$tabControl = new CAdminForm($sTableID, $aTabs);

$arEditFields = [];
foreach ($sDataClassName::getEntity()->getFields() as $field) {
    if (
        $field instanceof Bitrix\Main\Entity\ScalarField
        && !$field->isPrimary()
    ) {
        $arEditFields[$field->getName()] = $field;
    }
}

$bFromForm = false;
if (
    $_SERVER["REQUEST_METHOD"] == "POST"
    && (
        $_REQUEST["save"] <> ''
        || $_REQUEST["apply"] <> ''
        || $_REQUEST["Update"] == "Y"
        || $_REQUEST["save_and_add"] <> ''
    )
    && check_bitrix_sessid()
) {
    $arFields = [];
    foreach ($arEditFields as $key => $field) {
        if (!in_array($key, ["TIMESTAMP_X", "DATE_CREATE"])) {
            $value = $_REQUEST[$key];
            if ($field instanceof Bitrix\Main\Entity\BooleanField) {
                $value == "Y" ? "Y" : "N";
            } elseif ($field instanceof Bitrix\Main\Entity\DatetimeField) {
                $value = new \Bitrix\Main\Type\DateTime($value);
            }

            $arFields[$key] = $value;
        }
    }

    if (null !== $sUfid) {
        $USER_FIELD_MANAGER->EditFormAddFields($sUfid, $arFields);
    }

    if ($ID) {
        $result = $sDataClassName::update($ID, $arFields);
    } else {
        $result = $sDataClassName::add($arFields);
    }

    if ($result->isSuccess()) {
        if (!$ID) {
            $ID = $result->getId();
        }

        if ($_REQUEST["save"] <> '') {
            LocalRedirect($strRedirect_admin);
        } elseif ($_REQUEST["apply"] <> '') {
            LocalRedirect($strRedirect."&ID=".$ID."&".$tabControl->ActiveTabParam());
        } elseif (strlen($save_and_add) > 0) {
            LocalRedirect($strRedirect."&ID=0&".$tabControl->ActiveTabParam());
        }
    } else {
        $arErrors = [];
        foreach ($result->getErrors() as $error) {
            $arErrors[] = $error->getMessage();
        }
        $strError = join("<br>", $arErrors);
        $bFromForm = true;
    }
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$aMenu = [
    [
        "TEXT" => GetMessage("XDEV_CP_PROFILE_BACK_TO_ADMIN"),
        "LINK" => $strRedirect_admin,
        "ICON" => "btn_list",
    ],
];
if ($ID > 0) {
    $aMenu[] = [
        "TEXT" => GetMessage("XDEV_CP_PROFILE_NEW_PAGE_RECORD"),
        "LINK" => BX_ROOT."/admin/xdev_cp_page_edit.php?lang=".LANGUAGE_ID."&ID=0&PROFILE_ID=".$ID,
        "ICON" => "btn_new",
        "TITLE" => GetMessage("XDEV_CP_PROFILE_NEW_PAGE_RECORD"),
    ];
    $aMenu[] = ["SEPARATOR" => "Y"];
    $aMenu[] = [
        "TEXT" => GetMessage("XDEV_CP_PROFILE_NEW_RECORD"),
        "LINK" => $strRedirect,
        "ICON" => "btn_new",
        "TITLE" => GetMessage("XDEV_CP_PROFILE_NEW_RECORD"),
    ];
    $aMenu[] = [
        "TEXT" => GetMessage("XDEV_CP_PROFILE_DELETE_RECORD"),
        "LINK" => "javascript:if(confirm('"
            .GetMessageJS("XDEV_CP_PROFILE_DELETE_RECORD_CONFIRM")
            ."')) window.location='"
            .$strRedirect_admin
            ."&action=delete&ID="
            .$ID
            ."&"
            .bitrix_sessid_get()
            ."';",
        "ICON" => "btn_delete",
        "TITLE" => GetMessage("XDEV_CP_PROFILE_DELETE_RECORD"),
    ];
}

if ($bFromForm) {
    $DB->InitTableVarsForEdit($sDataClassName::getTableName(), "", "str_");
}

$context = new CAdminContextMenu($aMenu);
$context->Show();

if ($strError <> '') {
    $e = new CAdminException([["text" => $strError]]);
    $message = new CAdminMessage(GetMessage("MAIN_ERROR_SAVING"), $e);
    echo $message->Show();
}

//We have to explicitly call calendar and editor functions because
//first output may be discarded by form settings
$tabControl->BeginPrologContent();
if (method_exists($USER_FIELD_MANAGER, 'showscript')) {
    echo $USER_FIELD_MANAGER->ShowScript();
}

CAdminCalendar::ShowScript();
$tabControl->EndPrologContent();
$tabControl->BeginEpilogContent();
?>
<?= bitrix_sessid_post() ?>
    <input type="hidden" name="Update" value="Y"/>
<?
$tabControl->EndEpilogContent();

$tabControl->Begin(
    [
        "FORM_ACTION" => $APPLICATION->GetCurPage()."?ID=".intval($ID)."&lang=".LANG,
    ]
);

$tabControl->BeginNextFormTab();

if ($ID > 0) {
    $tabControl->AddViewField("ID", "ID:", $ID);

    if (is_set($arEditFields, "TIMESTAMP_X")) {
        $field = $arEditFields["TIMESTAMP_X"];
        unset($arEditFields["TIMESTAMP_X"]);

        $tabControl->AddViewField($field->getName(), $field->getTitle().":", ${"str_".$field->getName()});
    }

    if (is_set($arEditFields, "DATE_CREATE")) {
        $field = $arEditFields["DATE_CREATE"];
        unset($arEditFields["DATE_CREATE"]);

        $tabControl->AddViewField($field->getName(), $field->getTitle().":", ${"str_".$field->getName()});
    }
}

AT::drawFormFieldRow($tabControl, $arEditFields["ACTIVE"], $can_edit);
AT::drawFormFieldRow($tabControl, $arEditFields["HOSTNAME"], $can_edit);
AT::drawFormFieldRow($tabControl, $arEditFields["PROTO"], $can_edit);

$tabControl->Buttons(
    [
        "btnSaveAndAdd" => true,
        "back_url" => $strRedirect_admin,
    ]
);

$tabControl->Show();
$tabControl->ShowWarnings($tabControl->GetName(), $message);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>