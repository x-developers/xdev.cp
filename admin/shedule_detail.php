<?

use xdev\cp\admin\tools as AT;
use xdev\cp\Profile\PageTable;
use xdev\cp\Profile\ProfileTable;
use xdev\cp\Queue\SheduleTable;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/tools/prop_userid.php");
\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);

/**
 * List redirect page
 *
 * @var string
 */
$strRedirect_admin = BX_ROOT."/admin/xdev_cp_shedule_list.php?lang=".LANGUAGE_ID;

/**
 * Success redirect page
 *
 * @var string
 */
$strRedirect = BX_ROOT."/admin/xdev_cp_shedule_detail.php?lang=".LANGUAGE_ID;

/**
 * Module id
 *
 * @var string
 */
$module_id = "xdev.cp";

$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);
if ($MODULE_RIGHT == "D") {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}
$can_edit = ($MODULE_RIGHT >= "M");

CModule::IncludeModuleEx($module_id);

$sTableID = preg_replace("/\\\+/", "_", 'SheduleTable')."_edit";

$ID = intval($_REQUEST["ID"]);

$rs = SheduleTable::getList(
    [
        "select" => [
            "ID",
            "ACTIVE",
            "PROFILE_ID",
            "START_URL",
            "START_PAGE_ID",
            "STATUS_ID",
            "STATUS_DATE",
            "NEXT_EXEC",
            "LAST_EXEC",
            "DATE_CREATE",
            "TIMESTAMP_X",
            "QUEUE_NEW",
            "QUEUE_SUCCESS",
            "QUEUE_FAIL",
            "QUEUE_CNT",
            "DESCRIPTION",
        ],
        "filter" => ["ID" => $ID],
    ]
);
$rs = new CAdminResult($rs, $sTableID);
if (!$rs->ExtractFields("str_")) {
    $ID = 0;
    $str_ACTIVE = "Y";
    $str_START_URL = "/";
    $str_PROFILE_ID = isset($_REQUEST["PROFILE_ID"]) ? intval($_REQUEST["PROFILE_ID"]) : 0;
    $str_START_PAGE_ID = isset($_REQUEST["PAGE_ID"]) ? intval($_REQUEST["PAGE_ID"]) : 0;
}
$strQueueList = BX_ROOT."/admin/xdev_cp_queue_list.php?SHEDULE_ID=".$ID."&lang=".LANGUAGE_ID;

$APPLICATION->SetTitle($ID > 0 ? GetMessage("XDEV_CP_SHEDULE_EDIT_TITLE", ["#ID#" => $ID]) : GetMessage("XDEV_CP_SHEDULE_NEW_TITLE"));

$rs = ProfileTable::getList(
    [
        "select" => [
            "ID",
            "NAME",
        ],
        "filter" => [
            "ACTIVE" => "Y",
        ],
        "order" => [
            "NAME" => "ASC",
        ],
    ]
);
$arProfiles = [];
while ($ar = $rs->Fetch()) {
    $arProfiles[$ar["ID"]] = $ar["NAME"];
}

$aTabs = [];
$aTabs[] = ["DIV" => "edit1", "TAB" => GetMessage("XDEV_CP_SHEDULE_TAB1"), "TITLE" => GetMessage("XDEV_CP_SHEDULE_TAB1_TITLE")];

$strError = "";
$tabControl = new CAdminForm($sTableID, $aTabs);

$arEditFields = [];
foreach (SheduleTable::getEntity()->getFields() as $field) {
    if (
        $field instanceof Bitrix\Main\Entity\ScalarField
        && !$field->isPrimary()
    ) {
        $arEditFields[$field->getName()] = $field;
    }
}

$bFromForm = false;
if (
    $_SERVER["REQUEST_METHOD"] == "POST"
    && (
        $_REQUEST["save"] <> ''
        || $_REQUEST["apply"] <> ''
        || $_REQUEST["Update"] == "Y"
        || $_REQUEST["save_and_add"] <> ''
    )
    && check_bitrix_sessid()
    && $can_edit
) {
    $arFields = [];
    foreach ($arEditFields as $key => $field) {
        if (!in_array($key, ["TIMESTAMP_X", "DATE_CREATE", "STATUS_ID", "STATUS_DATE"])) {
            $value = $_REQUEST[$key];
            if ($field instanceof \Bitrix\Main\Entity\BooleanField) {
                if (count($field->getValues()) == 2) {
                    $value == "Y" ? "Y" : "N";
                }
            } elseif ($field instanceof \Bitrix\Main\Entity\DatetimeField) {
                $value = new \Bitrix\Main\Type\DateTime($value);
            }
            $arFields[$key] = $value;
        }
    }

    if ($ID) {
        $result = SheduleTable::update($ID, $arFields);
    } else {
        $result = SheduleTable::add($arFields);
    }

    if ($result->isSuccess()) {
        if (!$ID) {
            $ID = $result->getId();
        }

        if ($_REQUEST["save"] <> '') {
            LocalRedirect($strRedirect_admin);
        } elseif ($_REQUEST["apply"] <> '') {
            LocalRedirect($strRedirect."&ID=".$ID."&".$tabControl->ActiveTabParam());
        } elseif (strlen($save_and_add) > 0) {
            LocalRedirect($strRedirect."&ID=0&".$tabControl->ActiveTabParam());
        }
    } else {
        $arErrors = [];
        foreach ($result->getErrors() as $error) {
            $arErrors[] = $error->getMessage();
        }
        $strError = join("<br>", $arErrors);
        $bFromForm = true;
    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && $_REQUEST["Import"] == "Y") {
    $INTERVAL = $_SESSION["CPS_INTERVAL"] = intval($INTERVAL);

    @set_time_limit(0);

    $start_time = time();

    $arErrors = [];
    $arMessages = [];

    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_js.php");

    $NS = &$_SESSION["XDEV_CP_SPIDER"];
    if (!isset($_REQUEST["next"]) || !is_array($NS)) {
        $NS = [
            "STEP" => 1,
            "ID" => $_REQUEST["ID"],
            "DATA" => [],
            "INDEX" => -1,
        ];
    }

    if (!check_bitrix_sessid()) {
        $arErrors[] = GetMessage("ACCESS_DENIED");
    } elseif ($NS["ID"] > 0) {
        $result = new \Xdev\Cp\Internal\ListResult();
        if ($NS["STEP"] < 1) {
            $NS["STEP"]++;
        } elseif ($NS["STEP"] < 2) {
            SheduleTable::reset($NS["ID"]);
            $NS["STEP"]++;
        } elseif ($NS["STEP"] < 3) {
            $result = SheduleTable::start($NS["ID"]);
            if ($result->isSuccess()) {
                $NS["STEP"]++;
            }
        } elseif ($NS["STEP"] < 4) {
            $result = SheduleTable::execute($NS["ID"], $INTERVAL);
            if ($result->isSuccess()) {
                $counter = 0;
                foreach ($result->getData() as $key => $val) {
                    $counter += $val;

                    if (!isset($NS["DATA"][$key])) {
                        $NS["DATA"][$key] = 0;
                    }
                    $NS["DATA"][$key] += $val;
                }
                if (!$counter) {
                    $NS["STEP"]++;
                }
            }
        } elseif ($NS["STEP"] < 5) {
            if (!is_array($NS["INDEX"])) {
                SheduleTable::reindex($NS["ID"]);
            }

            $NS["INDEX"] = SheduleTable::checkIndex($NS["ID"]);

            if ($NS["INDEX"]["SUCCESS"] >= $NS["INDEX"]["TOTAL"]) {
                $NS["STEP"]++;
            }
        } elseif ($NS["STEP"] < 6) {
            $NS["STEP"]++;
        }

        foreach ($result->getErrors() as $error) {
            $arErrors[] = $error->getMessage();
        }
    } else {
        $arErrors[] = GetMessage("ACCESS_DENIED");
    }
    ?>
    <script>
    CloseWaitWindow();
    </script>
    <?

    foreach ($arErrors as $strError) {
        CAdminMessage::ShowMessage($strError);
    }
    foreach ($arMessages as $strMessage) {
        CAdminMessage::ShowMessage(["MESSAGE" => $strMessage, "TYPE" => "OK"]);
    }

    if (count($arErrors) == 0) {
        if ($NS["STEP"] < 6) {
            $progressItems = [];
            $progressTotal = 0;
            $progressValue = 0;

            if ($NS["STEP"] < 1) {
                $progressItems[] = GetMessage("XDEV_CP_SHEDULE_STEP_RESET");
            } elseif ($NS["STEP"] < 2) {
                $progressItems[] = "<b>".GetMessage("XDEV_CP_SHEDULE_STEP_RESET")."</b>";
            } else {
                $progressItems[] = GetMessage("XDEV_CP_SHEDULE_STEP_RESET_SUCCESS");
            }

            if ($NS["STEP"] < 2) {
                $progressItems[] = GetMessage("XDEV_CP_SHEDULE_STEP_START");
            } elseif ($NS["STEP"] < 3) {
                $progressItems[] = "<b>".GetMessage("XDEV_CP_SHEDULE_STEP_START")."</b>";
            } else {
                $progressItems[] = GetMessage("XDEV_CP_SHEDULE_STEP_START_SUCCESS");
            }

            if ($NS["STEP"] < 3) {
                $progressItems[] = GetMessage("XDEV_CP_SHEDULE_STEP_EXECUTE");
            } elseif ($NS["STEP"] < 4) {
                $progressTotal = array_sum($NS["DATA"]);

                $progressItems[] = "<b>".GetMessage("XDEV_CP_SHEDULE_STEP_EXECUTE_PROGRESS", ["#PROGRESS_VALUE#" => $progressTotal])."</b>";
            } else {
                $progressItems[] = GetMessage("XDEV_CP_SHEDULE_STEP_EXECUTE_SUCCESS");
            }

            if ($NS["STEP"] < 4) {
                $progressItems[] = GetMessage("XDEV_CP_SHEDULE_STEP_REINDEX");
            } elseif ($NS["STEP"] < 5) {
                $progressTotal = intval($NS["INDEX"]["TOTAL"]);
                $progressValue = intval($NS["INDEX"]["SUCCESS"]);

                $progressItems[] = "<b>".GetMessage(
                        "XDEV_CP_SHEDULE_STEP_REINDEX_PROGRESS", [
                            "#PROGRESS_VALUE#" => $progressValue,
                            "#PROGRESS_TOTAL#" => $progressTotal,
                        ]
                    )."</b>#PROGRESS_BAR#";
            } else {
                $progressItems[] = GetMessage("XDEV_CP_SHEDULE_STEP_REINDEX_SUCCESS");
            }

            CAdminMessage::ShowMessage(
                [
                    "HTML" => true,
                    "TYPE" => "PROGRESS",
                    "MESSAGE" => GetMessage("XDEV_CP_SHEDULE_PROGRESS_TITLE"),
                    "DETAILS" => "<p>".implode("</p><p>", $progressItems)."</p>",
                    "PROGRESS_TOTAL" => $progressTotal,
                    "PROGRESS_VALUE" => $progressValue,
                ]
            );

            if ($NS["STEP"] > 0) {
                echo '<script>DoNext({"next": "Y"});</script>';
            }
        } else {
            $progressItems = [
                GetMessage("XDEV_CP_SHEDULE_TOTAL_PAGES", ["#COUNT#" => intval(array_sum($NS["DATA"]))]),
                GetMessage("XDEV_CP_SHEDULE_TOTAL_SUCCESS", ["#COUNT#" => intval($NS["DATA"]["SUCCESS"])]),
                GetMessage("XDEV_CP_SHEDULE_TOTAL_ERRORS", ["#COUNT#" => intval($NS["DATA"]["ERRORS"])]),
            ];

            CAdminMessage::ShowMessage(
                [
                    "HTML" => true,
                    "TYPE" => "PROGRESS",
                    "MESSAGE" => GetMessage("XDEV_CP_SHEDULE_SUCCESS_TITLE"),
                    "DETAILS" => "<p>".implode("</p><p>", $progressItems)."</p>",
                    "BUTTONS" => [
                        [
                            "VALUE" => GetMessage("XDEV_CP_SHEDULE_DETAIL_URL"),
                            "ONCLICK" => "window.location = '".$strQueueList."';",
                        ],
                    ],
                ]
            );

            echo '<script>EndImport();</script>';
        }
    } else {
        echo '<script>EndImport();</script>';
    }

    require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin_js.php");
} else {
    if (isset($_SESSION["CPS_INTERVAL"])) {
        $INTERVAL = intval($_SESSION["CPS_INTERVAL"]);
    } else {
        $INTERVAL = 10;
    }
}

$_SESSION["CPS_INTERVAL"] = intval($INTERVAL);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$aMenu = [
    [
        "TEXT" => GetMessage("XDEV_CP_SHEDULE_BACK_TO_ADMIN"),
        "LINK" => $strRedirect_admin,
        "ICON" => "btn_list",
    ],
];
if ($ID > 0) {
    if (SheduleTable::canExport($ID)) {
        $aMenu[] = [
            "TEXT" => GetMessage("XDEV_CP_SHEDULE_EXPORT_ADD"),
            "LINK" => BX_ROOT."/admin/xdev_cp_export_detail.php?lang=".LANGUAGE_ID."&SHEDULE_ID=".$ID."&PROFILE_ID=".$str_PROFILE_ID,
            "ICON" => "btn_new",
            "TITLE" => GetMessage("XDEV_CP_SHEDULE_EXPORT_ADD"),
        ];
        $aMenu[] = ["SEPARATOR" => true];
    }
    $aMenu[] = [
        "TEXT" => GetMessage("XDEV_CP_SHEDULE_NEW_RECORD"),
        "LINK" => $strRedirect,
        "ICON" => "btn_new",
        "TITLE" => GetMessage("XDEV_CP_SHEDULE_NEW_RECORD"),
    ];
    $aMenu[] = [
        "TEXT" => GetMessage("XDEV_CP_SHEDULE_DELETE_RECORD"),
        "LINK" => "javascript:if(confirm('"
            .GetMessageJS("XDEV_CP_SHEDULE_DELETE_RECORD_CONFIRM")
            ."')) window.location='"
            .$strRedirect_admin
            ."&action=delete&ID="
            .$ID
            ."&"
            .bitrix_sessid_get()
            ."';",
        "ICON" => "btn_delete",
        "TITLE" => GetMessage("XDEV_CP_SHEDULE_DELETE_RECORD"),
    ];
}

if ($bFromForm) {
    $DB->InitTableVarsForEdit(SheduleTable::getTableName(), "", "str_");
}

$context = new CAdminContextMenu($aMenu);
$context->Show();

if ($strError <> '') {
    $e = new CAdminException([["text" => $strError]]);
    $message = new CAdminMessage(GetMessage("MAIN_ERROR_SAVING"), $e);
    echo $message->Show();
}

$tabControl->BeginEpilogContent();
?>
<?= bitrix_sessid_post() ?>
    <input type="hidden" name="Update" value="Y"/><?

$tabControl->EndEpilogContent();

$tabControl->Begin(
    [
        "FORM_ACTION" => $APPLICATION->GetCurPage()."?ID=".intval($ID)."&lang=".LANG,
    ]
);

$tabControl->BeginNextFormTab();

if ($ID > 0) {
    $tabControl->AddViewField("ID", "ID:", $ID);

    if (is_set($arEditFields, "DATE_CREATE")) {
        $field = $arEditFields["DATE_CREATE"];
        unset($arEditFields["DATE_CREATE"]);

        $tabControl->AddViewField($field->getName(), $field->getTitle().":", ${"str_".$field->getName()});
    }
    if (is_set($arEditFields, "STATUS_DATE")) {
        $field = $arEditFields["STATUS_DATE"];
        unset($arEditFields["STATUS_DATE"]);

        $tabControl->AddViewField($field->getName(), $field->getTitle().":", ${"str_".$field->getName()});

        $field = $arEditFields["STATUS_ID"];
        unset($arEditFields["STATUS_ID"]);

        $ar = SheduleTable::getStatusArray();
        $tabControl->AddViewField($field->getName(), $field->getTitle().":", $ar[$str_STATUS_ID].' ['.$str_STATUS_ID.']');
    }

    $tabControl->AddViewField(
        "QUEUE_CNT", GetMessage("XDEV_CP_QUEUE_SHEDULE_QUEUE_CNT_FIELD").":", "<a href=\"".$strQueueList."\">".$str_QUEUE_CNT."</a>"
    );
    $tabControl->AddViewField("QUEUE_SUCCESS", GetMessage("XDEV_CP_QUEUE_SHEDULE_QUEUE_SUCCESS_FIELD").":", $str_QUEUE_SUCCESS);
    $tabControl->AddViewField("QUEUE_NEW", GetMessage("XDEV_CP_QUEUE_SHEDULE_QUEUE_NEW_FIELD").":", $str_QUEUE_NEW);
    $tabControl->AddViewField("QUEUE_FAIL", GetMessage("XDEV_CP_QUEUE_SHEDULE_QUEUE_FAIL_FIELD").":", $str_QUEUE_FAIL);
}

AT::drawFormFieldRow($tabControl, $arEditFields["ACTIVE"], true);

$tabControl->BeginCustomField("START_PAGE_ID", GetMessage("XDEV_CP_SHEDULE_START_PAGE_ID_FIELD").":", true);
?>
    <tr>
        <td> <? echo $tabControl->GetCustomLabelHTML(); ?></td>
        <td><?

            if ($can_edit) {
                echo PageTable::getDropDownListEx($str_START_PAGE_ID, "PROFILE_ID", "START_PAGE_ID");
            } elseif ($str_START_PAGE_ID > 0) {
                $ar = PageTable::getRow(
                    [
                        "select" => [
                            "ID",
                            "NAME",
                        ],
                        "filter" => [
                            "ID" => $str_START_PAGE_ID,
                        ],
                    ]
                );
                if ($MODULE_RIGHT >= "W") {
                    echo '<a href="'.BX_ROOT.'/admin/xdev_cp_page_edit.php?ID='.$ar["ID"].'&lang='.LANGUAGE_ID.'">'.htmlspecialchars(
                            $ar["NAME"]
                        ).'</a>';
                } else {
                    echo $ar["NAME"];
                }
                ?><input type="hidden" name="START_PAGE_ID" value="<? echo htmlspecialchars($ar["ID"]); ?>"><?
                ?><input type="hidden" name="PROFILE_ID" value="<? echo htmlspecialchars($str_PROFILE_ID); ?>"><?
            }

            ?></td>
    </tr><?
$tabControl->EndCustomField("START_PAGE_ID");

$tabControl->BeginCustomField("START_URL", GetMessage("XDEV_CP_QUEUE_SHEDULE_START_URL_FIELD").":", true);
?>
    <tr>
        <td> <? echo $tabControl->GetCustomLabelHTML(); ?></td>
        <td><textarea rows="3" cols="100" name="START_URL"><? echo htmlspecialchars($str_START_URL); ?></textarea></td>

        <?
        $tabControl->EndCustomField("START_URL");

        $tabControl->BeginCustomField("DESCRIPTION", GetMessage("XDEV_CP_QUEUE_SHEDULE_DESCRIPTION_FIELD").":");
        ?>
    <tr>
        <td> <? echo $tabControl->GetCustomLabelHTML(); ?></td>
        <td><textarea rows="3" cols="100" name="DESCRIPTION"><? echo htmlspecialchars($str_DESCRIPTION); ?></textarea></td>

        <?
        $tabControl->EndCustomField("DESCRIPTION");

        /*
        $tabControl->BeginCustomField("COOKIES", GetMessage("XDEV_CP_QUEUE_SHEDULE_COOKIES_FIELD") . ":");
        ?><tr>
            <td> <? echo $tabControl->GetCustomLabelHTML(); ?></td>
            <td><textarea rows="5" cols="100" name="COOKIES"><? echo htmlspecialchars($str_COOKIES); ?></textarea></td>

        <?
        $tabControl->EndCustomField("COOKIES");
        */

        // AT::drawFormFieldRow($tabControl, $arEditFields["NEXT_EXEC"], $can_edit);

        if ($ID > 0)
        {
        $tabControl->BeginCustomField("IMPORT_CONT", $content);
        ?>
    <tr>
        <td><? echo GetMessage("XDEV_CP_QUEUE_SHEDULE_INTERVAL"); ?>:</td>
        <td><input type="text" name="INTERVAL" id="INTERVAL" value="<? echo $INTERVAL; ?>" size="10" maxlength="255"></td>
    </tr>
<tr>
    <td></td>
    <td>
        <div id="tbl_cp_import_result_div"></div>
        <script language="JavaScript" type="text/javascript">
        var running = false;
        var oldNS = '';

        function DoNext (NS) {
            var queryString = 'Import=Y'
                + '&ID=<?echo $ID?>'
                + '&lang=<?echo LANG?>'
                + '&<?echo bitrix_sessid_get()?>'
                + '&INTERVAL=' + parseInt(BX('INTERVAL').value);
            if(running) {
                setTimeout(function () {
                    ShowWaitWindow();
                    BX.ajax.post(
                        'xdev_cp_shedule_detail.php?' + queryString,
                        NS,
                        function (result) {
                            BX('tbl_cp_import_result_div').innerHTML = result;
                        },
                    );
                }, 500);
            }
        }

        function StartImport () {
            running = BX('start_button').disabled = true;
            DoNext();
        }

        function EndImport () {
            running = BX('start_button').disabled = false;
        }
        </script>
        <input type="button" id="start_button" value="<? echo GetMessage("XDEV_CP_QUEUE_SHEDULE_START_BUTTON"); ?>" OnClick="StartImport();"
               class="adm-btn-save">
        <input type="button" id="stop_button" value="<? echo GetMessage("XDEV_CP_QUEUE_SHEDULE_STOP_BUTTON"); ?>" OnClick="EndImport();">
    </td>
</tr><?
    $tabControl->EndCustomField("IMPORT_CONT");
}

$tabControl->Buttons(
    [
        "btnSaveAndAdd" => true,
        "back_url" => $strRedirect_admin,
    ]
);

$tabControl->Show();
$tabControl->ShowWarnings($tabControl->GetName(), $message);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>