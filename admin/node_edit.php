<?
use \Xdev\Cp\Admin\tools as AT;
use \Xdev\Cp\Profile\PageTable;
use \Xdev\Cp\Spider\NodeManager;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/tools/prop_userid.php");
\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
\Bitrix\Main\Loader::includeModule("iblock");

$subWindow = defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1;

if($subWindow)
{
	require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/iblock/classes/general/subelement.php');
}

/**
 * Endity datamanager classname
 * @var string
 */
$sDataClassName = "xdev\cp\Profile\NodeTable";

/**
 * Module id
 * @var string
 */
$module_id = "xdev.cp";

$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);
if($MODULE_RIGHT < "W") $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
$can_edit = ($MODULE_RIGHT >= "W");

CModule::IncludeModuleEx($module_id);

$sTableID = preg_replace("/\\\+/", "_", $sDataClassName) . "_sub_edit";

$ID = intval($_REQUEST["ID"]);

$rs = $sDataClassName::getByID($ID);
$rs = new CAdminResult($rs, $sTableID);
if(!$rs->ExtractFields("str_"))
{
	$ID = 0;
	$str_PARENT_ID = isset($_REQUEST["PARENT_ID"]) ? intval($_REQUEST["PARENT_ID"]) : "";
	$str_PAGE_ID = isset($_REQUEST["PAGE_ID"]) ? intval($_REQUEST["PAGE_ID"]) : 0;
	$str_TYPE = $_REQUEST["TYPE"];
	$str_ACTIVE = "Y";
}

$APPLICATION->SetTitle($ID > 0 ? GetMessage("XDEV_CP_NODE_EDIT_TITLE", array("#ID#" => $ID)) : GetMessage("XDEV_CP_NODE_NEW_TITLE"));

$str_PAGE_ID = intval($str_PAGE_ID);
$arPage = PageTable::getRowById($str_PAGE_ID);

$arRegisteredNode = Xdev\Cp\Spider\NodeManager::getRegisteredNodeById($str_TYPE);

if(!$arRegisteredNode || !is_array($arPage))
{
	require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');

	$e = new CAdminException(array(array("text" => GetMessage("ACCESS_DENIED"))));
	$message = new CAdminMessage("", $e);
	echo $message->Show();

	require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');
	die();
}

$strRedirect_admin = BX_ROOT."/admin/xdev_cp_node_list.php?lang=" . LANGUAGE_ID . "&PAGE_ID=" . $arPage["ID"];

$strRedirect = BX_ROOT."/admin/xdev_cp_node_edit.php?lang=" . LANGUAGE_ID . "&PAGE_ID=" . $arPage["ID"];

$aTabs = array();
$aTabs[] = array("DIV" => "node_edit1", "TAB" => GetMessage("XDEV_CP_NODE_TAB1"), "TITLE" => GetMessage("XDEV_CP_NODE_TAB1_TITLE"));

$strError = "";
if($subWindow)
{
	$arPostParams = array(
		'bxpublic' => 'Y',
		'PAGE_ID' => $str_PAGE_ID,
		'TYPE' => $str_TYPE,
		'sessid' => bitrix_sessid()
	);
	$listUrl = array(
		'LINK' => $APPLICATION->GetCurPageParam(),
		'POST_PARAMS' => $arPostParams,
	);
	$tabControl = new CAdminSubForm($sTableID, $aTabs, false, true, $listUrl, false);
	// $control = new CAdminSubForm($couponFormID, $tabList, false, true, $listUrl, false);
}
else
{
	$tabControl = new CAdminForm($sTableID, $aTabs);
	$tabControl->SetShowSettings(false);
}

$arEditFields = array();
foreach($sDataClassName::getEntity()->getFields() as $field)
{
	if(
		$field instanceof Bitrix\Main\Entity\ScalarField
		&& !$field->isPrimary()
	)
	{
		$arEditFields[$field->getName()] = $field;
	}
}

$bFromForm = false;
if(
	$_SERVER["REQUEST_METHOD"]=="POST"
	&& (
		$_REQUEST["save"]<>''
		|| $_REQUEST["apply"]<>''
		|| $_REQUEST["Update"]=="Y"
		|| $_REQUEST["save_and_add"]<>''
	)
	&& check_bitrix_sessid()
)
{
	$arFields = array();
	foreach($arEditFields as $key => $field)
	{
		if(!in_array($key, array("TIMESTAMP_X", "DATE_CREATE")))
		{
			$value = $_REQUEST[$key];
			if($field instanceof Bitrix\Main\Entity\BooleanField)
				$value ==  "Y" ? "Y" : "N";
			elseif($field instanceof Bitrix\Main\Entity\DatetimeField)
				$value = new \Bitrix\Main\Type\DateTime($value);

			$arFields[$key] = $value;
		}
	}

	if(!intval($arFields["PARENT_ID"]))
		$arFields["PARENT_ID"] = null;

	if($ID)
		$result = $sDataClassName::update($ID, $arFields);
	else
		$result = $sDataClassName::add($arFields);

	if($result->isSuccess())
	{
		if(!$ID)
			$ID = $result->getId();

		if($subWindow)
		{
			?><script type="text/javascript">
top.BX.closeWait(); top.BX.WindowManager.Get().AllowClose(); top.BX.WindowManager.Get().Close();
</script><?
			die();
		}
		else
		{
			if($_REQUEST["save"] <> '')
				LocalRedirect($strRedirect_admin);
			elseif($_REQUEST["apply"] <> '')
				LocalRedirect($strRedirect."&ID=".$ID."&".$tabControl->ActiveTabParam());
			elseif(strlen($save_and_add)>0)
				LocalRedirect($strRedirect."&ID=0&".$tabControl->ActiveTabParam());
		}
	}
	else
	{
		$arErrors = array();
		foreach($result->getErrors() as $error)
			$arErrors[] = $error->getMessage();
		$strError = join("<br>", $arErrors);
		$bFromForm = true;
	}
}
elseif($subWindow)
{
	if (!empty($_REQUEST['dontsave']))
	{
		?><script type="text/javascript">top.BX.closeWait(); top.BX.WindowManager.Get().AllowClose(); top.BX.WindowManager.Get().Close();</script><?
		die();
	}
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$aMenu = array(
	array(
		"TEXT" => GetMessage("XDEV_CP_NODE_BACK_TO_ADMIN"),
		"LINK" => $strRedirect_admin,
		"ICON" => "btn_list",
	)
);
if(!$subWindow && !$readOnly && $ID > 0)
{
	$aMenu[] = array("SEPARATOR" => "Y");
	$aMenu[] = array(
		"TEXT"	=> GetMessage("XDEV_CP_NODE_NEW_PARENT_RECORD"),
		"LINK"	=> BX_ROOT . "/admin/xdev_cp_node_edit.php?ID=0&PAGE_ID=".$arPage["ID"]."&PARENT_ID=".$ID."&lang=" . LANGUAGE_ID,
		"ICON"	=> "btn_new",
		"TITLE"	=> GetMessage("XDEV_CP_NODE_NEW_PARENT_RECORD"),
	);
	$aMenu[] = array(
		"TEXT"	=> GetMessage("XDEV_CP_NODE_NEW_RECORD"),
		"LINK"	=> $strRedirect,
		"ICON"	=> "btn_new",
		"TITLE"	=> GetMessage("XDEV_CP_NODE_NEW_RECORD"),
	);
	$aMenu[] = array(
		"TEXT"	=> GetMessage("XDEV_CP_NODE_DELETE_RECORD"),
		"LINK"	=> "javascript:if(confirm('" . GetMessageJS("XDEV_CP_NODE_DELETE_RECORD_CONFIRM") . "')) window.location='" . $strRedirect_admin . "&action=delete&ID=" . $ID . "&" . bitrix_sessid_get() . "';",
		"ICON"	=> "btn_delete",
		"TITLE"	=> GetMessage("XDEV_CP_NODE_DELETE_RECORD"),
	);
}

if($bFromForm)
	$DB->InitTableVarsForEdit($sDataClassName::getTableName(), "", "str_");

$context = new CAdminContextMenu($aMenu);

$context->Show();

if($strError <> '')
{
	$e = new CAdminException(array(array("text" => $strError)));
	$message = new CAdminMessage(GetMessage("MAIN_ERROR_SAVING"), $e);
	echo $message->Show();
}

//We have to explicitly call calendar and editor functions because
//first output may be discarded by form settings
//
$tabControl->BeginPrologContent();
// CJSCore::Init(array('date'));
$tabControl->EndPrologContent();
$tabControl->BeginEpilogContent();
echo GetFilterHiddens("filter_");
?>
<?=bitrix_sessid_post()?>
<input type="hidden" name="Update" value="Y" />
<input type="hidden" name="TYPE" value="<? echo $str_TYPE; ?>" />
<input type="hidden" name="PAGE_ID" value="<? echo $arPage["ID"]; ?>" />
<?
$tabControl->EndEpilogContent();

$tabControl->Begin(array(
	"FORM_ACTION" => $APPLICATION->GetCurPage()."?ID=".intval($ID)."&lang=" . LANG
));

$tabControl->BeginNextFormTab();

if($ID > 0)
{
	$tabControl->AddViewField("ID", "ID:", $ID);

	if(is_set($arEditFields, "TIMESTAMP_X"))
	{
		$field = $arEditFields["TIMESTAMP_X"];
		unset($arEditFields["TIMESTAMP_X"]);

		$tabControl->AddViewField($field->getName(), $field->getTitle() . ":", ${"str_" . $field->getName()});
	}

	if(is_set($arEditFields, "DATE_CREATE"))
	{
		$field = $arEditFields["DATE_CREATE"];
		unset($arEditFields["DATE_CREATE"]);

		$tabControl->AddViewField($field->getName(), $field->getTitle() . ":", ${"str_" . $field->getName()});
	}
}

$tabControl->AddViewField("TYPE", GetMessage("XDEV_CP_NODE_TYPE_FIELD") . ":", $arRegisteredNode["NAME"]);

$str = '';

if($MODULE_RIGHT >= "W")
{
	$str .= '<a href="' . BX_ROOT . '/admin/xdev_cp_page_edit.php?ID=' . $arPage["ID"] . '&lang=' . LANGUAGE_ID.'">' . htmlspecialchars($arPage["NAME"]) . '</a>';
}
else
{
	$str .= $arPage["NAME"];
}

$tabControl->AddViewField("PAGE_ID", GetMessage("XDEV_CP_NODE_PAGE_ID_FIELD") . ":", $str);

AT::drawFormFieldRow($tabControl, $arEditFields["ACTIVE"], $can_edit);

AT::drawFormFieldRow($tabControl, $arEditFields["NAME"], $can_edit);

AT::drawFormFieldRow($tabControl, $arEditFields["CSS_SELECTOR"], $can_edit);

if($str_TYPE == "L")
{
	$tabControl->BeginCustomField("TO_PAGE_ID", GetMessage("XDEV_CP_NODE_TO_PAGE_ID_FIELD") . ":", true);

	?><tr>
		<td width="40%"><? echo $tabControl->GetCustomLabelHTML(); ?></td>
		<td><?

		$arr = array(
			"REFERENCE_ID" => array(),
			"REFERENCE" => array(),
		);
		$rs = PageTable::getList(array(
			"select" => array("ID", "NAME"),
			"filter" => array(
				"PROFILE_ID" => $arPage["PROFILE_ID"],
			))
		);
		while($ar = $rs->Fetch())
		{
			$arr["REFERENCE"][] = $ar["NAME"];
			$arr["REFERENCE_ID"][] = $ar["ID"];
		}
		echo SelectBoxFromArray("TO_PAGE_ID", $arr,  $str_TO_PAGE_ID);

		?></td>
	</tr><?

	$tabControl->EndCustomField("TO_PAGE_ID");

	AT::drawFormFieldRow($tabControl, $arEditFields["EXPRESSION"], $can_edit);
}

$tabControl->BeginCustomField("PARENT_ID", GetMessage("XDEV_CP_NODE_PARENT_ID_FIELD") . ":");
?><tr>
	<td> <? echo $tabControl->GetCustomLabelHTML(); ?></td>
	<td><?

		?><select name="PARENT_ID">
			<option value="">(<? echo GetMessage("MAIN_NO"); ?>)</option><?

		$rs = $sDataClassName::getList(array(
			"select" => array(
				"ID", "CSS_SELECTOR"
			),
			"filter" => array(
				"!ID" => $ID,
				"PAGE_ID" => $str_PAGE_ID
			)
		));

		while($ar = $rs->Fetch())
		{
			?><option value="<? echo $ar["ID"]; ?>"<? echo ($str_PARENT_ID == $ar["ID"] ? ' selected="selected"' : ""); ?>><? echo htmlspecialchars($ar["CSS_SELECTOR"]); ?> [<? echo $ar["ID"]; ?>]</option><?
		}

		?></select>

	</td>
</tr><?
$tabControl->EndCustomField("PARENT_ID");

if($subWindow)
{
	$tabControl->Buttons(false, '');
}
else
{
	$tabControl->Buttons(array(
		"btnSaveAndAdd" => true,
		"back_url" => $strRedirect_admin,
	));
}

?>
<script type="text/javascript">
	<?
	if ($subWindow)
	{
		?>top.BX.WindowManager.Get().adjustSizeEx();
		<?
	}
?></script><?

$tabControl->Show();
$tabControl->ShowWarnings($tabControl->GetName(), $message);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>