<?

use Bitrix\Main\Localization\Loc;

use \Xdev\Cp\Admin\Tools as AT;
use \Xdev\Cp\Export\EntityTable;
use \Xdev\Cp\Export\ProfileTable;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/tools/prop_userid.php");
\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);

$subWindow = defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1;

if ($subWindow) {
    require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/iblock/classes/general/subelement.php');
}

/**
 * Endity datamanager classname
 *
 * @var string
 */
$sDataClassName = "xdev\cp\Export\FieldmapTable";

/**
 * Module id
 *
 * @var string
 */
$module_id = "xdev.cp";

$sTableID = preg_replace("/\\\+/", "_", $sDataClassName)."_sub_edit";

$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);
if ($MODULE_RIGHT < "W") {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}
$can_edit = ($MODULE_RIGHT >= "W");

CModule::IncludeModuleEx($module_id);

$arParent = false;
$ID = intval($_REQUEST["ID"]);

$rs = $sDataClassName::getByID($ID);
$rs = new CAdminResult($rs, $sTableID);
if (!$rs->ExtractFields("str_")) {
    $ID = 0;
    $str_ENTITY_ID = isset($_REQUEST["ENTITY_ID"]) ? intval($_REQUEST["ENTITY_ID"]) : 0;
}
$arEntity = false;
if ($str_ENTITY_ID) {
    $arEntity = EntityTable::getRowById($str_ENTITY_ID);
}

$APPLICATION->SetTitle($ID > 0 ? GetMessage("XDEV_CP_NODE_EDIT_TITLE", ["#ID#" => $ID]) : GetMessage("XDEV_CP_NODE_NEW_TITLE"));

if (!is_array($arEntity)) {
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

    $e = new CAdminException([["text" => GetMessage("XDEV_CP_NODE_PAGE_404")]]);
    $message = new CAdminMessage("", $e);
    echo $message->Show();
} else {
    $arExport = ProfileTable::getRowById($arEntity["EXPORT_ID"]);

    $strRedirect_admin = BX_ROOT."/admin/xdev_cp_export_field_list.php?lang=".LANGUAGE_ID."&ENTITY_ID=".$arEntity["ID"];

    $strRedirect = BX_ROOT."/admin/xdev_cp_export_field_edit.php?lang=".LANGUAGE_ID."&ENTITY_ID=".$arEntity["ID"];

    $aTabs = [];
    $aTabs[] = ["DIV" => "field_edit1", "TAB" => GetMessage("XDEV_CP_NODE_TAB1"), "TITLE" => GetMessage("XDEV_CP_NODE_TAB1_TITLE")];

    $strError = "";

    if ($subWindow) {
        $arPostParams = [
            'bxpublic' => 'Y',
            'ENTITY_ID' => $str_ENTITY_ID,
            'sessid' => bitrix_sessid(),
        ];
        $listUrl = [
            'LINK' => $APPLICATION->GetCurPageParam(),
            'POST_PARAMS' => $arPostParams,
        ];
        $tabControl = new CAdminSubForm($sTableID, $aTabs, false, true, $listUrl, false);
    } else {
        $tabControl = new CAdminForm($sTableID, $aTabs);
        $tabControl->SetShowSettings(false);
    }

    $arEditFields = [];
    foreach ($sDataClassName::getEntity()->getFields() as $field) {
        if (
            $field instanceof Bitrix\Main\Entity\ScalarField
            && !$field->isPrimary()
        ) {
            $arEditFields[$field->getName()] = $field;
        }
    }

    $bFromForm = false;
    if (
        $_SERVER["REQUEST_METHOD"] == "POST"
        && (
            $_REQUEST["save"] <> ''
            || $_REQUEST["apply"] <> ''
            || $_REQUEST["Update"] == "Y"
            || $_REQUEST["save_and_add"] <> ''
        )
        && check_bitrix_sessid()
    ) {
        $arFields = [];
        foreach ($arEditFields as $key => $field) {
            if (!in_array($key, ["TIMESTAMP_X", "DATE_CREATE"])) {
                $value = $_REQUEST[$key];
                if ($field instanceof Bitrix\Main\Entity\BooleanField) {
                    $value == "Y" ? "Y" : "N";
                } elseif ($field instanceof Bitrix\Main\Entity\DatetimeField) {
                    $value = new \Bitrix\Main\Type\DateTime($value);
                }

                $arFields[$key] = $value;
            }
        }

        if (!intval($arFields["NODE_ID"])) {
            $arFields["NODE_ID"] = null;
        }

        if ($ID) {
            $result = $sDataClassName::update($ID, $arFields);
        } else {
            $result = $sDataClassName::add($arFields);
        }

        if ($result->isSuccess()) {
            if (!$ID) {
                $ID = $result->getId();
            }

            if ($subWindow) {
                ?>
                <script type="text/javascript">
                top.BX.closeWait();
                top.BX.WindowManager.Get().AllowClose();
                top.BX.WindowManager.Get().Close();
                </script><?
                die();
            } else {
                if ($_REQUEST["save"] <> '') {
                    LocalRedirect($strRedirect_admin);
                } elseif ($_REQUEST["apply"] <> '') {
                    LocalRedirect($strRedirect."&ID=".$ID."&".$tabControl->ActiveTabParam());
                } elseif (strlen($save_and_add) > 0) {
                    LocalRedirect($strRedirect."&ID=0&".$tabControl->ActiveTabParam());
                }
            }
        } else {
            $arErrors = [];
            foreach ($result->getErrors() as $error) {
                $arErrors[] = $error->getMessage();
            }
            $strError = join("<br>", $arErrors);
            $bFromForm = true;
        }
    } elseif ($subWindow) {
        if (!empty($_REQUEST['dontsave'])) {
            ?>
            <script type="text/javascript">top.BX.closeWait();
            top.BX.WindowManager.Get().AllowClose();
            top.BX.WindowManager.Get().Close();</script><?
            die();
        }
    }

    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

    $aMenu = [
        [
            "TEXT" => GetMessage("XDEV_CP_NODE_BACK_TO_ADMIN"),
            "LINK" => $strRedirect_admin,
            "ICON" => "btn_list",
        ],
    ];
    if (!$subWindow && !$readOnly && $ID > 0) {
        $aMenu[] = ["SEPARATOR" => "Y"];
        $aMenu[] = [
            "TEXT" => GetMessage("XDEV_CP_NODE_NEW_PARENT_RECORD"),
            "LINK" => BX_ROOT."/admin/xdev_cp_export_entity_edit.php?ID=0&ENTITY_ID=".$arEntity["ID"]."&lang=".LANGUAGE_ID,
            "ICON" => "btn_new",
            "TITLE" => GetMessage("XDEV_CP_NODE_NEW_PARENT_RECORD"),
        ];
        $aMenu[] = [
            "TEXT" => GetMessage("XDEV_CP_NODE_NEW_RECORD"),
            "LINK" => $strRedirect,
            "ICON" => "btn_new",
            "TITLE" => GetMessage("XDEV_CP_NODE_NEW_RECORD"),
        ];
        $aMenu[] = [
            "TEXT" => GetMessage("XDEV_CP_NODE_DELETE_RECORD"),
            "LINK" => "javascript:if(confirm('"
                .GetMessageJS("XDEV_CP_NODE_DELETE_RECORD_CONFIRM")
                ."')) window.location='"
                .$strRedirect_admin
                ."&action=delete&ID="
                .$ID
                ."&"
                .bitrix_sessid_get()
                ."';",
            "ICON" => "btn_delete",
            "TITLE" => GetMessage("XDEV_CP_NODE_DELETE_RECORD"),
        ];
    }

    if ($bFromForm) {
        $DB->InitTableVarsForEdit($sDataClassName::getTableName(), "", "str_");
    }

    $context = new CAdminContextMenu($aMenu);

    $context->Show();

    if ($strError <> '') {
        $e = new CAdminException([["text" => $strError]]);
        $message = new CAdminMessage(GetMessage("MAIN_ERROR_SAVING"), $e);
        echo $message->Show();
    }

    $tabControl->BeginEpilogContent();

    echo GetFilterHiddens("filter_");
    ?>
    <?= bitrix_sessid_post() ?>
    <input type="hidden" name="Update" value="Y"/>
    <input type="hidden" name="ENTITY_ID" value="<? echo $arEntity["ID"]; ?>"/>
    <?
    $tabControl->EndEpilogContent();

    $tabControl->Begin(
        [
            "FORM_ACTION" => $APPLICATION->GetCurPage()."?ID=".intval($ID)."&lang=".LANG,
        ]
    );

    $tabControl->BeginNextFormTab();

    if ($ID > 0) {
        $tabControl->AddViewField("ID", "ID:", $ID);

        if (is_set($arEditFields, "TIMESTAMP_X")) {
            $field = $arEditFields["TIMESTAMP_X"];
            unset($arEditFields["TIMESTAMP_X"]);

            $tabControl->AddViewField($field->getName(), $field->getTitle().":", ${"str_".$field->getName()});
        }

        if (is_set($arEditFields, "DATE_CREATE")) {
            $field = $arEditFields["DATE_CREATE"];
            unset($arEditFields["DATE_CREATE"]);

            $tabControl->AddViewField($field->getName(), $field->getTitle().":", ${"str_".$field->getName()});
        }
    }

    $arTypes = \Xdev\cp\Export\EntityTable::getTypeArray();

    $str = '';

    if ($MODULE_RIGHT >= "W") {
        $str .= '<a href="'.BX_ROOT.'/admin/xdev_cp_export_entity_edit.php?ID='.$arEntity["ID"].'&lang='.LANGUAGE_ID.'">'.htmlspecialchars(
                $arTypes[$arEntity["TYPE_ID"]]
            ).'</a>';
    } else {
        $str .= htmlspecialchars($arTypes[$arEntity["TYPE_ID"]]);
    }

    $tabControl->AddViewField("ENTITY_ID", GetMessage("XDEV_CP_EXPORT_FIELDMAP_ENTITY_ID_FIELD").":", $str);

    $tabControl->BeginCustomField("NODE_ID", loc::getMessage("XDEV_CP_EXPORT_FIELDMAP_NODE_ID_FIELD").":", true);
    ?>
    <tr>
        <td width="40%"><? echo $tabControl->GetCustomLabelHTML(); ?></td>
        <td><? echo \xdev\cp\profile\NodeTable::getDropDownListEx(
                $str_NODE_ID, "PAGE_ID", "NODE_ID", ["PAGE.PROFILE_ID" => $arExport["PROFILE_ID"]]
            ); ?></td>
    </tr><?
    $tabControl->EndCustomField("NODE_ID");

    AT::drawFormFieldRow($tabControl, $arEditFields["NAME"], $can_edit);

    AT::drawFormFieldRow($tabControl, $arEditFields["VALUE"], $can_edit);

    if ($subWindow) {
        $tabControl->Buttons(false, '');
    } else {
        $tabControl->Buttons(
            [
                "btnSaveAndAdd" => true,
                "back_url" => $strRedirect_admin,
            ]
        );
    }

    ?>
    <script type="text/javascript">
    <?
    if ($subWindow)
    {
    ?>top.BX.WindowManager.Get().adjustSizeEx();
    <?
    }
    ?></script><?

    $tabControl->Show();
    $tabControl->ShowWarnings($tabControl->GetName(), $message);
}
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>