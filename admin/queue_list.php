<?

use \Bitrix\Main\Entity\Query;
use \Bitrix\Main\Entity\ExpressionField;

use xdev\cp\Queue\SheduleTable;
use xdev\cp\Profile\PageTable;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
CPageOption::GetOptionString("main", "nav_page_in_session", "N");

$SHEDULE_ID = isset($_REQUEST["SHEDULE_ID"]) ? intval($_REQUEST["SHEDULE_ID"]) : 0;

/**
 * Endity datamanager classname
 *
 * @var string
 */
$sDataClassName = "xdev\cp\Queue\QueueTable";

/**
 * Module id
 *
 * @var string
 */
$module_id = "xdev.cp";

$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);
if ($MODULE_RIGHT == "D") {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}
$can_edit = ($MODULE_RIGHT >= "W");

CModule::IncludeModuleEx($module_id);

$arShedule = SheduleTable::getRowById($SHEDULE_ID);
if (!is_array($arShedule)) {
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

    $e = new CAdminException([["text" => GetMessage("XDEV_CP_QUEUE_LIST_PAGE_404")]]);
    $message = new CAdminMessage("", $e);
    echo $message->Show();

    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_after.php");
    die();
}

$rs = PageTable::getList(
    [
        "select" => ["ID", "NAME"],
        "filter" => ["PROFILE_ID" => $arShedule["PROFILE_ID"]],
    ]
);
$arPages = [];
while ($ar = $rs->Fetch()) {
    $arPages[$ar["ID"]] = $ar;
}

/**
 * Entity edit page
 *
 * @var string
 */
$strEditPath = BX_ROOT."/admin/xdev_cp_node_edit.php?lang=".LANGUAGE_ID."&SHEDULE_ID=".$arShedule["ID"];

$sTableID = preg_replace("/\\\+/", "_", $sDataClassName)."_list";

$arHeaders = $arFilterFields = $arInitFilter = [];
$sPrimaryKey = "ID";
foreach ($sDataClassName::getEntity()->getFields() as $field) {
    if (
        ($field instanceof Bitrix\Main\Entity\ScalarField || $field instanceof Bitrix\Main\Entity\ExpressionField)
        && in_array($field->getName(), ["ID", "STATUS_ID", "DATE_CREATE", "PAGE_ID", "URL", "QUEUE_CNT"])
    ) {
        $id = $field->getColumnName();
        if ($field->isPrimary()) {
            $sPrimaryKey = $field->getColumnName();
        }

        if (strlen($field->getTitle())) {
            $title = GetMessage(sprintf("XDEV_CP_QUEUE_%s_FIELD_ADMIN", $field->getName()));
            $arHeaders[] = [
                "id" => $field->getName(),
                "content" => strlen($title) ? $title : $field->getTitle(),
                "sort" => $field->getName(),
                "default" => true,
            ];

            if ($field instanceof Bitrix\Main\Entity\DatetimeField) {
                $arInitFilter[$id."_FROM"] = "find_".$id."_from";
                $arInitFilter[$id."_TO"] = "find_".$id."_to";
                $arFilterFields["find_".$id."_from"] = $id;
                $arFilterFields["find_".$id."_to"] = $id;
            } else {
                $arInitFilter[$id] = "find_".$id;
                $arFilterFields["find_".$id] = $id;
            }
        }
    }
}

$lAdmin = new CAdminList(
    $sTableID,
    new CAdminSorting($sTableID, $sPrimaryKey, "asc")
);
// FILTER
$lAdmin->InitFilter($arInitFilter);

$arFilter = [
    "SHEDULE_ID" => $arShedule["ID"],
];
foreach ($sDataClassName::getEntity()->getFields() as $field) {
    if ($field instanceof Bitrix\Main\Entity\ScalarField) {
        $id = $field->getColumnName();
        $fieldName = "find_".$id;

        if ($field instanceof Bitrix\Main\Entity\DatetimeField) {
            if (!empty(${$fieldName."_from"})) {
                $arFilter[">=".$id] = ${$fieldName."_from"};
            }
            if (!empty(${$fieldName."_to"})) {
                $arFilter["<=".$id] = ${$fieldName."_to"};
            }
        } else {
            if (!empty(${$fieldName})) {
                $arFilter[$id] = ${$fieldName};
            }
        }
    }
}

// ACTIONS
if ($lAdmin->EditAction() && $can_edit) {
    foreach ($FIELDS as $ID => $arFields) {
        if (!$lAdmin->IsUpdated($ID)) {
            continue;
        }

        $errors = "";
        $result = $sDataClassName::update($ID, $arFields);
        if (!$result->isSuccess()) {
            foreach ($result->getErrors() as $error) {
                $errors .= $error->getMessage()."<br>";
            }
        }
        if (!empty($errors)) {
            $lAdmin->AddUpdateError($errors, $ID);
        }
    }
}

if ($arID = $lAdmin->GroupAction()) {
    if ($_REQUEST["action_target"] == "selected") {
        $arID = [];
        $rs = $sDataClassName::getList(
            [
                "select" => ["ID"],
                "filter" => $arFilter,
            ]
        );
        while ($ar = $rs->Fetch()) {
            $arID[] = $ar["ID"];
        }
    }

    foreach ($arID as $ID) {
        $ID = IntVal($ID);
        if ($ID <= 0) {
            continue;
        }

        $result = new \Bitrix\Main\Entity\Result();
        switch ($_REQUEST["action"]) {
            case "delete":
                if ($can_edit) {
                    $result = $sDataClassName::delete($ID);
                }
                break;

            case "clear":
                if ($can_edit) {
                    $sDataClassName::clear($ID);
                }
                break;
        }

        if (!$result->isSuccess()) {
            $arErrors = [];
            foreach ($result->getErrors() as $error) {
                $arErrors[] = $error->getMessage();
            }
            $lAdmin->AddGroupError(join("<br>", $arErrors), $ID);
        }
    }
}

$lAdmin->AddHeaders($arHeaders);

$arSelect = [];
foreach ($arHeaders as $val) {
    if (in_array($val["id"], $lAdmin->GetVisibleHeaderColumns())) {
        $arSelect[$val["id"]] = (is_set($val, "sort") ? $val["sort"] : $val["id"]);
    }
}
if (!in_array($sPrimaryKey, $arSelect)) {
    $arSelect[$sPrimaryKey] = $sPrimaryKey;
}

$order = strtoupper($order);

$usePageNavigation = true;
if (isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'excel') {
    $usePageNavigation = false;
} else {
    $navyParams = CDBResult::GetNavParams((new CAdminResult($rs, $sTableID))->GetNavSize($sTableID));
    if ($navyParams['SHOW_ALL']) {
        $usePageNavigation = false;
    } else {
        $navyParams['PAGEN'] = (int) $navyParams['PAGEN'];
        $navyParams['SIZEN'] = (int) $navyParams['SIZEN'];
    }
}
$getListParams = [
    "select" => $arSelect,
    "filter" => $arFilter,
    "order" => [
        $by => strtoupper($order),
    ],
];
unset($filterValues);
if ($usePageNavigation) {
    $getListParams["limit"] = $navyParams['SIZEN'];
    $getListParams["offset"] = $navyParams['SIZEN'] * ($navyParams['PAGEN'] - 1);
}

if ($usePageNavigation) {
    $countQuery = new Query($sDataClassName::getEntity());
    $countQuery->addSelect(new ExpressionField('CNT', 'COUNT(1)'));
    $countQuery->setFilter($getListParams['filter']);
    $totalCount = $countQuery->setLimit(null)->setOffset(null)->exec()->fetch();
    unset($countQuery);
    $totalCount = (int) $totalCount['CNT'];
    if ($totalCount > 0) {
        $totalPages = ceil($totalCount / $navyParams['SIZEN']);
        if ($navyParams['PAGEN'] > $totalPages) {
            $navyParams['PAGEN'] = $totalPages;
        }
        $getListParams['limit'] = $navyParams['SIZEN'];
        $getListParams['offset'] = $navyParams['SIZEN'] * ($navyParams['PAGEN'] - 1);
    } else {
        $navyParams['PAGEN'] = 1;
        $getListParams['limit'] = $navyParams['SIZEN'];
        $getListParams['offset'] = 0;
    }
}

$rsData = new CAdminResult($sDataClassName::getList($getListParams), $sTableID);
if ($usePageNavigation) {
    $rsData->NavStart($getListParams['limit'], $navyParams['SHOW_ALL'], $navyParams['PAGEN']);
    $rsData->NavRecordCount = $totalCount;
    $rsData->NavPageCount = $totalPages;
    $rsData->NavPageNomer = $navyParams['PAGEN'];
} else {
    $rsData->NavStart();
}

$lAdmin->NavText($rsData->GetNavPrint($title));

$arStats = $sDataClassName::getStatusArray();
while ($data = $rsData->NavNext(true, "f_")) {
    $arActions = [];

    $strDetailPath = "";
    if ($data["STATUS_ID"] != "N") {
        $strDetailPath = "/bitrix/admin/xdev_cp_queue_node_list.php?QUEUE_ID=".$f_ID."&lang=".LANGUAGE_ID;
    }

    if (strlen($strDetailPath)) {
        $row = &$lAdmin->AddRow(
            $f_ID,
            $data,
            $strDetailPath,
            GetMessage("XDEV_CP_QUEUE_LIST_VIEW_RESULT_NODES")
        );
    } else {
        $row = &$lAdmin->AddRow($f_ID, $data);
    }

    $row->AddViewField(
        "STATUS_ID", '<div class="lamp-'
        .$sDataClassName::getLampStatus($f_STATUS_ID)
        .'" title="'
        .htmlspecialchars($arStats[$f_STATUS_ID])
        .'" style="display:inline-block;"></div>'
    );

    $page = $arPages[$data["PAGE_ID"]];
    if (is_array($page)) {
        $row->AddViewField("PAGE_ID", $page["NAME"]);
    }

    $arActions = [];
    if ($can_edit) {
        if (strlen($strDetailPath)) {
            $arActions[] = [
                "ICON" => "view",
                "TEXT" => GetMessage("XDEV_CP_QUEUE_LIST_VIEW_RESULT_NODES"),
                "LINK" => $strDetailPath,
                "DEFAULT" => true,
            ];
        }

        $arActions[] = [
            "ICON" => "delete",
            "TEXT" => GetMessage("MAIN_ADMIN_MENU_DELETE"),
            "ACTION" => "if(confirm('".GetMessageJS("XDEV_CP_QUEUE_LIST_DELETE_RECORD_CONFIRM")."')) ".$lAdmin->ActionDoGroup(
                    $f_ID, "delete", "&SHEDULE_ID=".$arShedule["ID"]."&lang=".LANGUAGE_ID
                ),
        ];
    }

    $row->AddActions($arActions);
}

$lAdmin->AddFooter(
    [
        ["title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value" => $rsData->SelectedRowsCount()],
        ["counter" => true, "title" => GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value" => "0"],
    ]
);

$arParams = [];
$arGroupActions = [];
if ($can_edit) {
    $arGroupActions = [
        "delete" => true,
        "clear" => GetMessage("XDEV_CP_QUEUE_LIST_CLEAR_ACTION"),
    ];
}
$lAdmin->AddGroupActionTable($arGroupActions, $arParams);

$aMenu = [];
$lAdmin->AddAdminContextMenu($aMenu);

$lAdmin->CheckListMode();

$APPLICATION->SetTitle(GetMessage("XDEV_CP_QUEUE_LIST_TITLE_ADMIN", ["#ID#" => $arShedule["ID"]]));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

?>
    <form method="GET" action="<? echo $APPLICATION->GetCurPage(); ?>" name="find_form">
    <input type="hidden" name="lang" value="<? echo LANG ?>">
    <input type="hidden" name="filter" value="Y">
    <input type="hidden" name="SHEDULE_ID" value="<? echo $arShedule["ID"]; ?>"><?

$arFindFields = ["ID" => "ID"];
foreach ($arHeaders as $arHeader) {
    if (in_array($arHeader["id"], $arFilterFields) && $key = array_search($arHeader["id"], $arFilterFields)) {
        $arFindFields[$arHeader["id"]] = $arHeader["content"];
    }
}

$oFilter = new CAdminFilter($sTableID."_filter", $arFindFields);
$oFilter->Begin();

foreach ($sDataClassName::getEntity()->getFields() as $field) {
    if (
        ($field instanceof Bitrix\Main\Entity\ScalarField || $field instanceof Bitrix\Main\Entity\ExpressionField)
        && in_array($field->getColumnName(), $arFilterFields)
    ) {
        $id = $field->getColumnName();
        $fieldName = "find_".$id;

        ?>
        <tr>
        <td><? echo HasMessage($module_id."_FILTER_".$id)
                ? GetMessage($module_id."_FILTER_".$id)
                : $field->getTitle();
            ?>:
        </td>
        <td><?

            $arr = [
                "REFERENCE_ID" => [],
                "REFERENCE" => [],
            ];

            if ($field->getColumnName() == "PAGE_ID") {
                foreach ($arPages as $ar) {
                    $arr["REFERENCE"][] = $ar["NAME"];
                    $arr["REFERENCE_ID"][] = $ar["ID"];
                }
            } elseif ($field->getColumnName() == "STATUS_ID") {
                foreach ($sDataClassName::getStatusArray() as $key => $val) {
                    $arr["REFERENCE"][] = $val;
                    $arr["REFERENCE_ID"][] = $key;
                }
            } elseif ($field instanceof Bitrix\Main\Entity\BooleanField) {
                if (\xdev\cp\admin\tools::isBooleanField($field)) {
                    foreach ($field->getValues() as $val) {
                        $arr["REFERENCE"][] = ($val == "Y" ? GetMessage("MAIN_YES") : ($val == "N" ? GetMessage("MAIN_NO") : $val));
                        $arr["REFERENCE_ID"][] = $val;
                    }
                } else {
                    foreach ($field->getValues() as $val) {
                        $arr["REFERENCE"][] = $val;
                        $arr["REFERENCE_ID"][] = $val;
                    }
                }
            }

            if (!empty($arr["REFERENCE_ID"])) {
                echo SelectBoxFromArray($fieldName, $arr, ${$fieldName}, GetMessage("MAIN_ALL"));
            } elseif ($field instanceof Bitrix\Main\Entity\DatetimeField) {
                echo CalendarPeriod(
                    $fieldName."_from", htmlspecialcharsbx(${$fieldName."_from"}), $fieldName."_to",
                    htmlspecialcharsbx(${$fieldName."_from"}), "find_form", true
                );
            } else {
                ?><input type="text" name="<? echo $fieldName; ?>" value="<? echo htmlspecialcharsbx(${$fieldName}); ?>" /><?
            }

            ?></td>
        </tr><?
    }
}

$oFilter->Buttons(["table_id" => $sTableID, "url" => $APPLICATION->GetCurPage(), "form" => "find_form"]);
$oFilter->End();

?></form><?

if (strlen($errorMessage)) {
    CAdminMessage::ShowMessage(["MESSAGE" => $errorMessage, "TYPE" => "ERROR"]);
}

$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>